package gestionK.web;

import javax.servlet.http.HttpSession;

import gestionK.dominio.Usuario;

import org.json.JSONObject;
public class CheckSession {

	public static JSONObject check(HttpSession session){
		Usuario usuario=(Usuario) session.getAttribute("usuario");
		if (usuario == null) {
			JSONObject resultado=new JSONObject();
			resultado.put("tipo", "ERROR");
			resultado.put("texto", "Tu sesión ha expirado o no te has identificado");
			return resultado;
		}
		return null;
	}
}
