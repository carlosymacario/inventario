package gestionK.dominio.test;

import static org.junit.Assert.*;
import gestionK.dao.DAOCliente;
import gestionK.dominio.Albaran;
import gestionK.dominio.Cliente;
import gestionK.dominio.Factura;
import gestionK.dominio.Manager;
import gestionK.dominio.Presupuesto;
import gestionK.dominio.Tique;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class ManagerTest {

	@Before
	public void setUp() {
		Manager.get().eliminarUsuario("Macario");
	}
	
	@Test
	public void testRegistrarse() {
		try {
			Manager.get().registrar("Macario", "Macario", "Macario");
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testRegistrarse2() {
		try {
			Manager.get().registrar("Macario", "Macario", "Macario");
			Manager.get().registrar("Macario", "Macario", "Macario");
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testRegistrarse3() {
		try {
			Manager.get().registrar("Macario", "Macario", "Macario2");
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testIniciarSesion() {
		try {
			Manager.get().registrar("Macario", "Macario", "Macario");
			Manager.get().identificar("Macario", "Macario");
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testIniciarSesion2() {
		try {
			Manager.get().identificar("Macario2", "Macario");
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testIniciarSesion3() {
		try {
			Manager.get().registrar("Macario", "Macario", "Macario");
			Manager.get().identificar("Macario", "Macario2");
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testCrearCliente() {
		try {
			Manager.get().crearCliente("Javier", "Garrido", "05478515D", "C/ Honda, 7", "Herencia", "13640", "Ciudad Real", "España", "695710360", "jGarrido@email.com");
			Cliente cliente = new Cliente();
			cliente = DAOCliente.selectClienteTique("05478515D");
			Manager.get().eliminarCliente(cliente.getId());
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testModificarCliente() {
		try {
			Manager.get().crearCliente("Javier", "Garrido", "05478515D", "C/ Honda, 7", "Herencia", "13640", "Ciudad Real", "España", "695710360", "jGarrido@email.com");
			Cliente cliente = new Cliente();
			cliente = DAOCliente.selectClienteTique("05478515D");
			Manager.get().modificarCliente(cliente.getId(), "Javier", "Garrido", "05478515D", "C/ Honda, 7", "Herencia", "13640", "Ciudad Real", "España", "695710360", "jGarrido@email.com");
			Manager.get().eliminarCliente(cliente.getId());
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testEliminarCliente() {
		try {
			Manager.get().crearCliente("Javier", "Garrido", "05478515D", "C/ Honda, 7", "Herencia", "13640", "Ciudad Real", "España", "695710360", "jGarrido@email.com");
			Cliente cliente = new Cliente();
			cliente = DAOCliente.selectClienteTique("05478515D");
			Manager.get().eliminarCliente(cliente.getId());
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testCrearArticulo() {
		try {
			Manager.get().crearArticulo("25", "Cuaderno", 5, 2, 3);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("25"));
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testModificarArticulo() {
		try {
			Manager.get().crearArticulo("25", "Cuaderno", 5, 2, 3);
			Manager.get().modificarArticulo(Manager.get().selectArticulo("25"), "25", "Cuaderno", 6, 2, 3);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("25"));
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testEliminarArticulo() {
		try {
			Manager.get().crearArticulo("25", "Cuaderno", 5, 2, 3);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("25"));
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testCrearLibro() {
		try {
			Manager.get().crearLibro("30", "Juego de Tronos", "George R. R. Martin", "Gigamesh", "Distribooks", 5, 10, 15);
			Manager.get().eliminarLibro(Manager.get().selectLibro("30"));
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testModificarLibro() {
		try {
			Manager.get().crearLibro("30", "Juego de Tronos", "George R. R. Martin", "Gigamesh", "Distribooks", 5, 10, 15);
			Manager.get().modificarLibro(Manager.get().selectLibro("30"), "30", "Juego de Tronos", "George R. R. Martin", "Gigamesh", "Distribooks", 7, 10, 15);
			Manager.get().eliminarLibro(Manager.get().selectLibro("30"));
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testEliminarLibro() {
		try {
			Manager.get().crearLibro("30", "Juego de Tronos", "George R. R. Martin", "Gigamesh", "Distribooks", 5, 10, 15);
			Manager.get().eliminarLibro(Manager.get().selectLibro("30"));
		} catch (Exception e) {
			fail("No se esperaba excecpión");
		}
	}
	
	@Test
	public void testCrearTique() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Tique tique = new Tique();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			Manager.get().comprobarProductosTique(productos);
			tique = Manager.get().crearTique(3, productos);
			Manager.get().eliminarTique(tique.getId(), productos);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testCrearComprobarTique() {
		try {
			JSONArray productos = new JSONArray();
			Manager.get().comprobarProductosTique(productos);
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testModificarTique() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Tique tique = new Tique();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			tique = Manager.get().crearTique(3, productos);
			Manager.get().comprobarProductosTique(productos);
			Manager.get().modificarTique(3, productos, productos, tique.getId());
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
			Manager.get().eliminarTique(tique.getId(), productos);
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testModificarComprobarTique() {
		try {
			Tique tique = new Tique();
			JSONArray productos = new JSONArray();
			Manager.get().comprobarProductosTique(productos);
			tique = Manager.get().crearTique(3, productos);
			Manager.get().comprobarProductosTique(productos);
			Manager.get().modificarTique(3, productos, productos, tique.getId());
			Manager.get().eliminarTique(tique.getId(), productos);
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testEliminarTique() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Tique tique = new Tique();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			tique = Manager.get().crearTique(3, productos);
			Manager.get().eliminarTique(tique.getId(), productos);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testCrearFactura() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Factura factura = new Factura();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			Manager.get().comprobarProductosTique(productos);
			factura = Manager.get().crearFactura(3, productos, cliente);
			Manager.get().eliminarFactura(factura.getId(), productos);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testCrearComprobarFactura() {
		try {
			JSONArray productos = new JSONArray();
			Manager.get().comprobarProductosTique(productos);
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testModificarFactura() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Factura factura = new Factura();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			factura = Manager.get().crearFactura(3, productos, cliente);
			Manager.get().comprobarProductosTique(productos);
			Manager.get().modificarFactura(3, productos, cliente, productos, factura.getId(), cliente);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
			Manager.get().eliminarFactura(factura.getId(), productos);
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testModificarComprobarFactura() {
		try {
			Factura factura = new Factura();
			JSONArray productos = new JSONArray();
			Manager.get().comprobarProductosTique(productos);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			factura = Manager.get().crearFactura(3, productos, cliente);
			Manager.get().comprobarProductosTique(productos);
			Manager.get().modificarFactura(3, productos, cliente, productos, factura.getId(), cliente);
			Manager.get().eliminarFactura(factura.getId(), productos);
			fail("Esperaba excepción");
		} catch (Exception e) {
			
		}
	}
	
	@Test
	public void testEliminarFactura() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Factura factura = new Factura();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			factura = Manager.get().crearFactura(3, productos, cliente);
			Manager.get().eliminarFactura(factura.getId(), productos);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testCrearAlbaran() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Albaran albaran = new Albaran();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			albaran = Manager.get().crearAlbaran(3, productos, cliente);
			Manager.get().eliminarAlbaran(albaran.getId());
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testModificarAlbaran() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Albaran albaran = new Albaran();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			albaran = Manager.get().crearAlbaran(3, productos, cliente);
			Manager.get().modificarAlbaran(3, productos, cliente, productos, albaran.getId(), cliente);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
			Manager.get().eliminarAlbaran(albaran.getId());
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testEliminarAlbaran() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Albaran albaran = new Albaran();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			albaran = Manager.get().crearAlbaran(3, productos, cliente);
			Manager.get().eliminarAlbaran(albaran.getId());
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testCrearPresupuesto() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Presupuesto presupuesto = new Presupuesto();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			presupuesto = Manager.get().crearPresupuesto(3, productos, cliente);
			Manager.get().eliminarPresupuesto(presupuesto.getId());
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testModificarPresupuesto() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Presupuesto presupuesto = new Presupuesto();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			presupuesto = Manager.get().crearPresupuesto(3, productos, cliente);
			Manager.get().modificarPresupuesto(3, productos, cliente, productos, presupuesto.getId(), cliente);
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
			Manager.get().eliminarPresupuesto(presupuesto.getId());
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
	
	@Test
	public void testEliminarPresupuesto() {
		try {
			Manager.get().crearArticulo("33", "Cuaderno", 5, 2, 3);
			Presupuesto presupuesto = new Presupuesto();
			JSONArray productos = new JSONArray();
			JSONObject producto = new JSONObject();
			producto.put("codigo", "33");
			producto.put("nombre", "Cuaderno");
			producto.put("cantidad", "1");
			producto.put("precio", "3");
			producto.put("importe", "3");
			productos.put(0, producto);
			JSONObject cliente = new JSONObject();
			cliente.put("id", "1");
			cliente.put("nombre", "Javier");
			cliente.put("apellido", "Garrido");
			cliente.put("nif", "06359874D");
			cliente.put("direccion", "C/ Honda, 7");
			cliente.put("localidad", "Herencia");
			cliente.put("cp", "13640");
			cliente.put("provincia", "Ciudad Real");
			cliente.put("pais", "España");
			cliente.put("telefono", "639854603");
			cliente.put("correo", "jGarrido@email.com");
			presupuesto = Manager.get().crearPresupuesto(3, productos, cliente);
			Manager.get().eliminarPresupuesto(presupuesto.getId());
			Manager.get().eliminarArticulo(Manager.get().selectArticulo("33"));
		} catch (Exception e) {
			fail("No se esperaba excepción");
		}
	}
}
