package gestionK.dominio;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

public class Factura {
	private int numero;
	private String id;
	private double total;
	private String fecha;
	private String anyo;
	private List<Document> productos;
	private Cliente cliente;
	
	public Factura() {
		productos = new ArrayList<Document>();
		cliente = new Cliente();
	}
	
	public Factura(JSONObject facturaJSON) {
		productos = new ArrayList<Document>();
		this.numero = Integer.parseInt(facturaJSON.get("numero").toString());
		this.id = facturaJSON.get("id").toString();
		this.total = Double.parseDouble(facturaJSON.get("total").toString());
		this.fecha = facturaJSON.get("fecha").toString();
		this.anyo = facturaJSON.get("anyo").toString();
		JSONArray productosJSON = new JSONArray();
		productosJSON = facturaJSON.getJSONArray("productos");
		Document producto = new Document();
		for(int i=0; i<productosJSON.length(); i++) {
			producto = Document.parse(productosJSON.getJSONObject(i).toString());
			this.productos.add(producto);
		}
		this.cliente = new Cliente(facturaJSON.getJSONObject("cliente"));
	}

	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public String getAnyo() {
		return anyo;
	}

	public void setAnyo(String anyo) {
		this.anyo = anyo;
	}

	public List<Document> getProductos() {
		return productos;
	}

	public void setProductos(List<Document> productos) {
		this.productos = productos;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public Document getClienteAsDocument() {
		return cliente.toDocument();
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public JSONObject toJSONObject() {
		 JSONObject jso = new JSONObject();
		 jso.put("numero", (Object)this.numero);
		 jso.put("id", (Object)this.id);
		 jso.put("total", (Object)this.total);
		 jso.put("fecha", (Object)this.fecha);
		 jso.put("anyo", (Object)this.anyo);
		 jso.put("productos", (Object)this.productos);
		 jso.put("cliente", (Object)this.cliente.toJSONObject());
		 return jso;
	}
}
