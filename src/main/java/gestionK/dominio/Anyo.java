package gestionK.dominio;

import org.json.JSONObject;

public class Anyo {
	
	private String anyo;
	private String abierto;
	
	public String getAnyo() {
		return anyo;
	}

	public void setAnyo(String anyo) {
		this.anyo = anyo;
	}

	public String getAbierto() {
		return abierto;
	}

	public void setAbierto(String abierto) {
		this.abierto = abierto;
	}

	public JSONObject toJSONObject() {
		 JSONObject jso = new JSONObject();
		 jso.put("anyo", (Object)this.anyo);
		 jso.put("abierto", (Object)this.abierto);
		 return jso;
	 }

}
