package gestionK.dominio;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

public class BaseDatos {
	
	private MongoClient mongoClient;
	private MongoDatabase mongodb;
	
	public BaseDatos() {

		//Para cuando se vaya a migrar la base de datos a la nube
		/*MongoClientURI uri = new MongoClientURI("mongodb+srv://gestionK:<gestionK>@cluster0-skzrn.mongodb.net/gestionK?retryWrites=true&w=majority");

		mongoClient = new MongoClient(uri);
		mongodb = mongoClient.getDatabase("gestionK");*/
		
		mongoClient = new MongoClient();
		mongodb = mongoClient.getDatabase("gestionK");
	}

	public MongoClient getMongoClient() {
		return mongoClient;
	}

	public void setMongoClient(MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}

	public MongoDatabase getMongodb() {
		return mongodb;
	}

	public void setMongodb(MongoDatabase mongodb) {
		this.mongodb = mongodb;
	}
}
