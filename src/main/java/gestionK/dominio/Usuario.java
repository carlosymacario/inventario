package gestionK.dominio;

import java.io.IOException;
import java.util.Hashtable;

import javax.websocket.EncodeException;
import javax.websocket.Session;

import org.json.JSONObject;

import gestionK.dominio.Cliente;
import gestionK.dao.DAOUsuario;

public class Usuario {
	private String id;
	private String email;
	private Hashtable<String, Cliente> clientes;
	private Hashtable<String, Articulo> articulos;
	private Hashtable<String, Libro> libros;
	private Hashtable<String, Anyo> anyos;
	private Hashtable<String, Tique> tiques;
	private Hashtable<String, Factura> facturas;
	private Hashtable<String, Albaran> albaranes;
	private Hashtable<String, Presupuesto> presupuestos;
	private Session session;

	public Usuario(String login) {
		this.email=login;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id=id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email=email;
	}
	
	public Hashtable<String, Cliente> loadClientes() throws Exception {
		this.clientes=DAOUsuario.getClientes();
		return this.clientes;
	}
	
	public Hashtable<String, Articulo> loadArticulos() throws Exception {
		this.articulos=DAOUsuario.getArticulos();
		return this.articulos;
	}
	
	public Hashtable<String, Libro> loadLibros() throws Exception {
		this.libros=DAOUsuario.getLibros();
		return this.libros;
	}
	
	public Hashtable<String, Tique> loadTiques() throws Exception {
		this.tiques=DAOUsuario.getTiques();
		return this.tiques;
	}
	
	public Hashtable<String, Factura> loadFacturas() throws Exception {
		this.facturas=DAOUsuario.getFacturas();
		return this.facturas;
	}
	
	public Hashtable<String, Albaran> loadAlbaranes() throws Exception {
		this.albaranes=DAOUsuario.getAlbaranes();
		return this.albaranes;
	}
	
	public Hashtable<String, Presupuesto> loadPresupuestos() throws Exception {
		this.presupuestos=DAOUsuario.getPresupuestos();
		return this.presupuestos;
	}
	
	public Hashtable<String, Anyo> loadAnyos() throws Exception {
		this.anyos=DAOUsuario.getAnyos();
		return this.anyos;
	}
	
	public Hashtable<String, Cliente> loadBusquedaClientes(String nombreCliente, String apellidoCliente, String NIFCliente, String direccionCliente, String localidadCliente, String CPCliente, String provinciaCliente, String paisCliente, String telefonoCliente, String correoCliente) throws Exception {
		Hashtable<String, Cliente> busquedaClientes;
		busquedaClientes=DAOUsuario.getBusquedaClientes(nombreCliente, apellidoCliente, NIFCliente, direccionCliente, localidadCliente, CPCliente, provinciaCliente, paisCliente, telefonoCliente, correoCliente);
		return busquedaClientes;
	}
	
	public Hashtable<String, Articulo> loadBusquedaArticulos(String codigoArticulo, String nombreArticulo, int cantidadArticulo, double precioArticulo, double precioIVAArticulo) throws Exception {
		Hashtable<String, Articulo> busquedaArticulos;
		busquedaArticulos=DAOUsuario.getBusquedaArticulos(codigoArticulo, nombreArticulo, cantidadArticulo, precioArticulo, precioIVAArticulo);
		return busquedaArticulos;
	}
	
	public Hashtable<String, Libro> loadBusquedaLibros(String codigoLibro, String tituloLibro, String autorLibro, String editorialLibro, String distribuidorLibro, int cantidadLibro, double precioLibro, double precioIVALibro) throws Exception {
		Hashtable<String, Libro> busquedaLibros;
		busquedaLibros=DAOUsuario.getBusquedaLibros(codigoLibro, tituloLibro, autorLibro, editorialLibro, distribuidorLibro, cantidadLibro, precioLibro, precioIVALibro);
		return busquedaLibros;
	}
	
	public void setSession(Session session) {
		this.session=session;
	}	

	public Session getSession() {
		return session;
	}

	public void broadcast(JSONObject msgSalida) throws IOException, EncodeException {
		this.session.getRemote().sendObject(msgSalida.toString());
	}

}
