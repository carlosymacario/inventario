package gestionK.dominio;

import org.json.JSONObject;

public class Producto {
	private String id;
	private String codigo;
	private String nombre;
	private String precioIVA;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrecioIVA() {
		return precioIVA;
	}

	public void setPrecioIVA(String precioIVA) {
		this.precioIVA = precioIVA;
	}

	public JSONObject toJSONObject() {
		 JSONObject jso = new JSONObject();
		 jso.put("id", (Object)this.id);
		 jso.put("codigo", (Object)this.codigo);
		 jso.put("nombre", (Object)this.nombre);
		 jso.put("precioIVA", (Object)this.precioIVA);
		 return jso;
	 }
	
}
