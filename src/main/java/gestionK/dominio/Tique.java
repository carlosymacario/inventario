package gestionK.dominio;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

public class Tique {
	private int numero;
	private String id;
	private double total;
	private String fecha;
	private String anyo;
	private List<Document> productos;
	
	public Tique() {
		productos = new ArrayList<Document>();
	}

	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public String getAnyo() {
		return anyo;
	}

	public void setAnyo(String anyo) {
		this.anyo = anyo;
	}

	public List<Document> getProductos() {
		return productos;
	}

	public void setProductos(List<Document> productos) {
		this.productos = productos;
	}

	public JSONObject toJSONObject() {
		 JSONObject jso = new JSONObject();
		 jso.put("numero", (Object)this.numero);
		 jso.put("id", (Object)this.id);
		 jso.put("total", (Object)this.total);
		 jso.put("fecha", (Object)this.fecha);
		 jso.put("anyo", (Object)this.anyo);
		 jso.put("productos", (Object)this.productos);
		 return jso;
	 }
	
	public void toTique(JSONObject tiqueJSON) {
		this.numero = Integer.parseInt(tiqueJSON.get("numero").toString());
		this.id = tiqueJSON.get("id").toString();
		this.total = Double.parseDouble(tiqueJSON.get("total").toString());
		this.fecha = tiqueJSON.get("fecha").toString();
		this.anyo = tiqueJSON.get("anyo").toString();
		JSONArray productosJSON = new JSONArray();
		productosJSON = tiqueJSON.getJSONArray("productos");
		Document producto = new Document();
		for(int i=0; i<productosJSON.length(); i++) {
			producto = Document.parse(productosJSON.getJSONObject(i).toString());
			this.productos.add(producto);
		}
	}
	
}
