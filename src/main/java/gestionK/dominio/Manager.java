package gestionK.dominio;

import gestionK.dao.DAOAlbaran;
import gestionK.dao.DAOAnyo;
import gestionK.dao.DAOArticulo;
import gestionK.dao.DAOCliente;
import gestionK.dao.DAOFactura;
import gestionK.dao.DAOLibro;
import gestionK.dao.DAOPresupuesto;
import gestionK.dao.DAOProducto;
import gestionK.dao.DAOTique;
import gestionK.dominio.Cliente;
import gestionK.dominio.Usuario;
import gestionK.dao.DAOUsuario;
import gestionK.imprimir.EventoPaginaAlbaran;
import gestionK.imprimir.EventoPaginaFactura;
import gestionK.imprimir.EventoPaginaPresupuesto;
import gestionK.imprimir.ImprimirTique;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;

public class Manager {
	private static Manager yo;
    private Hashtable<String, Usuario> usuarios = new Hashtable();
    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
    DateFormat anyo = new SimpleDateFormat("yyyy");
    private Date date;
   

    private Manager() {
    }

    public static Manager get() {
        if (yo == null) {
            yo = new Manager();
        }
        return yo;
    }

    //Identificar
    public Usuario identificar(String login, String pwd) throws Exception {
        Usuario usuario = DAOUsuario.select((String)login, (String)pwd);
        usuario.loadClientes();
        usuario.loadArticulos();
        usuario.loadLibros();
        usuario.loadTiques();
        usuario.loadFacturas();
        usuario.loadAlbaranes();
        usuario.loadPresupuestos();
        usuario.loadAnyos();
       
        this.usuarios.put(usuario.getId(), usuario);
        return usuario;
    }
    
    public Usuario identificar(String idUsuario) throws Exception {
    	Usuario usuario = DAOUsuario.select(idUsuario);
    	usuario.loadClientes();
        usuario.loadArticulos();
        usuario.loadLibros();
        usuario.loadTiques();
        usuario.loadFacturas();
        usuario.loadAlbaranes();
        usuario.loadPresupuestos();
        usuario.loadAnyos();
    	
    	this.usuarios.put(usuario.getId(), usuario);
    	return usuario;
    }
    
    //Registrar
    public Usuario registrar(String login, String pwd, String pwd2) throws Exception {
    	Usuario usuario = null;
    	boolean existe = true;
    	if (pwd.equals(pwd2)) {
    		existe = DAOUsuario.comprobarUsuario(login);
    		if (!existe) {
    			usuario = DAOUsuario.insert(login, pwd);
            	usuario.loadClientes();
                usuario.loadArticulos();
                usuario.loadLibros();
                usuario.loadTiques();
                usuario.loadFacturas();
                usuario.loadAlbaranes();
                usuario.loadPresupuestos();
                usuario.loadAnyos();
                
                this.usuarios.put(usuario.getId(), usuario);
    		} else {
    			throw new Exception("Ese usuario ya existe");
    		}
    		
    	} else {
    		throw new Exception("Las contraseñas no coinciden.");
    	}
    	return usuario;
    }
    
    //Eliminar usuario
    public void eliminarUsuario(String login) {
    	DAOUsuario.delete(login);
    }
    
    //Conectar
    public Usuario conectar(String id) throws Exception {
        Usuario usuario = this.usuarios.get(id);
        if (usuario == null) {
            usuario = DAOUsuario.select(id);
            if (usuario == null) {
                throw new Exception("No se encuentra ese usuario");
            }
            this.usuarios.put(id, usuario);
        }
        return usuario;
    }
    
    //Años
    public void crearAnyo(String anyo, String abierto) throws Exception {
    	Anyo anyoNuevo = new Anyo();
    	anyoNuevo.setAnyo(anyo);
    	anyoNuevo.setAbierto(abierto);
    	DAOAnyo.insert((Anyo)anyoNuevo);
    }
    
    public void modificarAnyos(JSONArray anyos) throws Exception {
    	Document anyo = new Document();
    	for(int i=0; i<anyos.length(); i++){
    		anyo=Document.parse(anyos.getJSONObject((i)).toString());
    		DAOAnyo.update(anyo.get("anyo").toString(), anyo.get("abierto").toString());
    	}
    }
    
    public JSONArray getAnyosAsJSONArray(Usuario usuario) throws Exception {
        Hashtable anyos = usuario.loadAnyos();
        Enumeration eAnyos = anyos.elements();
        JSONArray result = new JSONArray();
        while (eAnyos.hasMoreElements()) {
            Anyo anyo = (Anyo)eAnyos.nextElement();
            result.put(anyo.toJSONObject());
        }
        return result;
    }
    
    //Cliente
    public JSONArray getClientesAsJSONArray(Usuario usuario) throws Exception {
        Hashtable clientes = usuario.loadClientes();
        Enumeration eClientes = clientes.elements();
        JSONArray result = new JSONArray();
        while (eClientes.hasMoreElements()) {
            Cliente cliente = (Cliente)eClientes.nextElement();
            result.put(cliente.toJSONObject());
        }
        return result;
    }
    
    public JSONArray getBusquedaClientesAsJSONArray(Usuario usuario, String nombreCliente, String apellidoCliente, String NIFCliente, String direccionCliente, String localidadCliente, String CPCliente, String provinciaCliente, String paisCliente, String telefonoCliente, String correoCliente) throws Exception {
        Hashtable clientes = usuario.loadBusquedaClientes(nombreCliente, apellidoCliente, NIFCliente, direccionCliente, localidadCliente, CPCliente, provinciaCliente, paisCliente, telefonoCliente, correoCliente);
        Enumeration eClientes = clientes.elements();
        JSONArray result = new JSONArray();
        while (eClientes.hasMoreElements()) {
            Cliente cliente = (Cliente)eClientes.nextElement();
            result.put(cliente.toJSONObject());
        }
        return result;
    }
    
    public String crearCliente(String nombreCliente, String apellidoCliente, String NIFCliente, String direccionCliente, String localidadCliente, String CPCliente, String provinciaCliente, String paisCliente, String telefonoCliente, String correoCliente) throws Exception {
        Cliente cliente = new Cliente();
        cliente.setNombre(nombreCliente);
        cliente.setApellido(apellidoCliente);
        cliente.setNIF(NIFCliente);
        cliente.setDireccion(direccionCliente);
        cliente.setLocalidad(localidadCliente);
        cliente.setCP(CPCliente);
        cliente.setProvincia(provinciaCliente);
        cliente.setPais(paisCliente);
        cliente.setTelefono(telefonoCliente);
        cliente.setCorreo(correoCliente);
        DAOCliente.insert((Cliente)cliente);
        return cliente.getId();
    }
    
    public void modificarCliente(String idCliente, String nombreCliente, String apellidoCliente, String NIFCliente, String direccionCliente, String localidadCliente, String CPCliente, String provinciaCliente, String paisCliente, String telefonoCliente, String correoCliente) throws Exception {
        Cliente cliente = new Cliente();
        cliente.setId(idCliente);
        cliente.setNombre(nombreCliente);
        cliente.setApellido(apellidoCliente);
        cliente.setNIF(NIFCliente);
        cliente.setDireccion(direccionCliente);
        cliente.setLocalidad(localidadCliente);
        cliente.setCP(CPCliente);
        cliente.setProvincia(provinciaCliente);
        cliente.setPais(paisCliente);
        cliente.setTelefono(telefonoCliente);
        cliente.setCorreo(correoCliente);
        DAOCliente.update((Cliente)cliente);
    }
    
    public void eliminarCliente(String idCliente) throws Exception {
        DAOCliente.delete(idCliente);
    }
    
    //Artículo
    public JSONArray getArticulosAsJSONArray(Usuario usuario) throws Exception {
        Hashtable articulos = usuario.loadArticulos();
        Enumeration eArticulos = articulos.elements();
        JSONArray result = new JSONArray();
        while (eArticulos.hasMoreElements()) {
            Articulo articulo = (Articulo)eArticulos.nextElement();
            result.put(articulo.toJSONObject());
        }
        return result;
    }
    
    public JSONArray getBusquedaArticulosAsJSONArray(Usuario usuario, String codigoArticulo, String nombreArticulo, int cantidadArticulo, double precioArticulo, double precioIVAArticulo) throws Exception {
        Hashtable articulos = usuario.loadBusquedaArticulos(codigoArticulo, nombreArticulo, cantidadArticulo, precioArticulo, precioIVAArticulo);
        Enumeration eArticulos = articulos.elements();
        JSONArray result = new JSONArray();
        while (eArticulos.hasMoreElements()) {
            Articulo articulo = (Articulo)eArticulos.nextElement();
            result.put(articulo.toJSONObject());
        }
        return result;
    }
    
    public String selectArticulo(String codigoArticulo) throws Exception {
    	String id = "";
    	Articulo articulo = new Articulo();
    	articulo = DAOArticulo.selectArticuloTique(codigoArticulo);
    	id = articulo.getId();
    	return id;
    }
    
    public String crearArticulo(String codigoArticulo, String nombreArticulo, int cantidadArticulo, double precioArticulo, double precioIVAArticulo) throws Exception {
    	boolean existe = true;
    	existe = DAOArticulo.comprobarCodigo(codigoArticulo);
    	
    	date = new Date();
    	Articulo articulo = new Articulo();
    	
    	if (!existe) {
    		articulo.setCodigo(codigoArticulo);
            articulo.setNombre(nombreArticulo);
            articulo.setFechaAlta(hourdateFormat.format(date));
            articulo.setFechaModificacion(hourdateFormat.format(date));
            articulo.setCantidad(cantidadArticulo);
            articulo.setPrecio(precioIVAArticulo*0.79);
            articulo.setPrecioIVA(precioIVAArticulo);
            DAOArticulo.insert((Articulo)articulo);
    	} else {
    		throw new Exception("Este articulo ya está dado de alta");
    	}
    	
        return articulo.getId();
    }
    
    public void modificarArticulo(String idArticulo, String codigoArticulo, String nombreArticulo, int cantidadArticulo, double precioArticulo, double precioIVAArticulo) throws Exception {
    	date = new Date();
    	Articulo articulo = new Articulo();
    	articulo.setId(idArticulo);
    	articulo.setCodigo(codigoArticulo);
        articulo.setNombre(nombreArticulo);
        articulo.setFechaModificacion(hourdateFormat.format(date));
        articulo.setCantidad(cantidadArticulo);
        articulo.setPrecio(precioIVAArticulo*0.79);
        articulo.setPrecioIVA(precioIVAArticulo);
        DAOArticulo.update((Articulo)articulo);
    }
    
    public void eliminarArticulo(String idArticulo) throws Exception {
        DAOArticulo.delete(idArticulo);
    }
    
    //Libro
    public JSONArray getLibrosAsJSONArray(Usuario usuario) throws Exception {
        Hashtable libros = usuario.loadLibros();
        Enumeration eLibros = libros.elements();
        JSONArray result = new JSONArray();
        while (eLibros.hasMoreElements()) {
            Libro libro = (Libro)eLibros.nextElement();
            result.put(libro.toJSONObject());
        }
        return result;
    }
    
    public JSONArray getBusquedaLibrosAsJSONArray(Usuario usuario, String codigoLibro, String tituloLibro, String autorLibro, String editorialLibro, String distribuidorLibro, int cantidadLibro, double precioLibro, double precioIVALibro) throws Exception {
        Hashtable libros = usuario.loadBusquedaLibros(codigoLibro, tituloLibro, autorLibro, editorialLibro, distribuidorLibro, cantidadLibro, precioLibro, precioIVALibro);
        Enumeration eLibros = libros.elements();
        JSONArray result = new JSONArray();
        while (eLibros.hasMoreElements()) {
            Libro libro = (Libro)eLibros.nextElement();
            result.put(libro.toJSONObject());
        }
        return result;
    }
    
    public String selectLibro(String codigoLibro) throws Exception {
    	String id = "";
    	Libro libro = new Libro();
    	libro = DAOLibro.selectLibroTique(codigoLibro);
    	id = libro.getId();
    	return id;
    }
    
    public String crearLibro(String codigoLibro, String tituloLibro, String autorLibro, String editorialLibro, String distribuidorLibro, int cantidadLibro, double precioLibro, double precioIVALibro) throws Exception {
    	boolean existe = true;
    	existe = DAOLibro.comprobarCodigo(codigoLibro);
    	
    	date = new Date();
    	Libro libro = new Libro();
    	
    	if (!existe) {
    		libro.setCodigo(codigoLibro);
            libro.setTitulo(tituloLibro);
            libro.setAutor(autorLibro);
            libro.setEditorial(editorialLibro);
            libro.setDistribuidor(distribuidorLibro);
            libro.setFechaAlta(hourdateFormat.format(date));
            libro.setFechaModificacion(hourdateFormat.format(date));
            libro.setCantidad(cantidadLibro);
            libro.setPrecio(precioIVALibro*0.96);
            libro.setPrecioIVA(precioIVALibro);
            DAOLibro.insert((Libro)libro);
    	} else {
    		throw new Exception("Este libro ya está dado de alta");
    	}
        
        return libro.getId();
    }
    
    public void modificarLibro(String idLibro, String codigoLibro, String tituloLibro, String autorLibro, String editorialLibro, String distribuidorLibro, int cantidadLibro, double precioLibro, double precioIVALibro) throws Exception {
    	date = new Date();
    	Libro libro = new Libro();
    	libro.setId(idLibro);
    	libro.setCodigo(codigoLibro);
        libro.setTitulo(tituloLibro);
        libro.setAutor(autorLibro);
        libro.setEditorial(editorialLibro);
        libro.setDistribuidor(distribuidorLibro);
        libro.setFechaModificacion(hourdateFormat.format(date));
        libro.setCantidad(cantidadLibro);
        libro.setPrecio(precioIVALibro*0.96);
        libro.setPrecioIVA(precioIVALibro);
        DAOLibro.update((Libro)libro);
    }
    
    public void eliminarLibro(String idLibro) throws Exception {
        DAOLibro.delete(idLibro);
    }
    
    //Tique
    public JSONArray getTiquesAsJSONArray(Usuario usuario) throws Exception {
        Hashtable tiques = usuario.loadTiques();
        Enumeration eTiques = tiques.elements();
        JSONArray resultOrdenado = new JSONArray();
        JSONArray result = new JSONArray();
        while (eTiques.hasMoreElements()) {
            Tique tique = (Tique)eTiques.nextElement();
            result.put(tique.toJSONObject());
        }
        resultOrdenado = ordenarArrayTiques(result);
        return resultOrdenado;
    }
    
    public Tique crearTique(double totalTique, JSONArray productosTique) throws Exception {
    	Tique tique = new Tique();
    	double totalComprobado = 0;
    	double importe = 0;
    	int cantidad = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document producto = new Document();
    	date = new Date();
    	tique.setNumero(DAOTique.numeroTiqueNuevo(DAOAnyo.selectAnyoAbierto()));
    	tique.setFecha(hourdateFormat.format(date));
    	tique.setAnyo(anyo.format(date));
    	for(int i=0; i<productosTique.length(); i++){
    		producto=Document.parse(productosTique.getJSONObject(i).toString());
    		if (!producto.get("codigo").toString().equals("")){
    			cantidad = Integer.parseInt(producto.get("cantidad").toString());
    			importe = cantidad * Double.parseDouble(producto.get("precio").toString());
    			producto.put("importe", importe);
    			totalComprobado = totalComprobado + importe;
    			DAOProducto.update(producto.get("codigo").toString(), producto.get("cantidad").toString());
    			productos.add(producto);
    		}
    	}
    	tique.setTotal(totalComprobado);
    	tique.setProductos(productos);
    	DAOTique.insert(tique);
    	return tique;
    }
    
    public Tique modificarTique(double totalTiqueNuevo, JSONArray productosTiqueNuevo, JSONArray productosTiqueSession, String idTiqueSession) throws Exception {
    	Tique tique = new Tique();
    	double importe = 0;
    	double totalComprobado = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document productoNuevo = new Document();
    	Document productoSession = new Document();
    	date = new Date();
    	tique.setId(idTiqueSession);
    	tique.setFecha(hourdateFormat.format(date));
    	tique.setAnyo(anyo.format(date));
    	int cantidad = 0;
    	for(int i=0; i<productosTiqueSession.length(); i++) {
    		productoSession = Document.parse(productosTiqueSession.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(productoSession.get("cantidad").toString());
    		DAOProducto.modificado(productoSession.get("codigo").toString(), cantidad);
    	}
    	for(int i=0; i<productosTiqueNuevo.length(); i++) {
    		productoNuevo = Document.parse(productosTiqueNuevo.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(productoNuevo.get("cantidad").toString());
			importe = cantidad * Double.parseDouble(productoNuevo.get("precio").toString());
			productoNuevo.put("importe", importe);
			totalComprobado = totalComprobado + importe;
    		DAOProducto.update(productoSession.get("codigo").toString(), productoNuevo.get("cantidad").toString());
    		productos.add(productoNuevo);
    	}
    	tique.setTotal(totalComprobado);
    	tique.setProductos(productos);
    	DAOTique.update(tique);
    	return tique;
    }
    
    public void eliminarTique(String idTique, JSONArray productosTique) throws Exception {
    	Document producto = new Document();
    	int cantidad = 0;
    	for(int i=0; i<productosTique.length(); i++){
    		producto = Document.parse(productosTique.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(producto.get("cantidad").toString());
    		DAOProducto.modificado(producto.get("codigo").toString(), cantidad);
    	}
    	DAOTique.delete(idTique);
    }
    
    public void imprimirTique(JSONObject tiqueJSON) throws Exception {
    	Tique tique = new Tique();
    	tique.toTique(tiqueJSON);
    	String cantidad;
		String nombre;
		String precio;
		String importe;
		
    	Date date=new Date();
		SimpleDateFormat horaFecha=new SimpleDateFormat("hh:mm:ss dd/MM/yyyy");
		SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		date = horaFecha.parse(tique.getFecha());
		ImprimirTique imprimirTique = new ImprimirTique();
		imprimirTique.AddCabecera("PAPELERIA LIBRERIA KALKOS");
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		imprimirTique.AddCabecera("Cristo de Urda, 25");
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		imprimirTique.AddCabecera("13640 Herencia, Ciudad Real");
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		imprimirTique.AddCabecera("NIF: 06221945P");
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		imprimirTique.AddCabecera("TFNO: 926 57 32 73");
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		imprimirTique.AddCabecera("Tique Nº " + tique.getNumero() + "\t " + fecha.format(date));
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		imprimirTique.AddCabecera(imprimirTique.DarEspacio());
		
		imprimirTique.AddItem("Artículo\t", "Uds\t", "Precio\t", "Importe");
		imprimirTique.AddItem("", "", "", imprimirTique.DarEspacio());
		imprimirTique.AddItem(imprimirTique.DibujarLinea(15) + "\t", imprimirTique.DibujarLinea(3) + "\t", imprimirTique.DibujarLinea(6) + "\t", imprimirTique.DibujarLinea(7));
		imprimirTique.AddItem("", "", "", imprimirTique.DarEspacio());
		
		for(int i=0; i<tique.getProductos().size(); i++) {
			nombre = tique.getProductos().get(i).get("nombre").toString();
			if(nombre.length() > 15) {
				nombre = nombre.substring(0, 12) + "...";
			}
			cantidad = tique.getProductos().get(i).get("cantidad").toString();
			precio = tique.getProductos().get(i).get("precio").toString();
			importe = tique.getProductos().get(i).get("importe").toString();
			imprimirTique.AddItem(nombre + "\t", cantidad + "\t", precio + "\t", importe);
			imprimirTique.AddItem("", "", "", imprimirTique.DarEspacio());
		}
		
		imprimirTique.AddTotal("", imprimirTique.DarEspacio());
		imprimirTique.AddTotal("TOTAL", String.valueOf(tique.getTotal()));
		imprimirTique.AddTotal("",imprimirTique.DarEspacio());
		
		imprimirTique.AddPieLinea(imprimirTique.DarEspacio());
		imprimirTique.AddPieLinea("Le atendió: KALKOS");
		imprimirTique.AddPieLinea(imprimirTique.DarEspacio());
		imprimirTique.AddPieLinea(imprimirTique.DarEspacio());
		imprimirTique.AddPieLinea(fechaHora.format(date));
		imprimirTique.AddPieLinea(imprimirTique.DarEspacio());
		imprimirTique.AddPieLinea(imprimirTique.DarEspacio());
		imprimirTique.ImprimirDocumento();
		imprimirTique.vaciarArrays();
    }
    
    //Factura
    public JSONArray getFacturasAsJSONArray(Usuario usuario) throws Exception {
        Hashtable facturas = usuario.loadFacturas();
        Enumeration eFacturas = facturas.elements();
        JSONArray result = new JSONArray();
        JSONArray resultOrdenado = new JSONArray();
        while (eFacturas.hasMoreElements()) {
            Factura factura = (Factura)eFacturas.nextElement();
            result.put(factura.toJSONObject());
        }
        resultOrdenado = ordenarArrayTiques(result);
        return resultOrdenado;
    }
    
    public Factura crearFactura(double totalFactura, JSONArray productosFactura, JSONObject clienteFactura) throws Exception {
    	Factura factura = new Factura();
    	double totalComprobado = 0;
    	double importe = 0;
    	int cantidad = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document producto = new Document();
    	Cliente cliente = new Cliente(clienteFactura);
    	date = new Date();
    	factura.setNumero(DAOFactura.numeroFacturaNuevo(DAOAnyo.selectAnyoAbierto()));
    	factura.setTotal(totalFactura);
    	factura.setFecha(hourdateFormat.format(date));
    	factura.setAnyo(anyo.format(date));
    	for(int i=0; i<productosFactura.length(); i++){
    		producto=Document.parse(productosFactura.getJSONObject(i).toString());
    		if (!producto.get("codigo").toString().equals("")){
    			cantidad = Integer.parseInt(producto.get("cantidad").toString());
    			importe = cantidad * Double.parseDouble(producto.get("precio").toString());
    			producto.put("importe", importe);
    			totalComprobado = totalComprobado + importe;
    			DAOProducto.update(producto.get("codigo").toString(), producto.get("cantidad").toString());
    			productos.add(producto);
    		}
    	}
    	factura.setProductos(productos);
    	factura.setCliente(cliente);
    	DAOFactura.insert(factura);
    	return factura;
    }
    
    public Factura modificarFactura(double totalFacturaNuevo, JSONArray productosFacturaNuevo, JSONObject clienteFacturaNuevo, JSONArray productosFacturaSession, String idFacturaSession, JSONObject clienteFacturaSession) throws Exception {
    	Factura factura = new Factura();
    	Cliente cliente = null;
    	double totalComprobado = 0;
    	double importe = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document productoNuevo = new Document();
    	Document productoSession = new Document();
    	date = new Date();
    	factura.setId(idFacturaSession);
    	factura.setFecha(hourdateFormat.format(date));
    	factura.setAnyo(anyo.format(date));
    	if (clienteFacturaNuevo == null) {
    		cliente = new Cliente(clienteFacturaSession);
        	factura.setCliente(cliente);
    	} else {
    		cliente = new Cliente(clienteFacturaNuevo);
        	factura.setCliente(cliente);
    	}
    	int cantidad = 0;
    	for(int i=0; i<productosFacturaSession.length(); i++) {
    		productoSession = Document.parse(productosFacturaSession.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(productoSession.get("cantidad").toString());
    		DAOProducto.modificado(productoSession.get("codigo").toString(), cantidad);
    	}
    	for(int i=0; i<productosFacturaNuevo.length(); i++) {
    		productoNuevo = Document.parse(productosFacturaNuevo.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(productoNuevo.get("cantidad").toString());
			importe = cantidad * Double.parseDouble(productoNuevo.get("precio").toString());
			productoNuevo.put("importe", importe);
			totalComprobado = totalComprobado + importe;
    		DAOProducto.update(productoNuevo.get("codigo").toString(), productoNuevo.get("cantidad").toString());
    		productos.add(productoNuevo);
    	}
    	factura.setTotal(totalComprobado);
    	factura.setProductos(productos);
    	DAOFactura.update(factura);
    	return factura;
    }
    
    public void eliminarFactura(String idFactura, JSONArray productosFactura) throws Exception {
    	Document producto = new Document();
    	int cantidad = 0;
    	for(int i=0; i<productosFactura.length(); i++){
    		producto = Document.parse(productosFactura.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(producto.get("cantidad").toString());
    		DAOProducto.modificado(producto.get("codigo").toString(), cantidad);
    	}
    	DAOFactura.delete(idFactura);
    }
    
    public void imprimirFactura(JSONObject facturaJSON) throws Exception {
        Factura factura = new Factura(facturaJSON);
        List<Document> productos = new ArrayList<Document>();
        JSONObject imprimirPie = new JSONObject();
        productos = factura.getProductos();
        List<Document> productosImprimir = new ArrayList<Document>();
    	Document producto = new Document();
    	String articuloLibro;
    	double precio = 0;
    	double precioSinIVA = 0;
    	double importe = 0;
    	double importeSinIVAArticulo = 0;
    	double importeSinIVALibro = 0;
    	double totalSinIVAArticulo = 0;
    	double totalSinIVALibro = 0;
    	double cuotaIVAArticulo = 0;
    	double cuotaIVALibro = 0;
    	int keyArticulo = 0;
    	int keyLibro = 0;
    	for(int i=0; i<productos.size(); i++) {
    		producto = productos.get(i);
			articuloLibro = DAOProducto.selectArticuloLibro(producto.get("codigo").toString());
			if (articuloLibro.equals("articulo")) {
				producto.put("IVA", "21");
				precio = Double.valueOf(producto.get("precio").toString());
				precioSinIVA = precio*0.79;
				producto.put("precioSinIVA", String.valueOf(precioSinIVA));
				importe = Double.valueOf(producto.get("importe").toString());
				importeSinIVAArticulo = importe*0.79;
				producto.put("importeSinIVA", String.valueOf(importeSinIVAArticulo));
				keyArticulo++;
				totalSinIVAArticulo = totalSinIVAArticulo + importeSinIVAArticulo;
			} else if (articuloLibro.equals("libro")) {
				producto.put("IVA", "4");
				precio = Double.valueOf(producto.get("precio").toString());
				precioSinIVA = precio*0.96;
				producto.put("precioSinIVA", String.valueOf(precioSinIVA));
				importe = Double.valueOf(producto.get("importe").toString());
				importeSinIVALibro = importe*0.96;
				producto.put("importeSinIVA", String.valueOf(importeSinIVALibro));
				keyLibro++;
				totalSinIVALibro = totalSinIVALibro + importeSinIVALibro;
			}
    		productosImprimir.add(producto);
    	}
    	
    	if (keyArticulo > 0 && keyLibro == 0) {
    		imprimirPie.put("IVA", "21");
    		imprimirPie.put("IVAS", "1");
    	} else if (keyLibro > 0 && keyArticulo == 0) {
    		imprimirPie.put("IVA", "4");
    		imprimirPie.put("IVAS", "1");
    	} else if (keyArticulo > 0 && keyLibro > 0) {
    		imprimirPie.put("IVAS", "2");
    	}
    	cuotaIVAArticulo = 21*totalSinIVAArticulo/79;
    	cuotaIVALibro = 4*totalSinIVALibro/96;
    	
    	imprimirPie.put("totalSinIVAArticulo", String.valueOf(totalSinIVAArticulo));
    	imprimirPie.put("totalSinIVALibro", String.valueOf(totalSinIVALibro));
    	imprimirPie.put("cuotaIVAArticulo", String.valueOf(cuotaIVAArticulo));
    	imprimirPie.put("cuotaIVALibro", String.valueOf(cuotaIVALibro));
        
        
    	try {
            // Creamos el escritor del archivo pdf
            PdfWriter pdfWriter = new PdfWriter("./PDF/Facturas/Factura_" + factura.getNumero() + ".pdf");
            // Creamos el documento PDF que se almacenara via el escritor
            PdfDocument pdfDoc = new PdfDocument(pdfWriter);
            // Creamos el documento pdf en si, con pagina tamaño letra
            com.itextpdf.layout.Document doc = new com.itextpdf.layout.Document(pdfDoc, PageSize.LETTER);
            // Creamos el manejador de evento de pagina, el cual agregara
            // el encabezado y pie de pagina
            EventoPaginaFactura evento = new EventoPaginaFactura(doc, factura, imprimirPie);
 
            // Indicamos que el manejador se encargara del evento END_PAGE
            pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, evento);
             
            // Establecemos los margenes
            doc.setMargins(250, 36, 200, 36);
            
            float[] anchos = {75F, 215F, 40F, 50F, 50F, 50F};
            Table tablaProductos = new Table(anchos);
            tablaProductos.setWidth(527F);
            
            // Creamos el contenido
            for (int i = 0; i < productosImprimir.size(); i++) {
            	tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("codigo").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("nombre").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("IVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("cantidad").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("precioSinIVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("importeSinIVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
            }
            
            doc.add(tablaProductos);
 
            // Cerramos el documento
            doc.close();
 
        } catch (FileNotFoundException ex) {
        	System.out.println("Error: " + ex.getMessage());
        }
    }
    
    //Albarán
    public JSONArray getAlbaranesAsJSONArray(Usuario usuario) throws Exception {
        Hashtable albaranes = usuario.loadAlbaranes();
        Enumeration eAlbaranes = albaranes.elements();
        JSONArray result = new JSONArray();
        JSONArray resultOrdenado = new JSONArray();
        while (eAlbaranes.hasMoreElements()) {
            Albaran albaran = (Albaran)eAlbaranes.nextElement();
            result.put(albaran.toJSONObject());
        }
        resultOrdenado = ordenarArrayTiques(result);
        return resultOrdenado;
    }
    
    public Albaran crearAlbaran(double totalAlbaran, JSONArray productosAlbaran, JSONObject clienteAlbaran) throws Exception {
    	Albaran albaran = new Albaran();
    	double totalComprobado = 0;
    	double importe = 0;
    	int cantidad = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document producto = new Document();
    	Cliente cliente = new Cliente(clienteAlbaran);
    	date = new Date();
    	albaran.setNumero(DAOAlbaran.numeroAlbaranNuevo(DAOAnyo.selectAnyoAbierto()));
    	albaran.setFecha(hourdateFormat.format(date));
    	albaran.setAnyo(anyo.format(date));
    	for(int i=0; i<productosAlbaran.length(); i++){
    		producto=Document.parse(productosAlbaran.getJSONObject(i).toString());
    		if (!producto.get("codigo").toString().equals("")){
    			cantidad = Integer.parseInt(producto.get("cantidad").toString());
    			importe = cantidad * Double.parseDouble(producto.get("precio").toString());
    			producto.put("importe", importe);
    			totalComprobado = totalComprobado + importe;
    			productos.add(producto);
    		}
    	}
    	albaran.setTotal(totalComprobado);
    	albaran.setProductos(productos);
    	albaran.setCliente(cliente);
    	DAOAlbaran.insert(albaran);
    	return albaran;
    }
    
    public Albaran modificarAlbaran(double totalAlbaranNuevo, JSONArray productosAlbaranNuevo, JSONObject clienteAlbaranNuevo, JSONArray productosAlbaranSession, String idAlbaranSession, JSONObject clienteAlbaranSession) throws Exception {
    	Albaran albaran = new Albaran();
    	Cliente cliente = null;
    	double totalComprobado = 0;
    	double importe = 0;
    	int cantidad = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document productoNuevo = new Document();
    	date = new Date();
    	albaran.setId(idAlbaranSession);
    	albaran.setFecha(hourdateFormat.format(date));
    	albaran.setAnyo(anyo.format(date));
    	if (clienteAlbaranNuevo == null) {
    		cliente = new Cliente(clienteAlbaranSession);
        	albaran.setCliente(cliente);
    	} else {
    		cliente = new Cliente(clienteAlbaranNuevo);
        	albaran.setCliente(cliente);
    	}
    	for(int i=0; i<productosAlbaranNuevo.length(); i++) {
    		productoNuevo = Document.parse(productosAlbaranNuevo.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(productoNuevo.get("cantidad").toString());
			importe = cantidad * Double.parseDouble(productoNuevo.get("precio").toString());
			productoNuevo.put("importe", importe);
			totalComprobado = totalComprobado + importe;
    		productos.add(productoNuevo);
    	}
    	albaran.setTotal(totalComprobado);
    	albaran.setProductos(productos);
    	DAOAlbaran.update(albaran);
    	return albaran;
    }
    
    public void eliminarAlbaran(String idAlbaran) throws Exception {
    	DAOAlbaran.delete(idAlbaran);
    }
    
    public void imprimirAlbaran(JSONObject albaranJSON) throws Exception {
        Albaran albaran = new Albaran(albaranJSON);
        List<Document> productos = new ArrayList<Document>();
        JSONObject imprimirPie = new JSONObject();
        productos = albaran.getProductos();
        List<Document> productosImprimir = new ArrayList<Document>();
    	Document producto = new Document();
    	String articuloLibro;
    	double precio = 0;
    	double precioSinIVA = 0;
    	double importe = 0;
    	double importeSinIVAArticulo = 0;
    	double importeSinIVALibro = 0;
    	double totalSinIVAArticulo = 0;
    	double totalSinIVALibro = 0;
    	double cuotaIVAArticulo = 0;
    	double cuotaIVALibro = 0;
    	int keyArticulo = 0;
    	int keyLibro = 0;
    	for(int i=0; i<productos.size(); i++) {
    		producto = productos.get(i);
			articuloLibro = DAOProducto.selectArticuloLibro(producto.get("codigo").toString());
			if (articuloLibro.equals("articulo")) {
				producto.put("IVA", "21");
				precio = Double.valueOf(producto.get("precio").toString());
				precioSinIVA = precio*0.79;
				producto.put("precioSinIVA", String.valueOf(precioSinIVA));
				importe = Double.valueOf(producto.get("importe").toString());
				importeSinIVAArticulo = importe*0.79;
				producto.put("importeSinIVA", String.valueOf(importeSinIVAArticulo));
				keyArticulo++;
				totalSinIVAArticulo = totalSinIVAArticulo + importeSinIVAArticulo;
			} else if (articuloLibro.equals("libro")) {
				producto.put("IVA", "4");
				precio = Double.valueOf(producto.get("precio").toString());
				precioSinIVA = precio*0.96;
				producto.put("precioSinIVA", String.valueOf(precioSinIVA));
				importe = Double.valueOf(producto.get("importe").toString());
				importeSinIVALibro = importe*0.96;
				producto.put("importeSinIVA", String.valueOf(importeSinIVALibro));
				keyLibro++;
				totalSinIVALibro = totalSinIVALibro + importeSinIVALibro;
			}
    		productosImprimir.add(producto);
    	}
    	
    	if (keyArticulo > 0 && keyLibro == 0) {
    		imprimirPie.put("IVA", "21");
    		imprimirPie.put("IVAS", "1");
    	} else if (keyLibro > 0 && keyArticulo == 0) {
    		imprimirPie.put("IVA", "4");
    		imprimirPie.put("IVAS", "1");
    	} else if (keyArticulo > 0 && keyLibro > 0) {
    		imprimirPie.put("IVAS", "2");
    	}
    	cuotaIVAArticulo = 21*totalSinIVAArticulo/79;
    	cuotaIVALibro = 4*totalSinIVALibro/96;
    	
    	imprimirPie.put("totalSinIVAArticulo", String.valueOf(totalSinIVAArticulo));
    	imprimirPie.put("totalSinIVALibro", String.valueOf(totalSinIVALibro));
    	imprimirPie.put("cuotaIVAArticulo", String.valueOf(cuotaIVAArticulo));
    	imprimirPie.put("cuotaIVALibro", String.valueOf(cuotaIVALibro));
        
        
    	try {
            // Creamos el escritor del archivo pdf
            PdfWriter pdfWriter = new PdfWriter("./PDF/Albaranes/Albaran_" + albaran.getNumero() + ".pdf");
            // Creamos el documento PDF que se almacenara via el escritor
            PdfDocument pdfDoc = new PdfDocument(pdfWriter);
            // Creamos el documento pdf en si, con pagina tamaño letra
            com.itextpdf.layout.Document doc = new com.itextpdf.layout.Document(pdfDoc, PageSize.LETTER);
            // Creamos el manejador de evento de pagina, el cual agregara
            // el encabezado y pie de pagina
            EventoPaginaAlbaran evento = new EventoPaginaAlbaran(doc, albaran, imprimirPie);
 
            // Indicamos que el manejador se encargara del evento END_PAGE
            pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, evento);
             
            // Establecemos los margenes
            doc.setMargins(250, 36, 200, 36);
            
            float[] anchos = {75F, 215F, 40F, 50F, 50F, 50F};
            Table tablaProductos = new Table(anchos);
            tablaProductos.setWidth(527F);
            
            // Creamos el contenido
            for (int i = 0; i < productosImprimir.size(); i++) {
            	tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("codigo").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("nombre").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("IVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("cantidad").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("precioSinIVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("importeSinIVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
            }
            
            doc.add(tablaProductos);
 
            // Cerramos el documento
            doc.close();
 
        } catch (FileNotFoundException ex) {
        	System.out.println("Error: " + ex.getMessage());
        }
    }
    
    //Presupuesto
    public JSONArray getPresupuestosAsJSONArray(Usuario usuario) throws Exception {
        Hashtable presupuestos = usuario.loadPresupuestos();
        Enumeration ePresupuestos = presupuestos.elements();
        JSONArray result = new JSONArray();
        JSONArray resultOrdenado = new JSONArray();
        while (ePresupuestos.hasMoreElements()) {
            Presupuesto presupuesto = (Presupuesto)ePresupuestos.nextElement();
            result.put(presupuesto.toJSONObject());
        }
        resultOrdenado = ordenarArrayTiques(result);
        return resultOrdenado;
    }
    
    public Presupuesto crearPresupuesto(double totalPresupuesto, JSONArray productosPresupuesto, JSONObject clientePresupuesto) throws Exception {
    	Presupuesto presupuesto = new Presupuesto();
    	double totalComprobado = 0;
    	double importe = 0;
    	int cantidad = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document producto = new Document();
    	Cliente cliente = new Cliente(clientePresupuesto);
    	date = new Date();
    	presupuesto.setNumero(DAOPresupuesto.numeroPresupuestoNuevo(DAOAnyo.selectAnyoAbierto()));
    	presupuesto.setFecha(hourdateFormat.format(date));
    	presupuesto.setAnyo(anyo.format(date));
    	for(int i=0; i<productosPresupuesto.length(); i++){
    		producto=Document.parse(productosPresupuesto.getJSONObject(i).toString());
    		if (!producto.get("codigo").toString().equals("")){
    			cantidad = Integer.parseInt(producto.get("cantidad").toString());
    			importe = cantidad * Double.parseDouble(producto.get("precio").toString());
    			producto.put("importe", importe);
    			totalComprobado = totalComprobado + importe;
    			productos.add(producto);
    		}
    	}
    	presupuesto.setTotal(totalComprobado);
    	presupuesto.setProductos(productos);
    	presupuesto.setCliente(cliente);
    	DAOPresupuesto.insert(presupuesto);
    	return presupuesto;
    }
    
    public Presupuesto modificarPresupuesto(double totalPresupuestoNuevo, JSONArray productosPresupuestoNuevo, JSONObject clientePresupuestoNuevo, JSONArray productosPresupuestoSession, String idPresupuestoSession, JSONObject clientePresupuestoSession) throws Exception {
    	Presupuesto presupuesto = new Presupuesto();
    	Cliente cliente = new Cliente();
    	double totalComprobado = 0;
    	double importe = 0;
    	int cantidad = 0;
    	List<Document> productos = new ArrayList<Document>();
    	Document productoNuevo = new Document();
    	date = new Date();
    	presupuesto.setId(idPresupuestoSession);
    	presupuesto.setFecha(hourdateFormat.format(date));
    	presupuesto.setAnyo(anyo.format(date));
    	if (clientePresupuestoNuevo == null) {
    		cliente = new Cliente(clientePresupuestoSession);
        	presupuesto.setCliente(cliente);
    	} else {
    		cliente = new Cliente(clientePresupuestoNuevo);
        	presupuesto.setCliente(cliente);
    	}
    	for(int i=0; i<productosPresupuestoNuevo.length(); i++) {
    		productoNuevo = Document.parse(productosPresupuestoNuevo.getJSONObject(i).toString());
    		cantidad = Integer.parseInt(productoNuevo.get("cantidad").toString());
			importe = cantidad * Double.parseDouble(productoNuevo.get("precio").toString());
			productoNuevo.put("importe", importe);
			totalComprobado = totalComprobado + importe;
    		productos.add(productoNuevo);
    	}
    	presupuesto.setTotal(totalComprobado);
    	presupuesto.setProductos(productos);
    	DAOPresupuesto.update(presupuesto);
    	return presupuesto;
    }
    
    public void eliminarPresupuesto(String idPresupuesto) throws Exception {
    	DAOPresupuesto.delete(idPresupuesto);
    }
    
    public void imprimirPresupuesto(JSONObject presupuestoJSON) throws Exception {
        Presupuesto presupuesto = new Presupuesto(presupuestoJSON);
        List<Document> productos = new ArrayList<Document>();
        JSONObject imprimirPie = new JSONObject();
        productos = presupuesto.getProductos();
        List<Document> productosImprimir = new ArrayList<Document>();
    	Document producto = new Document();
    	String articuloLibro;
    	double precio = 0;
    	double precioSinIVA = 0;
    	double importe = 0;
    	double importeSinIVAArticulo = 0;
    	double importeSinIVALibro = 0;
    	double totalSinIVAArticulo = 0;
    	double totalSinIVALibro = 0;
    	double cuotaIVAArticulo = 0;
    	double cuotaIVALibro = 0;
    	int keyArticulo = 0;
    	int keyLibro = 0;
    	for(int i=0; i<productos.size(); i++) {
    		producto = productos.get(i);
			articuloLibro = DAOProducto.selectArticuloLibro(producto.get("codigo").toString());
			if (articuloLibro.equals("articulo")) {
				producto.put("IVA", "21");
				precio = Double.valueOf(producto.get("precio").toString());
				precioSinIVA = precio*0.79;
				producto.put("precioSinIVA", String.valueOf(precioSinIVA));
				importe = Double.valueOf(producto.get("importe").toString());
				importeSinIVAArticulo = importe*0.79;
				producto.put("importeSinIVA", String.valueOf(importeSinIVAArticulo));
				keyArticulo++;
				totalSinIVAArticulo = totalSinIVAArticulo + importeSinIVAArticulo;
			} else if (articuloLibro.equals("libro")) {
				producto.put("IVA", "4");
				precio = Double.valueOf(producto.get("precio").toString());
				precioSinIVA = precio*0.96;
				producto.put("precioSinIVA", String.valueOf(precioSinIVA));
				importe = Double.valueOf(producto.get("importe").toString());
				importeSinIVALibro = importe*0.96;
				producto.put("importeSinIVA", String.valueOf(importeSinIVALibro));
				keyLibro++;
				totalSinIVALibro = totalSinIVALibro + importeSinIVALibro;
			}
    		productosImprimir.add(producto);
    	}
    	
    	if (keyArticulo > 0 && keyLibro == 0) {
    		imprimirPie.put("IVA", "21");
    		imprimirPie.put("IVAS", "1");
    	} else if (keyLibro > 0 && keyArticulo == 0) {
    		imprimirPie.put("IVA", "4");
    		imprimirPie.put("IVAS", "1");
    	} else if (keyArticulo > 0 && keyLibro > 0) {
    		imprimirPie.put("IVAS", "2");
    	}
    	cuotaIVAArticulo = 21*totalSinIVAArticulo/79;
    	cuotaIVALibro = 4*totalSinIVALibro/96;
    	
    	imprimirPie.put("totalSinIVAArticulo", String.valueOf(totalSinIVAArticulo));
    	imprimirPie.put("totalSinIVALibro", String.valueOf(totalSinIVALibro));
    	imprimirPie.put("cuotaIVAArticulo", String.valueOf(cuotaIVAArticulo));
    	imprimirPie.put("cuotaIVALibro", String.valueOf(cuotaIVALibro));
        
        
    	try {
            // Creamos el escritor del archivo pdf
            PdfWriter pdfWriter = new PdfWriter("./PDF/Presupuestos/Presupuesto_" + presupuesto.getNumero() + ".pdf");
            // Creamos el documento PDF que se almacenara via el escritor
            PdfDocument pdfDoc = new PdfDocument(pdfWriter);
            // Creamos el documento pdf en si, con pagina tamaño letra
            com.itextpdf.layout.Document doc = new com.itextpdf.layout.Document(pdfDoc, PageSize.LETTER);
            // Creamos el manejador de evento de pagina, el cual agregara
            // el encabezado y pie de pagina
            EventoPaginaPresupuesto evento = new EventoPaginaPresupuesto(doc, presupuesto, imprimirPie);
 
            // Indicamos que el manejador se encargara del evento END_PAGE
            pdfDoc.addEventHandler(PdfDocumentEvent.END_PAGE, evento);
             
            // Establecemos los margenes
            doc.setMargins(250, 36, 200, 36);
            
            float[] anchos = {75F, 215F, 40F, 50F, 50F, 50F};
            Table tablaProductos = new Table(anchos);
            tablaProductos.setWidth(527F);
            
            // Creamos el contenido
            for (int i = 0; i < productosImprimir.size(); i++) {
            	tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("codigo").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("nombre").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("IVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("cantidad").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("precioSinIVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaProductos.addCell(new Cell().add(productosImprimir.get(i).get("importeSinIVA").toString()).setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
            }
            
            doc.add(tablaProductos);
 
            // Cerramos el documento
            doc.close();
 
        } catch (FileNotFoundException ex) {
        	System.out.println("Error: " + ex.getMessage());
        }
    }
    
    //Se utiliza para tiques, facturas, albaranes y presupuestos.
    public JSONObject getBusquedaClienteTiqueAsJSON(String dniClienteTique) throws Exception {
    	Cliente clienteTique = DAOCliente.selectClienteTique(dniClienteTique);
        JSONObject result = clienteTique.toJSONObject();
        return result;
    }
    
    //Se utiliza para tiques, facturas, albaranes y presupuestos.
    public JSONArray ordenarArrayTiques(JSONArray tiques) {
    	JSONObject tiqueMaximo = new JSONObject();
    	for (int i=0; i < tiques.length(); i++) {
    		for (int j=0; j < tiques.length()-i-1; j++) {
    			if(Integer.parseInt(tiques.getJSONObject(j).get("numero").toString()) < Integer.parseInt(tiques.getJSONObject(j+1).get("numero").toString())){
    				tiqueMaximo = tiques.getJSONObject(j+1);
    				tiques.put(j+1,tiques.getJSONObject(j));
    				tiques.put(j,tiqueMaximo);
    			}
    		}
        }
    	return tiques;
    }
    
    //Se utiliza para tiques, facturas, albaranes y presupuestos
    public JSONObject getBusquedaProductoTiqueAsJSON(String codigoProductoTique) throws Exception {
    	Producto productoTique = DAOProducto.selectProductoTique(codigoProductoTique);
        JSONObject result = productoTique.toJSONObject();
        return result;
    }
    
    //Se utiliza para tiques, facturas, albaranes y presupuestos
    public void comprobarProductosTique(JSONArray productosTique) throws Exception {
    	Document producto = new Document();
    	int cantidad = 0;
    	
    	if(productosTique.length() == 0) {
    		throw new Exception("NO");
    	} else {
    		for(int i=0; i<productosTique.length(); i++){
        		producto=Document.parse(productosTique.getJSONObject((i)).toString());
        		cantidad = DAOProducto.comprobarProductoTique(producto.get("codigo").toString());
        		if (cantidad < Integer.parseInt(producto.get("cantidad").toString())){
        			throw new Exception("No hay existencias");
        		}
        	}
    	}
    }
}
