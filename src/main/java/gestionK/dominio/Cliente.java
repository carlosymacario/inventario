package gestionK.dominio;

import org.bson.Document;
import org.json.JSONObject;

public class Cliente {
	private String id;
	private String nombre;
	private String apellido;
	private String nif;
	private String direccion;
	private String localidad;
	private String cp;
	private String provincia;
	private String pais;
	private String telefono;
	private String correo;
	
	public Cliente() {
		
	}
	
	public Cliente(JSONObject clienteJSON) {
		this.id = clienteJSON.get("id").toString();
		this.nombre = clienteJSON.get("nombre").toString();
		this.apellido = clienteJSON.get("apellido").toString();
		this.nif = clienteJSON.get("nif").toString();
		this.direccion = clienteJSON.get("direccion").toString();
		this.localidad = clienteJSON.get("localidad").toString();
		this.cp = clienteJSON.get("cp").toString();
		this.provincia = clienteJSON.get("provincia").toString();
		this.pais = clienteJSON.get("pais").toString();
		this.telefono = clienteJSON.get("telefono").toString();
		this.correo = clienteJSON.get("correo").toString();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	 public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNIF() {
		return nif;
	}
	public void setNIF(String nif) {
		this.nif = nif;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getCP() {
		return cp;
	}
	public void setCP(String cp) {
		this.cp = cp;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public JSONObject toJSONObject() {
		 JSONObject jso = new JSONObject();
		 jso.put("id", (Object)this.id);
		 jso.put("nombre", (Object)this.nombre);
		 jso.put("apellido", (Object)this.apellido);
		 jso.put("nif", (Object)this.nif);
		 jso.put("direccion", (Object)this.direccion);
		 jso.put("localidad", (Object)this.localidad);
		 jso.put("cp", (Object)this.cp);
		 jso.put("provincia", (Object)this.provincia);
		 jso.put("pais", (Object)this.pais);
		 jso.put("telefono", (Object)this.telefono);
		 jso.put("correo", (Object)this.correo);
		 return jso;
	}
	public Document toDocument() {
		Document document = new Document();
		document.append("id", this.id);
		document.append("nombre", this.nombre);
		document.append("apellido", this.apellido);
		document.append("nif", this.nif);
		document.append("direccion", this.direccion);
		document.append("localidad", this.localidad);
		document.append("cp", this.cp);
		document.append("provincia", this.provincia);
		document.append("pais", this.pais);
		document.append("telefono", this.telefono);
		document.append("correo", this.correo);
		return document;
	}
}
