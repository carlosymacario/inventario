package gestionK.dominio;

import org.json.JSONObject;

public class Articulo {
	private String id;
	private String codigo;
	private String nombre;
	private String fechaAlta;
	private String fechaModificacion;
	private int cantidad;
	private double precio;
	private double precioIVA;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public double getPrecioIVA() {
		return precioIVA;
	}

	public void setPrecioIVA(double precioIVA) {
		this.precioIVA = precioIVA;
	}

	public JSONObject toJSONObject() {
		 JSONObject jso = new JSONObject();
		 jso.put("id", (Object)this.id);
		 jso.put("codigo", (Object)this.codigo);
		 jso.put("nombre", (Object)this.nombre);
		 jso.put("fechaAlta", (Object)this.fechaAlta);
		 jso.put("fechaModificacion", (Object)this.fechaModificacion);
		 jso.put("cantidad", (Object)this.cantidad);
		 jso.put("precio", (Object)this.precio);
		 jso.put("precioIVA", (Object)this.precioIVA);
		 return jso;
	 }
	
}
