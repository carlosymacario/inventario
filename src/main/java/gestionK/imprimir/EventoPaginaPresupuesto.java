package gestionK.imprimir;
 
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import gestionK.dominio.Cliente;
import gestionK.dominio.Presupuesto;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
 
public class EventoPaginaPresupuesto implements IEventHandler {
 
    private final Document documento;
    private final Presupuesto presupuesto;
    private final JSONObject productosImprimir;
 
    public EventoPaginaPresupuesto(Document doc, Presupuesto presupuesto, JSONObject productosImprimir) {
        documento = doc;
        this.presupuesto = presupuesto;
        this.productosImprimir = productosImprimir;
    }
     
    /**
     * Crea el rectangulo donde pondremos el encabezado
     * @param docEvent Evento de documento
     * @return Area donde colocaremos el encabezado
     */
    private Rectangle crearRectanguloEncabezado(PdfDocumentEvent docEvent) {
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();        
         
        float xEncabezado = pdfDoc.getDefaultPageSize().getX() + documento.getLeftMargin();
        float yEncabezado = pdfDoc.getDefaultPageSize().getTop() - documento.getTopMargin() + 15;
        float anchoEncabezado = page.getPageSize().getWidth() - 72;
        float altoEncabezado = 200F;
 
        Rectangle rectanguloEncabezado = new Rectangle(xEncabezado, yEncabezado, anchoEncabezado, altoEncabezado);
         
        return rectanguloEncabezado;        
    }
    
    private Rectangle crearRectanguloCliente(PdfDocumentEvent docEvent) {
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();        
         
        float xEncabezado = pdfDoc.getDefaultPageSize().getX() + documento.getLeftMargin();
        float yEncabezado = pdfDoc.getDefaultPageSize().getTop() - documento.getTopMargin() - 60;
        float anchoEncabezado = page.getPageSize().getWidth() - 72;
        float altoEncabezado = 200F;
 
        Rectangle rectanguloEncabezado = new Rectangle(xEncabezado, yEncabezado, anchoEncabezado, altoEncabezado);
         
        return rectanguloEncabezado;        
    }
    
    private Rectangle crearRectanguloFactura(PdfDocumentEvent docEvent) {
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();        
         
        float xEncabezado = pdfDoc.getDefaultPageSize().getX() + documento.getLeftMargin();
        float yEncabezado = pdfDoc.getDefaultPageSize().getTop() - documento.getTopMargin() - 5;
        float anchoEncabezado = page.getPageSize().getWidth() - 72;
        float altoEncabezado = 50F;
 
        Rectangle rectanguloEncabezado = new Rectangle(xEncabezado, yEncabezado, anchoEncabezado, altoEncabezado);
         
        return rectanguloEncabezado;        
    }
    
    private Rectangle crearRectanguloProductos(PdfDocumentEvent docEvent) {
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();        
         
        float xProductos = pdfDoc.getDefaultPageSize().getX() + documento.getLeftMargin();
        float yProductos = pdfDoc.getDefaultPageSize().getTop() - documento.getTopMargin() - 30;
        float anchoProductos = page.getPageSize().getWidth() - 72;
        float altoProductos = 50F;
 
        Rectangle rectanguloProductos = new Rectangle(xProductos, yProductos, anchoProductos, altoProductos);
         
        return rectanguloProductos;        
    }
     
    /**
     * Crea el rectangulo donde pondremos el pie de pagina
     * @param docEvent Evento del documento
     * @return Area donde colocaremos el pie de pagina
     */
    private Rectangle crearRectanguloPie(PdfDocumentEvent docEvent) {
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();
         
        float xPie = pdfDoc.getDefaultPageSize().getX() + documento.getLeftMargin();
        float yPie = pdfDoc.getDefaultPageSize().getBottom() - 70F;
        float anchoPie = page.getPageSize().getWidth() - 72;
        float altoPie = 200F;
 
        Rectangle rectanguloPie = new Rectangle(xPie, yPie, anchoPie, altoPie);
         
        return rectanguloPie;
    }
     
    /**
     * Crea la tabla que contendra el mensaje del encabezado
     * @param mensaje Mensaje que desplegaremos
     * @return Tabla con el mensaje de encabezado
     * @throws MalformedURLException 
     */
    private Table crearTablaEncabezado() throws MalformedURLException {
        float[] anchos = {100F, 210F, 200F};
        Table tablaEncabezado = new Table(anchos);
        tablaEncabezado.setWidth(527F);
        
        String IMG = "src/main/webapp/css/imagenes/Logo.jpg";
        String ruta = System.getProperty("user.dir");
        String [] split = ruta.split("\\\\");
        ruta = "";
        for(int i=0; i<split.length;i++) {
        	ruta = ruta + split[i] + "/";
        }
        IMG = ruta + IMG;
        Image img = new Image(ImageDataFactory.create(IMG));
        
        Cell cell = new Cell(4, 1)
        .add(img.scaleToFit(90F, 40F))
        .setVerticalAlignment(VerticalAlignment.MIDDLE)
        .setHorizontalAlignment(HorizontalAlignment.CENTER)
        .setFontSize(9)
        .setBorderRight(Border.NO_BORDER);
 
        tablaEncabezado.addCell(cell);
        tablaEncabezado.addCell(new Cell().add("Mª Dolores Gallego de la Sacristana Fdez. Baillo").setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("C.I.F.: 06221945P").setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("Cristo de Urda, 23").setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("Teléfono: 926 57 32 73\t Fax: 926 57 32 73").setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("13640 Herencia").setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("E-mail: papeleriakalkos@gmail.com").setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("Ciudad Real").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER));
         
        return tablaEncabezado;
    }
    
    private Table crearTablaCliente() {
    	Cliente cliente = new Cliente();
    	cliente = presupuesto.getCliente();
    	
        float[] anchos = {250F};
        Table tablaEncabezado = new Table(anchos);
        tablaEncabezado.setWidth(527F);
 
        tablaEncabezado.addCell(new Cell().add("Identificación fiscal: " + cliente.getNIF()).setFontSize(9).setBorderBottom(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add(cliente.getNombre()).setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add(cliente.getDireccion()).setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add(cliente.getCP() + " " + cliente.getLocalidad()).setFontSize(9).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add(cliente.getProvincia()).setFontSize(9).setBorderTop(Border.NO_BORDER));
         
        return tablaEncabezado;
    }
    
    private Table crearTablaFactura(PdfDocumentEvent docEvent) {
    	SimpleDateFormat formato = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
    	SimpleDateFormat soloFecha = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaDate = null;
        try{
        	fechaDate = formato.parse(presupuesto.getFecha());
        } catch (Exception e) {
        	System.out.println("Error: " + e.getMessage());
        }
        
    	
    	PdfPage page = docEvent.getPage();
        float[] anchos = {180F, 180F, 180F};
        Table tablaEncabezado = new Table(anchos);
        tablaEncabezado.setWidth(527F);
 
        tablaEncabezado.addCell(new Cell().add("PRESUPUESTO: " + presupuesto.getNumero()).setFontSize(9).setBorderRight(Border.NO_BORDER));
        tablaEncabezado.addCell(new Cell().add("Fecha: " + soloFecha.format(fechaDate)).setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setHorizontalAlignment(HorizontalAlignment.CENTER));
        tablaEncabezado.addCell(new Cell().add("Nº hoja: " + docEvent.getDocument().getPageNumber(page)).setFontSize(9).setBorderLeft(Border.NO_BORDER).setHorizontalAlignment(HorizontalAlignment.RIGHT));
         
        return tablaEncabezado;
    }
    
    private Table crearTablaProductos() {
    	float[] anchos = {73F, 210F, 40F, 50F, 50F, 50F};
        Table tablaProductos = new Table(anchos);
        tablaProductos.setWidth(527F);
        
        tablaProductos.addCell(new Cell().add("Referencia").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaProductos.addCell(new Cell().add("Descripción").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaProductos.addCell(new Cell().add("% IVA").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaProductos.addCell(new Cell().add("Uds").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaProductos.addCell(new Cell().add("Precio").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaProductos.addCell(new Cell().add("Importe").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        
        return tablaProductos;
    }
     
    /**
     * Crea la tabla de pie de pagina, con el numero de pagina
     * @param docEvent Evento del documento
     * @return Pie de pagina con el numero de pagina
     */
    private Table crearTablaPie(PdfDocumentEvent docEvent) {
    	//iva puede valer 1 o 2. Para saber si hay solo articulos, solo libros o las dos cosas
    	int iva = Integer.parseInt(productosImprimir.get("IVAS").toString());
    	
    	DecimalFormat formato2Decimales = new DecimalFormat("#.00");
    	
    	double totalSinIVAArticulo = 0;
        double totalSinIVALibro = 0;
        
        double cuotaIVAArticulo = 0;
        double cuotaIVALibro = 0;
    	
        float[] anchos = {132F, 132F, 132F, 132F};
        Table tablaPie = new Table(anchos);
        tablaPie.setWidth(527F);
        
        tablaPie.addCell(new Cell().add("Base Imponible").setFontSize(9).setBorderRight(Border.NO_BORDER));
        tablaPie.addCell(new Cell().add("% IVA").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        tablaPie.addCell(new Cell().add("Cuota IVA").setFontSize(9).setBorderLeft(Border.NO_BORDER));
        tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        
        if (iva == 1) {
        	//Se comprueba si solo vienen articulos
        	if (productosImprimir.get("IVA").toString().equals("21")) {
        		totalSinIVAArticulo = Double.parseDouble(productosImprimir.get("totalSinIVAArticulo").toString());
        		cuotaIVAArticulo = Double.parseDouble(productosImprimir.get("cuotaIVAArticulo").toString());
        		
        		tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(totalSinIVAArticulo))).setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("21").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(cuotaIVAArticulo))).setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        	
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("TOTAL").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                
                tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(totalSinIVAArticulo))).setFontSize(9).setBorderRight(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(cuotaIVAArticulo))).setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add(String.valueOf(presupuesto.getTotal())).setFontSize(9).setBorderLeft(Border.NO_BORDER));
            //Se comprueba si solo vienen libros 
        	} else if (productosImprimir.get("IVA").toString().equals("4")) {
        		totalSinIVALibro = Double.parseDouble(productosImprimir.get("totalSinIVALibro").toString());
                cuotaIVALibro = Double.parseDouble(productosImprimir.get("cuotaIVALibro").toString());
        		
        		tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(totalSinIVALibro))).setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("4").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(cuotaIVALibro))).setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        	
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("TOTAL").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                
                tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(totalSinIVALibro))).setFontSize(9).setBorderRight(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(cuotaIVALibro))).setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
                tablaPie.addCell(new Cell().add(String.valueOf(presupuesto.getTotal())).setFontSize(9).setBorderLeft(Border.NO_BORDER));
        	}
        } else if (iva == 2) {
        	totalSinIVAArticulo = Double.parseDouble(productosImprimir.get("totalSinIVAArticulo").toString());
            totalSinIVALibro = Double.parseDouble(productosImprimir.get("totalSinIVALibro").toString());
            
            cuotaIVAArticulo = Double.parseDouble(productosImprimir.get("cuotaIVAArticulo").toString());
            cuotaIVALibro = Double.parseDouble(productosImprimir.get("cuotaIVALibro").toString());
        	
        	tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(totalSinIVAArticulo))).setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("21").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(cuotaIVAArticulo))).setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(totalSinIVALibro))).setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("4").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add(String.valueOf(formato2Decimales.format(cuotaIVALibro))).setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
        
            tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderRight(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderBottom(Border.NO_BORDER).setBorderTop(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("TOTAL").setFontSize(9).setBorderTop(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
            
            tablaPie.addCell(new Cell().add(String.valueOf(totalSinIVAArticulo + totalSinIVALibro)).setFontSize(9).setBorderRight(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add("").setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add(String.valueOf(cuotaIVAArticulo + cuotaIVALibro)).setFontSize(9).setBorderLeft(Border.NO_BORDER).setBorderRight(Border.NO_BORDER));
            tablaPie.addCell(new Cell().add(String.valueOf(presupuesto.getTotal())).setFontSize(9).setBorderLeft(Border.NO_BORDER));
        }
         
        return tablaPie;
    }
     
 
    /**
     * Manejador del evento de cambio de pagina, agrega el encabezado y pie de pagina
     * @param event Evento de pagina
     */
    @Override
    public void handleEvent(Event event) {
        PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
        PdfDocument pdfDoc = docEvent.getDocument();
        PdfPage page = docEvent.getPage();
        PdfCanvas canvas = new PdfCanvas(page.newContentStreamBefore(), page.getResources(), pdfDoc);
         
        Table tablaEncabezado = null;
		try {
			tablaEncabezado = this.crearTablaEncabezado();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Rectangle rectanguloEncabezado = this.crearRectanguloEncabezado(docEvent);        
        Canvas canvasEncabezado = new Canvas(canvas, pdfDoc, rectanguloEncabezado);        
        canvasEncabezado.add(tablaEncabezado);
        
        Table tablaCliente = this.crearTablaCliente();
        Rectangle rectanguloCliente = this.crearRectanguloCliente(docEvent);
        Canvas canvasCliente = new Canvas(canvas, pdfDoc, rectanguloCliente);
        canvasCliente.add(tablaCliente);
        
        Table tablaFactura = this.crearTablaFactura(docEvent);
        Rectangle rectanguloFactura = this.crearRectanguloFactura(docEvent);
        Canvas canvasFactura = new Canvas(canvas, pdfDoc, rectanguloFactura);
        canvasFactura.add(tablaFactura);
        
        Table tablaProductos = this.crearTablaProductos();
        Rectangle rectanguloProductos = this.crearRectanguloProductos(docEvent);
        Canvas canvasProductos = new Canvas(canvas, pdfDoc, rectanguloProductos);
        canvasProductos.add(tablaProductos);
 
        Table tablaNumeracion = this.crearTablaPie(docEvent);
        Rectangle rectanguloPie = this.crearRectanguloPie(docEvent);
        Canvas canvasPie = new Canvas(canvas, pdfDoc, rectanguloPie);        
        canvasPie.add(tablaNumeracion);
    }
}