package gestionK.dao;

import java.util.Iterator;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;

import gestionK.dominio.BaseDatos;
import gestionK.dominio.Cliente;

public class DAOCliente {
	
	public static Cliente select(String idCliente) throws Exception {
		BaseDatos bd = new BaseDatos();
		Cliente resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("clientes").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("_id").toString().equals(idCliente)) {
				resultado = new Cliente();
				resultado.setId(idCliente);
				resultado.setNombre(registro.get("nombre").toString());
				resultado.setApellido(registro.get("apellido").toString());
				resultado.setNIF(registro.get("nif").toString());
				resultado.setDireccion(registro.get("direccion").toString());
				resultado.setLocalidad(registro.get("localidad").toString());
				resultado.setCP(registro.get("cp").toString());
				resultado.setProvincia(registro.get("provincia").toString());
				resultado.setPais(registro.get("pais").toString());
				resultado.setTelefono(registro.get("telefono").toString());
				resultado.setCorreo(registro.get("correo").toString());
			}
		}
    	return resultado;
    }
	
	public static Cliente selectClienteTique(String dniClienteTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		Cliente resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("clientes").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("nif").toString().equalsIgnoreCase(dniClienteTique)) {
				resultado = new Cliente();
				resultado.setId(registro.get("_id").toString());
				resultado.setNombre(registro.get("nombre").toString());
				resultado.setApellido(registro.get("apellido").toString());
				resultado.setNIF(registro.get("nif").toString());
				resultado.setDireccion(registro.get("direccion").toString());
				resultado.setLocalidad(registro.get("localidad").toString());
				resultado.setCP(registro.get("cp").toString());
				resultado.setProvincia(registro.get("provincia").toString());
				resultado.setPais(registro.get("pais").toString());
				resultado.setTelefono(registro.get("telefono").toString());
				resultado.setCorreo(registro.get("correo").toString());
			}
		}
    	return resultado;
    }
	
	public static void insert(Cliente cliente) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		bd.getMongodb().getCollection("clientes").insertOne(
				new Document("nombre", cliente.getNombre())
					.append("apellido", cliente.getApellido())
					.append("nif", cliente.getNIF())
					.append("direccion", cliente.getDireccion())
					.append("localidad", cliente.getLocalidad())
					.append("cp", cliente.getCP())
					.append("provincia", cliente.getProvincia())
					.append("pais", cliente.getPais())
					.append("telefono", cliente.getTelefono())
					.append("correo", cliente.getCorreo()));
		
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("clientes").find(
				new Document("nombre", cliente.getNombre())
					.append("apellido", cliente.getApellido())
					.append("nif", cliente.getNIF())
					.append("direccion", cliente.getDireccion())
					.append("localidad", cliente.getLocalidad())
					.append("cp", cliente.getCP())
					.append("provincia", cliente.getProvincia())
					.append("pais", cliente.getPais())
					.append("telefono", cliente.getTelefono())
					.append("correo", cliente.getCorreo()));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			cliente.setId(String.valueOf(registro.get("_id")));
		}
	 }
	 
	public static void update(Cliente cliente) throws Exception {
		BaseDatos bd = new BaseDatos();
			
		ObjectId id = new ObjectId(cliente.getId());
		Document filtro = new Document("_id", id);
			
		bd.getMongodb().getCollection("clientes").updateOne(filtro, new Document("$set",
				new Document("nombre", cliente.getNombre())
					.append("apellido", cliente.getApellido())
					.append("nif", cliente.getNIF())
					.append("direccion", cliente.getDireccion())
					.append("localidad", cliente.getLocalidad())
					.append("cp", cliente.getCP())
					.append("provincia", cliente.getProvincia())
					.append("pais", cliente.getPais())
					.append("telefono", cliente.getTelefono())
					.append("correo", cliente.getCorreo())));
	}
	 
	public static void delete(String idCliente) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		ObjectId id = new ObjectId(idCliente);
		Document filtro = new Document("_id", id);
				
		bd.getMongodb().getCollection("clientes").deleteOne(filtro);
	} 
}
