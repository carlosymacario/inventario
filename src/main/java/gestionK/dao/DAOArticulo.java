package gestionK.dao;

import java.util.Iterator;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;

import gestionK.dominio.Articulo;
import gestionK.dominio.BaseDatos;

public class DAOArticulo {
	
	public static Articulo select(String idArticulo) throws Exception {
		BaseDatos bd = new BaseDatos();
		Articulo resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("articulo", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("_id").toString().equals(idArticulo)) {
				resultado = new Articulo();
				resultado.setId(idArticulo);
				resultado.setCodigo(registro.get("codigo").toString());
				resultado.setNombre(registro.get("nombre").toString());
				resultado.setFechaAlta(registro.get("fechaAlta").toString());
				resultado.setFechaModificacion(registro.get("fechaModificacion").toString());
				resultado.setCantidad(Integer.parseInt(registro.get("cantidad").toString()));
				resultado.setPrecio(Double.parseDouble(registro.get("precio").toString()));
				resultado.setPrecioIVA(Double.parseDouble(registro.get("precioIVA").toString()));
			}
		}
    	return resultado;
    }
	
	public static Boolean comprobarCodigo(String codigoArticulo) throws Exception {
		BaseDatos bd = new BaseDatos();
		Boolean resultado = false;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("articulo", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("codigo").toString().equals(codigoArticulo)) {
				resultado = true;
			}
		}
    	return resultado;
	}
	
	public static Articulo selectArticuloTique(String codigoArticuloTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		Articulo resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("articulo", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("codigo").toString().equals(codigoArticuloTique)) {
				resultado = new Articulo();
				resultado.setId(registro.get("_id").toString());
				resultado.setCodigo(registro.get("codigo").toString());
				resultado.setNombre(registro.get("nombre").toString());
				resultado.setFechaAlta(registro.get("fechaAlta").toString());
				resultado.setFechaModificacion(registro.get("fechaModificacion").toString());
				resultado.setCantidad(Integer.parseInt(registro.get("cantidad").toString()));
				resultado.setPrecio(Double.parseDouble(registro.get("precio").toString()));
				resultado.setPrecioIVA(Double.parseDouble(registro.get("precioIVA").toString()));
			}
		}
    	return resultado;
    }
	
	public static void insert(Articulo articulo) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		bd.getMongodb().getCollection("productos").insertOne(
				new Document("articulo", "1")
					.append("codigo", articulo.getCodigo())
					.append("nombre", articulo.getNombre())
					.append("fechaAlta", articulo.getFechaAlta())
					.append("fechaModificacion", articulo.getFechaModificacion())
					.append("cantidad", articulo.getCantidad())
					.append("precio", articulo.getPrecio())
					.append("precioIVA", articulo.getPrecioIVA()));
		
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(
				new Document("articulo", "1")
				.append("codigo", articulo.getCodigo())
				.append("nombre", articulo.getNombre())
				.append("fechaAlta", articulo.getFechaAlta())
				.append("fechaModificacion", articulo.getFechaModificacion())
				.append("cantidad", articulo.getCantidad())
				.append("precio", articulo.getPrecio())
				.append("precioIVA", articulo.getPrecioIVA()));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			articulo.setId(String.valueOf(registro.get("_id")));
		}
	 }
	 
	public static void update(Articulo articulo) throws Exception {
		BaseDatos bd = new BaseDatos();
			
		ObjectId id = new ObjectId(articulo.getId());
		Document filtro = new Document("articulo", "1").append("_id", id);

		bd.getMongodb().getCollection("productos").updateOne(filtro, new Document("$set",
				new Document("codigo", articulo.getCodigo())
				.append("nombre", articulo.getNombre())
				.append("fechaModificacion", articulo.getFechaModificacion())
				.append("cantidad", articulo.getCantidad())
				.append("precio", articulo.getPrecio())
				.append("precioIVA", articulo.getPrecioIVA())));
	}
	 
	public static void delete(String idArticulo) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		ObjectId id = new ObjectId(idArticulo);
		Document filtro = new Document("articulo", "1").append("_id", id);
				
		bd.getMongodb().getCollection("productos").deleteOne(filtro);
	}
}
