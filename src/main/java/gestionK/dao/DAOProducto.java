package gestionK.dao;

import java.util.Iterator;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;

import gestionK.dominio.BaseDatos;
import gestionK.dominio.Producto;

public class DAOProducto {
	
	public static Producto select(String idProducto) throws Exception {
		BaseDatos bd = new BaseDatos();
		Producto resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("_id").toString().equals(idProducto)) {
				resultado = new Producto();
				resultado.setId(idProducto);
				resultado.setCodigo(registro.get("codigo").toString());
				resultado.setNombre(registro.get("nombre").toString());
				resultado.setPrecioIVA(registro.get("precioIVA").toString());
			}
		}
    	return resultado;
    }
	
	public static String selectArticuloLibro(String codigoProducto) throws Exception {
		BaseDatos bd = new BaseDatos();
		String resultado = "";
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("codigo").toString().equals(codigoProducto)) {
				if (registro.get("articulo") != null) {
					resultado = "articulo";
				} else if (registro.get("libro") != null) {
					resultado = "libro";
				}
			}
		}
    	return resultado;
    }
	
	public static Producto selectProductoTique(String codigoProductoTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		Producto resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("codigo").toString().equals(codigoProductoTique)) {
				resultado = new Producto();
				resultado.setId(registro.get("_id").toString());
				resultado.setCodigo(registro.get("codigo").toString());
				if(registro.get("nombre") != null){
					resultado.setNombre(registro.get("nombre").toString());
				} else {
					resultado.setNombre(registro.get("titulo").toString());
				}
				resultado.setPrecioIVA(registro.get("precioIVA").toString());
			}
		}
    	return resultado;
    }
	
	public static int comprobarProductoTique(String codigoProductoTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		int cantidad = 0;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("codigo", codigoProductoTique));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			cantidad = Integer.parseInt(registro.get("cantidad").toString());
		}
		return cantidad;
	}
	
	public static void insert(Producto producto) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		bd.getMongodb().getCollection("productos").insertOne(
				new Document("codigo", producto.getCodigo())
					.append("nombre", producto.getNombre())
					.append("precioIVA", producto.getPrecioIVA()));
		
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(
				new Document("codigo", producto.getCodigo())
				.append("nombre", producto.getNombre())
				.append("precioIVA", producto.getPrecioIVA()));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			producto.setId(String.valueOf(registro.get("_id")));
		}
	 }
	
	public static void update(String codigoProductoTique, String cantidadProductoTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		int cantidadProductoBD = DAOProducto.comprobarProductoTique(codigoProductoTique);
		int cantidadNueva = cantidadProductoBD - Integer.parseInt(cantidadProductoTique);
		
		Document filtro = new Document("codigo", codigoProductoTique);
			
		bd.getMongodb().getCollection("productos").updateOne(filtro, new Document("$set",
				new Document("cantidad", cantidadNueva)));
	}
	
	//Cuando se modifica un tique y se cambia la cantidad o los productos que contiene y también cuando se elimina un tique
	public static void modificado(String codigoProductoTique, int diferenciaCantidad) throws Exception {
		BaseDatos bd = new BaseDatos();
		int cantidadProductoBD = DAOProducto.comprobarProductoTique(codigoProductoTique);
		int cantidadNueva = cantidadProductoBD + diferenciaCantidad;
		
		Document filtro = new Document("codigo", codigoProductoTique);
			
		bd.getMongodb().getCollection("productos").updateOne(filtro, new Document("$set",
				new Document("cantidad", cantidadNueva)));
	}
	 
	public static void delete(String idProducto) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		ObjectId id = new ObjectId(idProducto);
		Document filtro = new Document("_id", id);
				
		bd.getMongodb().getCollection("productos").deleteOne(filtro);
	}
}
