package gestionK.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;

import gestionK.dominio.BaseDatos;
import gestionK.dominio.Cliente;
import gestionK.dominio.Presupuesto;

public class DAOPresupuesto {
	
	public static Presupuesto select(String idPresupuesto) throws Exception {
		BaseDatos bd = new BaseDatos();
		Presupuesto resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("presupuestos").find();
		it = iterable.iterator();
		Cliente cliente = new Cliente();
		while (it.hasNext()) {
			Document registro = it.next();
			if (registro.get("_id").toString().equals(idPresupuesto)) {
				resultado = new Presupuesto();
				resultado.setNumero(Integer.parseInt(registro.get("numero").toString()));
				resultado.setId(registro.get("_id").toString());
				resultado.setTotal(Double.parseDouble(registro.get("total").toString()));;
				resultado.setFecha(registro.get("fecha").toString());
				resultado.setAnyo(registro.get("anyo").toString());
				List<Document> productos = new ArrayList<Document>();
				productos = (List<Document>)registro.get("productos");
				resultado.setProductos(productos);
				Document clienteJSON = new Document();
				clienteJSON = (Document)registro.get("cliente");
				cliente.setId(clienteJSON.get("id").toString());
				cliente.setNombre(clienteJSON.get("nombre").toString());
				cliente.setApellido(clienteJSON.get("apellido").toString());
				cliente.setNIF(clienteJSON.get("nif").toString());
				cliente.setDireccion(clienteJSON.get("direccion").toString());
				cliente.setLocalidad(clienteJSON.get("localidad").toString());
				cliente.setCP(clienteJSON.get("cp").toString());
				cliente.setProvincia(clienteJSON.get("provincia").toString());
				cliente.setPais(clienteJSON.get("pais").toString());
				cliente.setTelefono(clienteJSON.get("telefono").toString());
				cliente.setCorreo(clienteJSON.get("correo").toString());
				resultado.setCliente(cliente);
			}
		}
    	return resultado;
    }
	
	public static void insert(Presupuesto presupuesto) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		bd.getMongodb().getCollection("presupuestos").insertOne(
				new Document("numero", presupuesto.getNumero())
					.append("total", presupuesto.getTotal())
					.append("fecha", presupuesto.getFecha())
					.append("anyo", presupuesto.getAnyo())
					.append("productos", presupuesto.getProductos())
					.append("cliente", presupuesto.getClienteAsDocument()));
		
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("presupuestos").find(
				new Document("numero", presupuesto.getNumero())
					.append("total", presupuesto.getTotal())
					.append("fecha", presupuesto.getFecha())
					.append("anyo", presupuesto.getAnyo())
					.append("productos", presupuesto.getProductos())
					.append("cliente", presupuesto.getClienteAsDocument()));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			presupuesto.setId(String.valueOf(registro.get("_id")));
		}
	 }
	
	public static void update(Presupuesto presupuesto) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		Document filtro = new Document("_id", new ObjectId(presupuesto.getId()));
		
		bd.getMongodb().getCollection("presupuestos").updateOne(filtro, new Document("$set",
				new Document("total", presupuesto.getTotal())
					.append("fecha", presupuesto.getFecha())
					.append("productos", presupuesto.getProductos())
					.append("cliente", presupuesto.getClienteAsDocument())));
	}
	
	public static void delete(String idPresupuesto) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		ObjectId id = new ObjectId(idPresupuesto);
		Document filtro = new Document("_id", id);
				
		bd.getMongodb().getCollection("presupuestos").deleteOne(filtro);
	}
	
	public static int numeroPresupuestoNuevo(String anyoAbierto) throws Exception {
		BaseDatos bd = new BaseDatos();
		int numeroNuevo = 0;
		int numeroMayor = 0;
	    
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("presupuestos").find(
				new Document("anyo", anyoAbierto));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			numeroMayor = Integer.parseInt(registro.get("numero").toString());
			if (numeroMayor > numeroNuevo) {
				numeroNuevo = numeroMayor;
			}
		}

    	return numeroNuevo+1;
    }
}
