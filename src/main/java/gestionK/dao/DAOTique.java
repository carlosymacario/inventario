package gestionK.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;

import gestionK.dominio.BaseDatos;
import gestionK.dominio.Tique;

public class DAOTique {
	
	public static Tique select(String idTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		Tique resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("tiques").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if (registro.get("_id").toString().equals(idTique)) {
				resultado = new Tique();
				resultado.setNumero(Integer.parseInt(registro.get("numero").toString()));
				resultado.setId(registro.get("_id").toString());
				resultado.setTotal(Double.parseDouble(registro.get("total").toString()));;
				resultado.setFecha(registro.get("fecha").toString());
				resultado.setAnyo(registro.get("anyo").toString());
				List<Document> productos = new ArrayList<Document>();
				productos = (List<Document>)registro.get("productos");
				resultado.setProductos(productos);
			}
		}
    	return resultado;
    }
	
	public static void insert(Tique tique) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		bd.getMongodb().getCollection("tiques").insertOne(
				new Document("numero", tique.getNumero())
					.append("total", tique.getTotal())
					.append("fecha", tique.getFecha())
					.append("anyo", tique.getAnyo())
					.append("productos", tique.getProductos()));
		
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("tiques").find(
				new Document("numero", tique.getNumero())
					.append("total", tique.getTotal())
					.append("fecha", tique.getFecha())
					.append("anyo", tique.getAnyo())
					.append("productos", tique.getProductos()));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			tique.setId(String.valueOf(registro.get("_id")));
		}
	 }
	
	public static void update(Tique tique) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		Document filtro = new Document("_id", new ObjectId(tique.getId()));
		
		bd.getMongodb().getCollection("tiques").updateOne(filtro, new Document("$set",
				new Document("total", tique.getTotal())
					.append("fecha", tique.getFecha())
					.append("productos", tique.getProductos())));
	}
	
	public static void delete(String idTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		ObjectId id = new ObjectId(idTique);
		Document filtro = new Document("_id", id);
				
		bd.getMongodb().getCollection("tiques").deleteOne(filtro);
	}
	
	public static int numeroTiqueNuevo(String anyoAbierto) throws Exception {
		BaseDatos bd = new BaseDatos();
		int numeroNuevo = 0;
		int numeroMayor = 0;
	    
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("tiques").find(
				new Document("anyo", anyoAbierto));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			numeroMayor = Integer.parseInt(registro.get("numero").toString());
			if (numeroMayor > numeroNuevo) {
				numeroNuevo = numeroMayor;
			}
		}

    	return numeroNuevo+1;
    }
}
