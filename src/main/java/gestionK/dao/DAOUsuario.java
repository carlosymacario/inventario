package gestionK.dao;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.regex.Pattern;

import org.bson.Document;

import com.mongodb.BasicDBObject;

import com.mongodb.client.*;

import gestionK.dominio.Albaran;
import gestionK.dominio.Anyo;
import gestionK.dominio.Articulo;
import gestionK.dominio.BaseDatos;
import gestionK.dominio.Cliente;
import gestionK.dominio.Factura;
import gestionK.dominio.Libro;
import gestionK.dominio.Presupuesto;
import gestionK.dominio.Tique;
import gestionK.dominio.Usuario;

public class DAOUsuario {
	
	public static Usuario select(String login, String pwd) throws Exception {
		BaseDatos bd = new BaseDatos();
        Usuario result = null;
        
        Iterator<Document> it;
        FindIterable<Document> iterable = bd.getMongodb().getCollection("usuarios").find();
        it = iterable.iterator();
        while (it.hasNext()) {
        	Document registro = it.next();
            if(registro.get("usuario").equals(login)) {
            	if(registro.get("contrasena").equals(getMD5(pwd))) {
            		result = new Usuario(login);
            		result.setId((registro.get("_id").toString()));
            		Usuario usuario = result;
            		return usuario;
            	}
            }
        }
        return result;
    }
	
	public static Usuario select(String id) throws Exception {
		BaseDatos bd = new BaseDatos();
		Usuario result = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("usuarios").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("_id").toString().equals(id)) {
				result = new Usuario(registro.get("usuario").toString());
				result.setId(registro.get("_id").toString());
				Usuario usuario = result;
				return usuario;
			}
		}
		return result;
	}
	
	public static Boolean comprobarUsuario(String login) throws Exception {
		BaseDatos bd = new BaseDatos();
		Boolean resultado = false;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("usuarios").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("usuario").toString().equals(login)) {
				resultado = true;
			}
		}
    	return resultado;
	}
	
	public static Usuario insert(String login, String password) throws Exception {
		BaseDatos bd = new BaseDatos();
		Usuario usuario = new Usuario(login);
		
		bd.getMongodb().getCollection("usuarios").insertOne(
				new Document("usuario", login)
					.append("contrasena", getMD5(password)));
		
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("usuarios").find(
				new Document("usuario", login)
					.append("contrasena", getMD5(password)));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			usuario.setId(String.valueOf(registro.get("_id")));
		}
		return usuario;
	 }
	 
	public static Hashtable<String,Cliente> getClientes()throws Exception{
    	BaseDatos bd = new BaseDatos();
		Hashtable<String,Cliente> resultado = new Hashtable<String, Cliente>();
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("clientes").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Cliente cliente = DAOCliente.select(id);
			resultado.put(id, cliente);
		}
    	return resultado;
    }
	
	public static Hashtable<String,Articulo> getArticulos()throws Exception{
		BaseDatos bd = new BaseDatos();
    	Hashtable<String,Articulo> resultado = new Hashtable<String, Articulo>();
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("articulo", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Articulo articulo = DAOArticulo.select(id);
			resultado.put(id, articulo);
		}
    	return resultado;
    }
	
	public static Hashtable<String,Libro> getLibros()throws Exception{
		BaseDatos bd = new BaseDatos();
    	Hashtable<String,Libro> resultado = new Hashtable<String, Libro>();
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("libro", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Libro libro = DAOLibro.select(id);
			resultado.put(id, libro);
		}
    	return resultado;
    }
	
	public static Hashtable<String, Tique> getTiques()throws Exception{
		BaseDatos bd = new BaseDatos();
    	Hashtable<String,Tique> resultado = new Hashtable<String, Tique>();
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("tiques").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Tique tique = DAOTique.select(id);
			resultado.put(id, tique);
		}
    	return resultado;
    }
	
	public static Hashtable<String, Factura> getFacturas()throws Exception{
		BaseDatos bd = new BaseDatos();
    	Hashtable<String, Factura> resultado = new Hashtable<String, Factura>();
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("facturas").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Factura factura = DAOFactura.select(id);
			resultado.put(id, factura);
		}
    	return resultado;
    }
	
	public static Hashtable<String, Albaran> getAlbaranes()throws Exception{
		BaseDatos bd = new BaseDatos();
    	Hashtable<String, Albaran> resultado = new Hashtable<String, Albaran>();
    	
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("albaranes").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Albaran albaran = DAOAlbaran.select(id);
			resultado.put(id, albaran);
		}
    	return resultado;
    }
	
	public static Hashtable<String, Presupuesto> getPresupuestos()throws Exception{
		BaseDatos bd = new BaseDatos();
    	Hashtable<String, Presupuesto> resultado = new Hashtable<String, Presupuesto>();
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("presupuestos").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Presupuesto presupuesto = DAOPresupuesto.select(id);
			resultado.put(id, presupuesto);
		}
    	return resultado;
    }
	
	public static Hashtable<String, Anyo> getAnyos()throws Exception{
		BaseDatos bd = new BaseDatos();
    	Hashtable<String,Anyo> resultado = new Hashtable<String, Anyo>();
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("anyos").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Anyo anyo = DAOAnyo.select(id);
			resultado.put(id, anyo);
		}
    	return resultado;
    }
	
	public static Hashtable<String,Cliente> getBusquedaClientes(String nombreCliente, String apellidoCliente, String NIFCliente, String direccionCliente, String localidadCliente, String CPCliente, String provinciaCliente, String paisCliente, String telefonoCliente, String correoCliente)throws Exception{
    	BaseDatos bd = new BaseDatos();
		Hashtable<String,Cliente> resultado = new Hashtable<String, Cliente>();
		
		Document regQuery = new Document();
		//Para buscar una subcadena se utiliza la opción m.
		BasicDBObject filtros = new BasicDBObject();
		if (!nombreCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(nombreCliente));
			regQuery.append("$options", "i");
			
			filtros.append("nombre", regQuery);
		}
		if (!apellidoCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(apellidoCliente));
			regQuery.append("$options", "i");
			
			filtros.append("apellido", regQuery);
		}
		if (!NIFCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(NIFCliente));
			regQuery.append("$options", "i");
			
			filtros.append("nif", regQuery);
		}
		if (!direccionCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(direccionCliente));
			regQuery.append("$options", "i");
			
			filtros.append("direccion", regQuery);
		}
		if (!localidadCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(localidadCliente));
			regQuery.append("$options", "i");
			
			filtros.append("localidad", regQuery);
		}
		if (!CPCliente.equals("")) {			
			filtros.append("cp", CPCliente);
		}
		if (!provinciaCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(provinciaCliente));
			regQuery.append("$options", "i");
			
			filtros.append("provincia", regQuery);
		}
		if (!paisCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(paisCliente));
			regQuery.append("$options", "i");
			
			filtros.append("pais", regQuery);
		}
		if (!telefonoCliente.equals("")) {
			filtros.append("telefono", telefonoCliente);
		}
		if (!correoCliente.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(correoCliente));
			regQuery.append("$options", "i");
			
			filtros.append("correo", regQuery);
		}
		
		Iterator<Document> it;
		FindIterable<Document> iterable;
		if (filtros.isEmpty()) {
			iterable = bd.getMongodb().getCollection("clientes").find();
		} else {
			iterable = bd.getMongodb().getCollection("clientes").find(filtros);
		}
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Cliente cliente = DAOCliente.select(id);
			resultado.put(id, cliente);
		}
    	return resultado;
    }
	
	public static Hashtable<String,Articulo> getBusquedaArticulos(String codigoArticulo, String nombreArticulo, int cantidadArticulo, double precioArticulo, double precioIVAArticulo)throws Exception{
    	BaseDatos bd = new BaseDatos();
		Hashtable<String,Articulo> resultado = new Hashtable<String, Articulo>();
		
		Document regQuery = new Document();
		
		BasicDBObject filtros = new BasicDBObject();
		filtros.append("articulo", "1");
		if (!codigoArticulo.equals("")) {
			filtros.append("codigo", codigoArticulo);
		}
		if (!nombreArticulo.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(nombreArticulo));
			regQuery.append("$options", "i");
			
			filtros.append("nombre", regQuery);
		}
		if (cantidadArticulo != -1) {
			filtros.append("cantidad", cantidadArticulo);
		}
		if (precioArticulo != -1) {
			filtros.append("precio", precioArticulo);
		}
		if (precioIVAArticulo != -1) {
			filtros.append("precioIVA", precioIVAArticulo);
		}
		
		Iterator<Document> it;
		FindIterable<Document> iterable;
		if (filtros.isEmpty()) {
			iterable = bd.getMongodb().getCollection("productos").find(new Document("articulo", "1"));
		} else {
			iterable = bd.getMongodb().getCollection("productos").find(filtros);
		}
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Articulo articulo = DAOArticulo.select(id);
			resultado.put(id, articulo);
		}
    	return resultado;
    }
	
	public static Hashtable<String,Libro> getBusquedaLibros(String codigoLibro, String tituloLibro, String autorLibro, String editorialLibro, String distribuidorLibro, int cantidadLibro, double precioLibro, double precioIVALibro)throws Exception{
    	BaseDatos bd = new BaseDatos();
		Hashtable<String,Libro> resultado = new Hashtable<String, Libro>();
		
		Document regQuery = new Document();
		
		BasicDBObject filtros = new BasicDBObject();
		filtros.append("libro", "1");
		if (!codigoLibro.equals("")) {
			filtros.append("codigo", codigoLibro);
		}
		if (!tituloLibro.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(tituloLibro));
			regQuery.append("$options", "i");
			
			filtros.append("titulo", regQuery);
		}
		if (!autorLibro.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(autorLibro));
			regQuery.append("$options", "i");
			
			filtros.append("autor", regQuery);
		}
		if (!editorialLibro.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(editorialLibro));
			regQuery.append("$options", "i");
			
			filtros.append("editorial", regQuery);
		}
		if (!distribuidorLibro.equals("")) {
			regQuery = new Document();
			regQuery.append("$regex", "^(?)" + Pattern.quote(distribuidorLibro));
			regQuery.append("$options", "i");
			
			filtros.append("distribuidor", regQuery);
		}
		if (cantidadLibro != -1) {
			filtros.append("cantidad", cantidadLibro);
		}
		if (precioLibro != -1) {
			filtros.append("precio", precioLibro);
		}
		if (precioIVALibro != -1) {
			filtros.append("precioIVA", precioIVALibro);
		}
		
		Iterator<Document> it;
		FindIterable<Document> iterable;
		if (filtros.isEmpty()) {
			iterable = bd.getMongodb().getCollection("productos").find(new Document("libro", "1"));
		} else {
			iterable = bd.getMongodb().getCollection("productos").find(filtros);
		}
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			String id = registro.get("_id").toString();
			Libro libro = DAOLibro.select(id);
			resultado.put(id, libro);
		}
    	return resultado;
    }
	
	public static void delete(String login) {
		BaseDatos bd = new BaseDatos();
		
		Document filtro = new Document("usuario", login);
				
		bd.getMongodb().getCollection("usuarios").deleteOne(filtro);
	}
	
	public static String getMD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
}
