package gestionK.dao;

import gestionK.dominio.Anyo;
import gestionK.dominio.BaseDatos;

import java.util.Iterator;

import org.bson.Document;

import com.mongodb.client.FindIterable;

public class DAOAnyo {
	
	public static Anyo select(String idAnyo) throws Exception {
		BaseDatos bd = new BaseDatos();
		Anyo resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("anyos").find();
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("_id").toString().equals(idAnyo)) {
				resultado = new Anyo();
				resultado.setAnyo(registro.get("anyo").toString());
				resultado.setAbierto(registro.get("abierto").toString());
			}
		}
    	return resultado;
    }
	
	public static void insert(Anyo anyoNuevo) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		bd.getMongodb().getCollection("anyos").insertOne(
				new Document("anyo", anyoNuevo.getAnyo())
					.append("abierto", anyoNuevo.getAbierto()));
	}
	
	public static void update(String anyo, String abierto) throws Exception {
		BaseDatos bd = new BaseDatos();

		Document filtro = new Document("anyo", anyo);
			
		bd.getMongodb().getCollection("anyos").updateOne(filtro, new Document("$set",
				new Document("abierto", abierto)));
	}
	
	public static String selectAnyoAbierto() throws Exception {
		BaseDatos bd = new BaseDatos();
		String anyo = "";
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("anyos").find(
				new Document("abierto", "abierto"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			anyo = registro.get("anyo").toString();
		}
    	return anyo;
    }
}
