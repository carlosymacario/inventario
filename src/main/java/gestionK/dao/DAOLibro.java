package gestionK.dao;

import java.util.Iterator;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;

import gestionK.dominio.BaseDatos;
import gestionK.dominio.Libro;

public class DAOLibro {
	
	public static Libro select(String idLibro) throws Exception {
		BaseDatos bd = new BaseDatos();
		Libro resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("libro", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("_id").toString().equals(idLibro)) {
				resultado = new Libro();
				resultado.setId(idLibro);
				resultado.setCodigo(registro.get("codigo").toString());
				resultado.setTitulo(registro.get("titulo").toString());
				resultado.setAutor(registro.get("autor").toString());
				resultado.setEditorial(registro.get("editorial").toString());
				resultado.setDistribuidor(registro.get("distribuidor").toString());
				resultado.setFechaAlta(registro.get("fechaAlta").toString());
				resultado.setFechaModificacion(registro.get("fechaModificacion").toString());
				resultado.setCantidad(Integer.parseInt(registro.get("cantidad").toString()));
				resultado.setPrecio(Double.parseDouble(registro.get("precio").toString()));
				resultado.setPrecioIVA(Double.parseDouble(registro.get("precioIVA").toString()));
			}
		}
    	return resultado;
    }
	
	public static Boolean comprobarCodigo(String codigoArticulo) throws Exception {
		BaseDatos bd = new BaseDatos();
		Boolean resultado = false;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("libro", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("codigo").toString().equals(codigoArticulo)) {
				resultado = true;
			}
		}
    	return resultado;
	}
	
	public static Libro selectLibroTique(String codigoLibroTique) throws Exception {
		BaseDatos bd = new BaseDatos();
		Libro resultado = null;
	        
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(new Document("libro", "1"));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			if(registro.get("codigo").toString().equals(codigoLibroTique)) {
				resultado = new Libro();
				resultado.setId(registro.get("_id").toString());
				resultado.setCodigo(registro.get("codigo").toString());
				resultado.setTitulo(registro.get("titulo").toString());
				resultado.setAutor(registro.get("autor").toString());
				resultado.setEditorial(registro.get("editorial").toString());
				resultado.setDistribuidor(registro.get("distribuidor").toString());
				resultado.setFechaAlta(registro.get("fechaAlta").toString());
				resultado.setFechaModificacion(registro.get("fechaModificacion").toString());
				resultado.setCantidad(Integer.parseInt(registro.get("cantidad").toString()));
				resultado.setPrecio(Double.parseDouble(registro.get("precio").toString()));
				resultado.setPrecioIVA(Double.parseDouble(registro.get("precioIVA").toString()));
			}
		}
    	return resultado;
    }
	
	public static void insert(Libro libro) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		bd.getMongodb().getCollection("productos").insertOne(
				new Document("libro", "1")
					.append("codigo", libro.getCodigo())
					.append("titulo", libro.getTitulo())
					.append("autor", libro.getAutor())
					.append("editorial", libro.getEditorial())
					.append("distribuidor", libro.getDistribuidor())
					.append("fechaAlta", libro.getFechaAlta())
					.append("fechaModificacion", libro.getFechaModificacion())
					.append("cantidad", libro.getCantidad())
					.append("precio", libro.getPrecio())
					.append("precioIVA", libro.getPrecioIVA()));
		
		Iterator<Document> it;
		FindIterable<Document> iterable = bd.getMongodb().getCollection("productos").find(
				new Document("libro", "1")
				.append("codigo", libro.getCodigo())
				.append("titulo", libro.getTitulo())
				.append("autor", libro.getAutor())
				.append("editorial", libro.getEditorial())
				.append("distribuidor", libro.getDistribuidor())
				.append("fechaAlta", libro.getFechaAlta())
				.append("fechaModificacion", libro.getFechaModificacion())
				.append("cantidad", libro.getCantidad())
				.append("precio", libro.getPrecio())
				.append("precioIVA", libro.getPrecioIVA()));
		it = iterable.iterator();
		while (it.hasNext()) {
			Document registro = it.next();
			libro.setId(String.valueOf(registro.get("_id")));
		}
	 }
	 
	public static void update(Libro libro) throws Exception {
		BaseDatos bd = new BaseDatos();
			
		ObjectId id = new ObjectId(libro.getId());
		Document filtro = new Document("libro", "1").append("_id", id);

		bd.getMongodb().getCollection("productos").updateOne(filtro, new Document("$set",
				new Document("codigo", libro.getCodigo())
				.append("titulo", libro.getTitulo())
				.append("autor", libro.getAutor())
				.append("editorial", libro.getEditorial())
				.append("distribuidor", libro.getDistribuidor())
				.append("fechaModificacion", libro.getFechaModificacion())
				.append("cantidad", libro.getCantidad())
				.append("precio", libro.getPrecio())
				.append("precioIVA", libro.getPrecioIVA())));
	}
	 
	public static void delete(String idLibro) throws Exception {
		BaseDatos bd = new BaseDatos();
		
		ObjectId id = new ObjectId(idLibro);
		Document filtro = new Document("libro", "1").append("_id", id);
				
		bd.getMongodb().getCollection("productos").deleteOne(filtro);
	}
}
