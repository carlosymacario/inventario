"use strict";

function Usuario() {
	this.id=-1;
	this.email=null;
}

//Conectar

Usuario.prototype.conectar = function(key) {
	var request=new XMLHttpRequest();
	request.open("get", "conectar.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				usuario.email=resultado.email;
				usuario.id=resultado.id;
				sessionStorage.usuario=JSON.stringify(usuario);
				if (key==1) {
					usuario.cargarClientes();
				} else if (key==0) {
					usuario.cargarArticulos();
				} else if (key==2) {
					usuario.cargarLibros();
				} else if (key==3) {
					usuario.cargarTiques();
				} else if (key==4) {
					usuario.cargarFacturas();
				} else if (key==5) {
					usuario.cargarAlbaranes();
				} else if (key==6) {
					usuario.cargarPresupuestos();
				}
			} else if (resultado.tipo=="ERROR") {
				alert("Error al acceder al sistema: " + resultado.texto);
			}
		}
	};
	request.send();
}

Usuario.prototype.registro = function() {
	var email=document.getElementById("emailRegistro").value;
	var pwd=document.getElementById("passwordRegistro").value;
	var pwd2=document.getElementById("passwordRegistro2").value;

	var xrequest=new XMLHttpRequest();
	xrequest.open("post", "registrar.jsp");
	xrequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	xrequest.onreadystatechange = function() {
		if (xrequest.readyState==4) {
			var resultado=JSON.parse(xrequest.responseText);
			if (resultado.tipo=="OK") {
				usuario.email = resultado.email;
				usuario.id = resultado.id;
				sessionStorage.usuario=JSON.stringify(usuario);
				window.location.href = 'principal.html';
			} else {
				alert("Error al registrarse: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		email : email,
		pwd : pwd,
		pwd2 : pwd2
	};
	var linea="p=" + JSON.stringify(parametros);
	xrequest.send(linea);
}

Usuario.prototype.login = function() {
	var email=document.getElementById("emailLogin").value;
	var pwd=document.getElementById("passwordLogin").value;

	var xrequest=new XMLHttpRequest();
	xrequest.open("post", "identificar.jsp");
	xrequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	xrequest.onreadystatechange = function() {
		if (xrequest.readyState==4) {
			var resultado=JSON.parse(xrequest.responseText);
			if (resultado.tipo=="OK") {
				usuario.email = resultado.email;
				usuario.id = resultado.id;
				sessionStorage.usuario=JSON.stringify(usuario);
				window.location.href = 'principal.html';
			} else {
				alert("Error al hacer login: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		email : email,
		pwd : pwd
	};
	var linea="p=" + JSON.stringify(parametros);
	xrequest.send(linea);
}


Usuario.prototype.loginCookie = function() {
	var xrequest=new XMLHttpRequest();
	xrequest.open("post", "loginCookie.jsp");
	xrequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xrequest.onreadystatechange = function() {
		if (xrequest.readyState==4) {
			var resultado=JSON.parse(xrequest.responseText);
			if (resultado.tipo=="OK") {
				usuario.email=resultado.email;
				usuario.id=resultado.id;
				sessionStorage.usuario=JSON.stringify(usuario);
				window.location.href='principal.html';
			} else {
				alert("Error al hacer login con Cookie: " + resultado.texto);
			}
		}
	}
	xrequest.send();
}

//Años

Usuario.prototype.cargarAnyos = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarAnyos.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				var anyos=resultado.anyos;
				for (var i=0; i<anyos.length; i++) {
					var anyo=anyos[i];
					var tr=document.createElement("tr");
					tbodyAnyos.appendChild(tr);
					var td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<label id='lbAnyo"+ i +"'>"+ anyo.anyo +"</label>";
					td=document.createElement("td");
					tr.appendChild(td);
					if (anyo.abierto == "abierto") {
						td.innerHTML="<select id='selectAnyo"+ i +"'><option selected>abierto</option><option>cerrado</option></select>";
					} else {
						td.innerHTML="<select id='selectAnyo"+ i +"'><option>abierto</option><option selected>cerrado</option></select>";
					}
				}
			} else {
				alert("Error al cargar años: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.crearAnyo = function() {
	var nAnyos = document.getElementById("tbodyAnyos").rows.length;
	var i = parseInt(nAnyos) - parseInt(1);
	var nuevoAnyo = document.getElementById("lbAnyo"+i).value;
	var abierto = document.getElementById("selectAnyo"+i).value;
	
	anyoNuevo=new Anyo();
	anyoNuevo.crear(nuevoAnyo, abierto);
}

Usuario.prototype.modificarAnyos = function() {
	var nAnyos = document.getElementById("tbodyAnyos").rows.length;
	var anyos  = [];
	var objeto = {};
	
	for (var i=0; i<nAnyos; i++) {
		anyos.push({ 
			"anyo"         : document.getElementById("lbAnyo"+i).innerHTML,
			"abierto"      : document.getElementById("selectAnyo"+i).value
		});
		
	}
	
	objeto.anyos = anyos;
	
	anyoNuevo=new Anyo();
	anyoNuevo.modificar(anyos);
}

//Clientes

Usuario.prototype.crearCliente = function() {
	var nombre = document.getElementById("cajaNombreCliente").value;
	var apellido = document.getElementById("cajaApellidoCliente").value;
	var NIF = document.getElementById("cajaNIFCliente").value;
	var direccion = document.getElementById("cajaDireccionCliente").value;
	var localidad = document.getElementById("cajaLocalidadCliente").value;
	var CP = document.getElementById("cajaCPCliente").value;
	var provincia = document.getElementById("cajaProvinciaCliente").value;
	var pais = document.getElementById("cajaPaisCliente").value;
	var telefono = document.getElementById("cajaTelefonoCliente").value;
	var correo = document.getElementById("cajaCorreoCliente").value;
	if (nombre != '' && apellido != '' && NIF != '' && direccion != '' && localidad != '' && CP != '' && provincia != '' && pais != '') {
		cliente=new Cliente();
		cliente.crear(nombre, apellido, NIF, direccion, localidad, CP, provincia, pais, telefono, correo);
	} else {
		var alert=document.getElementById("alert");
		alert.innerHTML="<div class='col-xs-11 col-md-11 col-lg-11'><div class='alert alert-danger'><strong>¡Atención!</strong> Debe rellenar todos los campos obligatorios.</div></div>";
	}
}

Usuario.prototype.modificarCliente = function() {
	var nombre = document.getElementById("cajaNombreCliente").value;
	var apellido = document.getElementById("cajaApellidoCliente").value;
	var NIF = document.getElementById("cajaNIFCliente").value;
	var direccion = document.getElementById("cajaDireccionCliente").value;
	var localidad = document.getElementById("cajaLocalidadCliente").value;
	var CP = document.getElementById("cajaCPCliente").value;
	var provincia = document.getElementById("cajaProvinciaCliente").value;
	var pais = document.getElementById("cajaPaisCliente").value;
	var telefono = document.getElementById("cajaTelefonoCliente").value;
	var correo = document.getElementById("cajaCorreoCliente").value;
	if (nombre != '' && apellido != '' && NIF != '' && direccion != '' && localidad != '' && CP != '' && provincia != '' && pais != '') {
		cliente=new Cliente();
		cliente.modificar(sessionStorage.idCliente, nombre, apellido, NIF, direccion, localidad, CP, provincia, pais, telefono, correo);
	} else {
		var alert=document.getElementById("alert");
		alert.innerHTML="<div class='col-xs-6 col-md-6 col-lg-6'><div class='alert alert-danger'><strong>¡Atención!</strong> Debe rellenar todos los campos obligatorios.</div></div>";
	}
}

Usuario.prototype.cargarClientes = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarClientes.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				var clientes=resultado.clientes;
				document.getElementById("tdCargarClientes").innerHTML='';
				for (var i=0; i<clientes.length; i++) {
					var cliente=clientes[i];
					var href=document.createElement("a"); 
					document.getElementById("tdCargarClientes").appendChild(href);
					href.setAttribute("href", "javascript:mostrar('" + cliente.id + "')");
					href.setAttribute("class", "enlaces-lista");
					href.innerHTML=cliente.nombre + " " + cliente.apellido;
					var br=document.createElement("br"); 
					document.getElementById("tdCargarClientes").appendChild(br);
				}
			} else {
				alert("Error al cargar cliente: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.buscarClientes = function() {
	var nombre = document.getElementById("cajaNombreCliente").value;
	var apellido = document.getElementById("cajaApellidoCliente").value;
	var NIF = document.getElementById("cajaNIFCliente").value;
	var direccion = document.getElementById("cajaDireccionCliente").value;
	var localidad = document.getElementById("cajaLocalidadCliente").value;
	var CP = document.getElementById("cajaCPCliente").value;
	var provincia = document.getElementById("cajaProvinciaCliente").value;
	var pais = document.getElementById("cajaPaisCliente").value;
	var telefono = document.getElementById("cajaTelefonoCliente").value;
	var correo = document.getElementById("cajaCorreoCliente").value;
	
	var request=new XMLHttpRequest();
	request.open("post", "buscarClientes.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				usuario.clientes = resultado.clientes;
				usuario.mostrarBusquedaClientes();
			} else {
				alert("Error al buscar clientes: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		nombreCliente : nombre,
		apellidoCliente : apellido,
		NIFCliente : NIF,
		direccionCliente : direccion,
		localidadCliente : localidad,
		CPCliente : CP,
		provinciaCliente : provincia,
		paisCliente : pais,
		telefonoCliente : telefono,
		correoCliente : correo
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

Usuario.prototype.mostrarBusquedaClientes = function() {
	tableBusquedaCliente.innerHTML="<thead><tr><th>#</th><th>Nombre</th><th>Apellido</th><th>Modificar</th><tr><tbody id=tableListaBody></tbody></thead>";
	for (var i=0; i<this.clientes.length; i++) {
		var cliente=this.clientes[i];
		var tr=document.createElement("tr");
		tableListaBody.appendChild(tr);
		var td=document.createElement("td");
		tr.appendChild(td);
		td.innerHTML=i+1;
		td=document.createElement("td");
		tr.appendChild(td);
		td.innerHTML=cliente.nombre;
		td=document.createElement("td");
		tr.appendChild(td);
		td.innerHTML=cliente.apellido;
		td=document.createElement("td");
		var btnVer=document.createElement("button");
		td.appendChild(btnVer);
		btnVer.innerHTML="Modificar";
		btnVer.setAttribute("class", "boton");
		btnVer.setAttribute("onclick", "javascript:mostrar('" + cliente.id + "'); javascript:mostrarModificar()");
		tr.appendChild(td);
	}
}

//Articulos

Usuario.prototype.crearArticulo = function() {
	var codigo = document.getElementById("cajaCodigoArticulo").value;
	var nombre = document.getElementById("cajaNombreArticulo").value;
	var cantidad = document.getElementById("cajaCantidadArticulo").value;
	var precio = document.getElementById("cajaPrecioArticulo").value;
	var precioIVA = document.getElementById("cajaPrecioIVAArticulo").value;
	if (codigo != '' && nombre != '' && cantidad != '' && precioIVA != '') {
		articulo=new Articulo();
		articulo.crear(codigo, nombre, cantidad, precio, precioIVA);
	} else {
		var alert=document.getElementById("alert");
		alert.innerHTML="<div class='col-xs-11 col-md-11 col-lg-11'><div class='alert alert-danger'><strong>¡Atención!</strong> Debe rellenar todos los campos obligatorios.</div></div>";
	}
}

Usuario.prototype.modificarArticulo = function() {
	var codigo = document.getElementById("cajaCodigoArticulo").value;
	var nombre = document.getElementById("cajaNombreArticulo").value;
	var cantidad = document.getElementById("cajaCantidadArticulo").value;
	var precio = document.getElementById("cajaPrecioArticulo").value;
	var precioIVA = document.getElementById("cajaPrecioIVAArticulo").value;
	if (codigo != '' && nombre != '' && cantidad != '' && precioIVA != '') {
		articulo=new Articulo();
		articulo.modificar(sessionStorage.idArticulo, codigo, nombre, cantidad, precio, precioIVA);
	} else {
		var alert=document.getElementById("alert");
		alert.innerHTML="<div class='col-xs-6 col-md-6 col-lg-6'><div class='alert alert-danger'><strong>¡Atención!</strong> Debe rellenar todos los campos obligatorios.</div></div>";
	}
}

Usuario.prototype.cargarArticulos = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarArticulos.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				var articulos=resultado.articulos;
				document.getElementById("tdCargarArticulos").innerHTML='';
				for (var i=0; i<articulos.length; i++) {
					var articulo=articulos[i];
					var href=document.createElement("a"); 
					document.getElementById("tdCargarArticulos").appendChild(href);
					href.setAttribute("href", "javascript:mostrar('" + articulo.id + "')");
					href.setAttribute("class", "enlaces-lista");
					href.innerHTML=articulo.nombre;
					var br=document.createElement("br"); 
					document.getElementById("tdCargarArticulos").appendChild(br);
				}
			} else {
				alert("Error al cargar articulo: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.buscarArticulos = function() {
	var codigo = document.getElementById("cajaCodigoArticulo").value;
	var nombre = document.getElementById("cajaNombreArticulo").value;
	var cantidad = document.getElementById("cajaCantidadArticulo").value;
	var precio = document.getElementById("cajaPrecioArticulo").value;
	var precioIVA = document.getElementById("cajaPrecioIVAArticulo").value;
	
	var request=new XMLHttpRequest();
	request.open("post", "buscarArticulos.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				usuario.articulos = resultado.articulos;
				usuario.mostrarBusquedaArticulos();
			} else {
				alert("Error al buscar articulos: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		codigoArticulo : codigo,
		nombreArticulo : nombre,
		cantidadArticulo : cantidad,
		precioArticulo : precio,
		precioIVAArticulo : precioIVA
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

Usuario.prototype.mostrarBusquedaArticulos = function() {
	tableBusquedaArticulo.innerHTML="<thead><tr><th>#</th><th>Nombre</th><th>Modificar</th><tr><tbody id=tableListaBody></tbody></thead>";
	for (var i=0; i<this.articulos.length; i++) {
		var articulo=this.articulos[i];
		var tr=document.createElement("tr");
		tableListaBody.appendChild(tr);
		var td=document.createElement("td");
		tr.appendChild(td);
		td.innerHTML=i+1;
		td=document.createElement("td");
		tr.appendChild(td);
		td.innerHTML=articulo.nombre;
		td=document.createElement("td");
		var btnVer=document.createElement("button");
		td.appendChild(btnVer);
		btnVer.innerHTML="Modificar";
		btnVer.setAttribute("class", "boton");
		btnVer.setAttribute("onclick", "javascript:mostrar('" + articulo.id + "'); javascript:mostrarModificar()");
		tr.appendChild(td);
	}
}

//Libros

Usuario.prototype.crearLibro = function() {
	var codigo = document.getElementById("cajaCodigoLibro").value;
	var titulo = document.getElementById("cajaTituloLibro").value;
	var autor = document.getElementById("cajaAutorLibro").value;
	var editorial = document.getElementById("cajaEditorialLibro").value;
	var distribuidor = document.getElementById("cajaDistribuidorLibro").value;
	var cantidad = document.getElementById("cajaCantidadLibro").value;
	var precio = document.getElementById("cajaPrecioLibro").value;
	var precioIVA = document.getElementById("cajaPrecioIVALibro").value;
	if (codigo != '' && titulo != '' && autor != '' && editorial != '' && distribuidor != '' && cantidad != '' && precioIVA != '') {
		libro=new Libro();
		libro.crear(codigo, titulo, autor, editorial, distribuidor, cantidad, precio, precioIVA);
	} else {
		var alert=document.getElementById("alert");
		alert.innerHTML="<div class='col-xs-11 col-md-11 col-lg-11'><div class='alert alert-danger'><strong>¡Atención!</strong> Debe rellenar todos los campos obligatorios.</div></div>";
	}
}

Usuario.prototype.modificarLibro = function() {
	var codigo = document.getElementById("cajaCodigoLibro").value;
	var titulo = document.getElementById("cajaTituloLibro").value;
	var autor = document.getElementById("cajaAutorLibro").value;
	var editorial = document.getElementById("cajaEditorialLibro").value;
	var distribuidor = document.getElementById("cajaDistribuidorLibro").value;
	var cantidad = document.getElementById("cajaCantidadLibro").value;
	var precio = document.getElementById("cajaPrecioLibro").value;
	var precioIVA = document.getElementById("cajaPrecioIVALibro").value;
	if (codigo != '' && titulo != '' && autor != '' && editorial != '' && distribuidor != '' && cantidad != '' && precioIVA != '') {
		libro=new Libro();
		libro.modificar(sessionStorage.idLibro, codigo, titulo, autor, editorial, distribuidor, cantidad, precio, precioIVA);
	} else {
		var alert=document.getElementById("alert");
		alert.innerHTML="<div class='col-xs-6 col-md-6 col-lg-6'><div class='alert alert-danger'><strong>¡Atención!</strong> Debe rellenar todos los campos obligatorios.</div></div>";
	}
}

Usuario.prototype.cargarLibros = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarLibros.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				var libros=resultado.libros;
				document.getElementById("tdCargarLibros").innerHTML='';
				for (var i=0; i<libros.length; i++) {
					var libro=libros[i];
					var href=document.createElement("a"); 
					document.getElementById("tdCargarLibros").appendChild(href);
					href.setAttribute("href", "javascript:mostrar('" + libro.id + "')");
					href.setAttribute("class", "enlaces-lista");
					href.innerHTML=libro.titulo;
					var br=document.createElement("br"); 
					document.getElementById("tdCargarLibros").appendChild(br);
				}
			} else {
				alert("Error al cargar libro: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.buscarLibros = function() {
	var codigo = document.getElementById("cajaCodigoLibro").value;
	var titulo = document.getElementById("cajaTituloLibro").value;
	var autor = document.getElementById("cajaAutorLibro").value;
	var editorial = document.getElementById("cajaEditorialLibro").value;
	var distribuidor = document.getElementById("cajaDistribuidorLibro").value;
	var cantidad = document.getElementById("cajaCantidadLibro").value;
	var precio = document.getElementById("cajaPrecioLibro").value;
	var precioIVA = document.getElementById("cajaPrecioIVALibro").value;
	
	var request=new XMLHttpRequest();
	request.open("post", "buscarLibros.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				usuario.libros = resultado.libros;
				usuario.mostrarBusquedaLibros();
			} else {
				alert("Error al buscar libros: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		codigoLibro : codigo,
		tituloLibro : titulo,
		autorLibro : autor,
		editorialLibro : editorial,
		distribuidorLibro : distribuidor,
		cantidadLibro : cantidad,
		precioLibro : precio,
		precioIVALibro : precioIVA
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

Usuario.prototype.mostrarBusquedaLibros = function() {
	tableBusquedaLibro.innerHTML="<thead><tr><th>#</th><th>Nombre</th><th>Modificar</th><tr><tbody id=tableListaBody></tbody></thead>";
	for (var i=0; i<this.libros.length; i++) {
		var libro=this.libros[i];
		var tr=document.createElement("tr");
		tableListaBody.appendChild(tr);
		var td=document.createElement("td");
		tr.appendChild(td);
		td.innerHTML=i+1;
		td=document.createElement("td");
		tr.appendChild(td);
		td.innerHTML=libro.titulo;
		td=document.createElement("td");
		var btnVer=document.createElement("button");
		td.appendChild(btnVer);
		btnVer.innerHTML="Modificar";
		btnVer.setAttribute("class", "boton");
		btnVer.setAttribute("onclick", "javascript:mostrar('" + libro.id + "'); javascript:mostrarModificar()");
		tr.appendChild(td);
	}
}

//Tiques

Usuario.prototype.crearTique = function(key) {
	var total = document.getElementById("totalNuevo").innerHTML;
	var nProductos = document.getElementById("tbodyNuevoTique").rows.length;
	var lineaVacia = 0;
	var productos  = [];
	var objeto = {};

	for (var i=1; i<=nProductos; i++) {
		if (document.getElementById("codigoNuevo"+i).value != "") {
			productos.push({ 
				"codigo"      : document.getElementById("codigoNuevo"+i).value,
				"nombre"      : document.getElementById("nombreNuevo"+i).value,
				"cantidad"    : document.getElementById("cantidadNuevo"+i).value,
				"precio"      : document.getElementById("precioNuevo"+i).value,
				"importe"     : document.getElementById("importeNuevo"+i).value
			});
		}
	}
	
	objeto.productos = productos;
	
	tique=new Tique();
	tique.comprobarProductos(total, productos, key);

}

Usuario.prototype.cargarTiques = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarTiques.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				var tiques=resultado.tiques;
				//Se añaden todos los tiques a la session
				sessionStorage.tiques=JSON.stringify(tiques);
				var tique=tiques[0];
				//Se añade el último tique a la session
				sessionStorage.tique=JSON.stringify(tique);
				//Se muestra el último tique
				document.getElementById("tableTiqueBody").innerHTML='';
				document.getElementById("total").innerHTML=tique.total + " €";
				document.getElementById("fecha").innerHTML=tique.fecha;
				document.getElementById("numero").innerHTML=tique.numero;
				var productos=tique.productos;
				for (var i=0; i<productos.length; i++) {
					var producto=productos[i];
					var tr=document.createElement("tr");
					tableTiqueBody.appendChild(tr);
					var td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.codigo;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.nombre;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.cantidad;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.precio + " €";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.importe + " €";
				}
				//Se pone el total también en la pantalla modificar
				document.getElementById("totalNuevo").innerHTML=tique.total;
				var j = 0;
				for (var k=1; k<=productos.length; k++) {
					producto=productos[j];
					++j;
					//Se pinta el último tique en la pantalla modificar
					tr=document.createElement("tr");
					tbodyNuevoTique.appendChild(tr);
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
				}
			} else {
				alert("Error al cargar tique: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.tiqueAnteriorSiguiente = function(key) {
	var tiqueJSON = JSON.parse(sessionStorage.tique);
	var tiques = JSON.parse(sessionStorage.tiques);
	var tiqueAnteriorSiguiente = null;
	for(var i=0; i<tiques.length; i++) {
		if (key == 0) {
			if (tiques[i].numero < tiqueJSON.numero) {
				tiqueAnteriorSiguiente = tiques[i];
				break;
			}
		} else if (key == 1) {
			if (tiques[i].numero > tiqueJSON.numero) {
				tiqueAnteriorSiguiente = tiques[i];
			}
		}
	}
	if (tiqueAnteriorSiguiente != null) {
		//Se muestra el tique
		tbodyNuevoTique.innerHTML = "";
		//Se añade el tique a la session
		sessionStorage.tique=JSON.stringify(tiqueAnteriorSiguiente);
		document.getElementById("tableTiqueBody").innerHTML='';
		document.getElementById("total").innerHTML=tiqueAnteriorSiguiente.total + " €";
		document.getElementById("fecha").innerHTML=tiqueAnteriorSiguiente.fecha;
		document.getElementById("numero").innerHTML=tiqueAnteriorSiguiente.numero;
		var productos=tiqueAnteriorSiguiente.productos;
		for (var i=0; i<productos.length; i++) {
		//Se muestra el último tique
			var producto=productos[i];
			var tr=document.createElement("tr");
			tableTiqueBody.appendChild(tr);
			var td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.codigo;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.nombre;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.cantidad;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.precio + " €";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.importe + " €";
		}
		//Se pone el total también en la pantalla modificar
		document.getElementById("totalNuevo").innerHTML=tiqueAnteriorSiguiente.total;
		var j = 0;
		for (var k=1; k<=productos.length; k++) {
			producto=productos[j];
			++j;
			//Se pinta el último tique en la pantalla modificar
			tr=document.createElement("tr");
			tbodyNuevoTique.appendChild(tr);
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
		}
	}
}

Usuario.prototype.imprimirTique = function() {
	var tique = JSON.parse(sessionStorage.tique);
	
	var request=new XMLHttpRequest();
	request.open("post", "imprimirTique.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha impreso")
			} else {
				alert("Error al imprimir tique: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		tique : tique
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

//Facturas

Usuario.prototype.crearFactura = function(key) {
	var total = document.getElementById("totalNuevo").innerHTML;
	var nProductos = document.getElementById("tbodyNuevaFactura").rows.length;
	var lineaVacia = 0;
	var productos  = [];
	var objeto = {};

	for (var i=1; i<=nProductos; i++) {
		if (document.getElementById("codigoNuevo"+i).value != "") {
			productos.push({ 
				"codigo"      : document.getElementById("codigoNuevo"+i).value,
				"nombre"      : document.getElementById("nombreNuevo"+i).value,
				"cantidad"    : document.getElementById("cantidadNuevo"+i).value,
				"precio"      : document.getElementById("precioNuevo"+i).value,
				"importe"     : document.getElementById("importeNuevo"+i).value
			});
		}
	}
	
	objeto.productos = productos;
	
	if (productos.length == 0) {
		alert("No se puede guardar una factura sin productos");
	} else {
		factura=new Factura();
		factura.comprobarProductos(total, productos, key);
	}
}

Usuario.prototype.cargarFacturas = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarFacturas.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				//Antes de cargar las facturas se elimina de la session el cliente que haya
				sessionStorage.removeItem("cliente");
				var facturas=resultado.facturas;
				//Se añaden todas las facturas a la session
				sessionStorage.facturas=JSON.stringify(facturas);
				var factura=facturas[0];
				var cliente=factura.cliente;
				//Se añade la factura a la session
				sessionStorage.factura=JSON.stringify(factura);
				document.getElementById("tableFacturaBody").innerHTML='';
				document.getElementById("total").innerHTML=factura.total + " €";
				document.getElementById("fecha").innerHTML=factura.fecha;
				document.getElementById("numero").innerHTML=factura.numero;
				document.getElementById("cliente").innerHTML=cliente.nombre;
				var productos=factura.productos;
				for (var i=0; i<productos.length; i++) {
					//Se muestra la última factura
					var producto=productos[i];
					var tr=document.createElement("tr");
					tableFacturaBody.appendChild(tr);
					var td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.codigo;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.nombre;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.cantidad;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.precio + " €";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.importe + " €";
				}
				
				//Se pone el total también en la pantalla modificar
				document.getElementById("totalNuevo").innerHTML=factura.total;
				document.getElementById("clienteNuevo").value=cliente.nombre;
				var j = 0;
				for (var k=1; k<=productos.length; k++) {
					producto=productos[j];
					++j;
					//Se pinta la última factura en la pantalla modificar
					tr=document.createElement("tr");
					tbodyNuevaFactura.appendChild(tr);
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
				}
			} else {
				alert("Error al cargar factura: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.facturaAnteriorSiguiente = function(key) {
	var facturaJSON = JSON.parse(sessionStorage.factura);
	var facturas = JSON.parse(sessionStorage.facturas);
	var facturaAnteriorSiguiente = null;
	for(var i=0; i<facturas.length; i++) {
		if (key == 0) {
			if (facturas[i].numero < facturaJSON.numero) {
				facturaAnteriorSiguiente = facturas[i];
				break;
			}
		} else if (key == 1) {
			if (facturas[i].numero > facturaJSON.numero) {
				facturaAnteriorSiguiente = facturas[i];
			}
		}
	}
	if (facturaAnteriorSiguiente != null) {
		//Se muestra la factura
		tbodyNuevaFactura.innerHTML = "";
		//Se añade la factura a la session
		sessionStorage.factura=JSON.stringify(facturaAnteriorSiguiente);
		var cliente=facturaAnteriorSiguiente.cliente;
		document.getElementById("tableFacturaBody").innerHTML='';
		document.getElementById("total").innerHTML=facturaAnteriorSiguiente.total + " €";
		document.getElementById("fecha").innerHTML=facturaAnteriorSiguiente.fecha;
		document.getElementById("numero").innerHTML=facturaAnteriorSiguiente.numero;
		document.getElementById("cliente").innerHTML=cliente.nombre;
		var productos=facturaAnteriorSiguiente.productos;
		for (var i=0; i<productos.length; i++) {
			//Se muestra la última factura
			var producto=productos[i];
			var tr=document.createElement("tr");
			tableFacturaBody.appendChild(tr);
			var td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.codigo;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.nombre;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.cantidad;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.precio + " €";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.importe + " €";
		}		
		//Se pone el total también en la pantalla modificar
		document.getElementById("totalNuevo").innerHTML=facturaAnteriorSiguiente.total;
		document.getElementById("clienteNuevo").value=cliente.nombre;
		var j = 0;
		for (var k=1; k<=productos.length; k++) {
			producto=productos[j];
			++j;
			//Se pinta la última factura en la pantalla modificar
			tr=document.createElement("tr");
			tbodyNuevaFactura.appendChild(tr);
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
		}
	}
}

Usuario.prototype.imprimirFactura = function() {
	var factura = JSON.parse(sessionStorage.factura);
	
	var request=new XMLHttpRequest();
	request.open("post", "imprimirFactura.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha impreso")
			} else {
				alert("Error al imprimir factura: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		factura : factura
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

//Albaranes

Usuario.prototype.crearAlbaran = function(key) {
	var total = document.getElementById("totalNuevo").innerHTML;
	var nProductos = document.getElementById("tbodyNuevoAlbaran").rows.length;
	var lineaVacia = 0;
	var productos  = [];
	var objeto = {};

	for (var i=1; i<=nProductos; i++) {
		if (document.getElementById("codigoNuevo"+i).value != "") {
			productos.push({ 
				"codigo"      : document.getElementById("codigoNuevo"+i).value,
				"nombre"      : document.getElementById("nombreNuevo"+i).value,
				"cantidad"    : document.getElementById("cantidadNuevo"+i).value,
				"precio"      : document.getElementById("precioNuevo"+i).value,
				"importe"     : document.getElementById("importeNuevo"+i).value
			});
		}
	}
	
	objeto.productos = productos;
	
	if (productos.length == 0) {
		alert("No se puede guardar un albarán sin productos");
	} else {
		albaran=new Albaran();
		//Albarán nuevo
		if (key==0) {
			albaran.crear(total, productos);
		//Albarán modificado
		} else if (key==1){
			albaran.modificar(total, productos);
		}
	}
}

Usuario.prototype.cargarAlbaranes = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarAlbaranes.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				//Antes de cargar los albaranes se elimina de la session el cliente que haya
				sessionStorage.removeItem("cliente");
				var albaranes=resultado.albaranes;
				//Se añaden todas los albaranes a la session
				sessionStorage.albaranes=JSON.stringify(albaranes);
				var albaran=albaranes[0];
				var cliente=albaran.cliente;
				//Se añade el albarán a la session
				sessionStorage.albaran=JSON.stringify(albaran);
				document.getElementById("tableAlbaranBody").innerHTML='';
				document.getElementById("total").innerHTML=albaran.total + " €";
				document.getElementById("fecha").innerHTML=albaran.fecha;
				document.getElementById("numero").innerHTML=albaran.numero;
				document.getElementById("cliente").innerHTML=cliente.nombre;
				var productos=albaran.productos;
				for (var i=0; i<productos.length; i++) {
					//Se muestra el último albarán
					var producto=productos[i];
					var tr=document.createElement("tr");
					tableAlbaranBody.appendChild(tr);
					var td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.codigo;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.nombre;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.cantidad;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.precio + " €";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.importe + " €";
				}
				
				//Se pone el total también en la pantalla modificar
				document.getElementById("totalNuevo").innerHTML=albaran.total;
				document.getElementById("clienteNuevo").value=cliente.nombre;
				var j = 0;
				for (var k=1; k<=productos.length; k++) {
					producto=productos[j];
					++j;
					//Se pinta el último albarán en la pantalla modificar
					tr=document.createElement("tr");
					tbodyNuevoAlbaran.appendChild(tr);
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
				}
			} else {
				alert("Error al cargar albarán: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.albaranAnteriorSiguiente = function(key) {
	var albaranJSON = JSON.parse(sessionStorage.albaran);
	var albaranes = JSON.parse(sessionStorage.albaranes);
	var albaranAnteriorSiguiente = null;
	for(var i=0; i<albaranes.length; i++) {
		if (key == 0) {
			if (albaranes[i].numero < albaranJSON.numero) {
				albaranAnteriorSiguiente = albaranes[i];
				break;
			}
		} else if (key == 1) {
			if (albaranes[i].numero > albaranJSON.numero) {
				albaranAnteriorSiguiente = albaranes[i];
			}
		}
	}
	if (albaranAnteriorSiguiente != null) {
		//Se muestra el albarán
		tbodyNuevoAlbaran.innerHTML = "";
		//Se añade el albarán a la session
		sessionStorage.albaran=JSON.stringify(albaranAnteriorSiguiente);
		var cliente=albaranAnteriorSiguiente.cliente;
		document.getElementById("tableAlbaranBody").innerHTML='';
		document.getElementById("total").innerHTML=albaranAnteriorSiguiente.total + " €";
		document.getElementById("fecha").innerHTML=albaranAnteriorSiguiente.fecha;
		document.getElementById("numero").innerHTML=albaranAnteriorSiguiente.numero;
		document.getElementById("cliente").innerHTML=cliente.nombre;
		var productos=albaranAnteriorSiguiente.productos;
		for (var i=0; i<productos.length; i++) {
			//Se muestra el último albarán
			var producto=productos[i];
			var tr=document.createElement("tr");
			tableAlbaranBody.appendChild(tr);
			var td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.codigo;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.nombre;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.cantidad;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.precio + " €";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.importe + " €";
		}		
		//Se pone el total también en la pantalla modificar
		document.getElementById("totalNuevo").innerHTML=albaranAnteriorSiguiente.total;
		document.getElementById("clienteNuevo").value=cliente.nombre;
		var j = 0;
		for (var k=1; k<=productos.length; k++) {
			producto=productos[j];
			++j;
			//Se pinta el último albarán en la pantalla modificar
			tr=document.createElement("tr");
			tbodyNuevoAlbaran.appendChild(tr);
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
		}
	}
}

Usuario.prototype.imprimirAlbaran = function() {
	var albaran = JSON.parse(sessionStorage.albaran);
	
	var request=new XMLHttpRequest();
	request.open("post", "imprimirAlbaran.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha impreso")
			} else {
				alert("Error al imprimir albaran: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		albaran : albaran
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

//Presupuestos

Usuario.prototype.crearPresupuesto = function(key) {
	var total = document.getElementById("totalNuevo").innerHTML;
	var nProductos = document.getElementById("tbodyNuevoPresupuesto").rows.length;
	var lineaVacia = 0;
	var productos  = [];
	var objeto = {};

	for (var i=1; i<=nProductos; i++) {
		if (document.getElementById("codigoNuevo"+i).value != "") {
			productos.push({ 
				"codigo"      : document.getElementById("codigoNuevo"+i).value,
				"nombre"      : document.getElementById("nombreNuevo"+i).value,
				"cantidad"    : document.getElementById("cantidadNuevo"+i).value,
				"precio"      : document.getElementById("precioNuevo"+i).value,
				"importe"     : document.getElementById("importeNuevo"+i).value
			});
		}
	}
	
	objeto.productos = productos;
	
	if (productos.length == 0) {
		alert("No se puede guardar un presupuesto sin productos");
	} else {
		presupuesto=new Presupuesto();
		//Presupuesto nuevo
		if (key==0) {
			presupuesto.crear(total, productos);
		//Presupuesto modificado
		} else if (key==1){
			presupuesto.modificar(total, productos);
		}
	}
}

Usuario.prototype.cargarPresupuestos = function() {
	var request=new XMLHttpRequest();
	request.open("get", "cargarPresupuestos.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				//Antes de cargar los presupuestos se elimina de la session el cliente que haya
				sessionStorage.removeItem("cliente");
				var presupuestos=resultado.presupuestos;
				//Se añaden todas los presupuestos a la session
				sessionStorage.presupuestos=JSON.stringify(presupuestos);
				var presupuesto=presupuestos[0];
				var cliente=presupuesto.cliente;
				//Se añade el presupuesto a la session
				sessionStorage.presupuesto=JSON.stringify(presupuesto);
				document.getElementById("tablePresupuestoBody").innerHTML='';
				document.getElementById("total").innerHTML=presupuesto.total + " €";
				document.getElementById("fecha").innerHTML=presupuesto.fecha;
				document.getElementById("numero").innerHTML=presupuesto.numero;
				document.getElementById("cliente").innerHTML=cliente.nombre;
				var productos=presupuesto.productos;
				for (var i=0; i<productos.length; i++) {
					//Se muestra el último presupuesto
					var producto=productos[i];
					var tr=document.createElement("tr");
					tablePresupuestoBody.appendChild(tr);
					var td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.codigo;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.nombre;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.cantidad;
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.precio + " €";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML=producto.importe + " €";
				}
				
				//Se pone el total también en la pantalla modificar
				document.getElementById("totalNuevo").innerHTML=presupuesto.total;
				document.getElementById("clienteNuevo").value=cliente.nombre;
				var j = 0;
				for (var k=1; k<=productos.length; k++) {
					producto=productos[j];
					++j;
					//Se pinta el último presupuesto en la pantalla modificar
					tr=document.createElement("tr");
					tbodyNuevoPresupuesto.appendChild(tr);
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
					td=document.createElement("td");
					tr.appendChild(td);
					td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
				}
			} else {
				alert("Error al cargar presupuesto: " + resultado.texto);
			}
		}
	}
	request.send();
}

Usuario.prototype.presupuestoAnteriorSiguiente = function(key) {
	var presupuestoJSON = JSON.parse(sessionStorage.presupuesto);
	var presupuestos = JSON.parse(sessionStorage.presupuestos);
	var presupuestoAnteriorSiguiente = null;
	for(var i=0; i<presupuestos.length; i++) {
		if (key == 0) {
			if (presupuestos[i].numero < presupuestoJSON.numero) {
				presupuestoAnteriorSiguiente = presupuestos[i];
				break;
			}
		} else if (key == 1) {
			if (presupuestos[i].numero > presupuestoJSON.numero) {
				presupuestoAnteriorSiguiente = presupuestos[i];
			}
		}
	}
	if (presupuestoAnteriorSiguiente != null) {
		//Se muestra el presupuesto
		tbodyNuevoPresupuesto.innerHTML = "";
		//Se añade el presupuesto a la session
		sessionStorage.presupuesto=JSON.stringify(presupuestoAnteriorSiguiente);
		var cliente=presupuestoAnteriorSiguiente.cliente;
		document.getElementById("tablePresupuestoBody").innerHTML='';
		document.getElementById("total").innerHTML=presupuestoAnteriorSiguiente.total + " €";
		document.getElementById("fecha").innerHTML=presupuestoAnteriorSiguiente.fecha;
		document.getElementById("numero").innerHTML=presupuestoAnteriorSiguiente.numero;
		document.getElementById("cliente").innerHTML=cliente.nombre;
		var productos=presupuestoAnteriorSiguiente.productos;
		for (var i=0; i<productos.length; i++) {
			//Se muestra el último presupuesto
			var producto=productos[i];
			var tr=document.createElement("tr");
			tablePresupuestoBody.appendChild(tr);
			var td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.codigo;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.nombre;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.cantidad;
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.precio + " €";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML=producto.importe + " €";
		}		
		//Se pone el total también en la pantalla modificar
		document.getElementById("totalNuevo").innerHTML=presupuestoAnteriorSiguiente.total;
		document.getElementById("clienteNuevo").value=cliente.nombre;
		var j = 0;
		for (var k=1; k<=productos.length; k++) {
			producto=productos[j];
			++j;
			//Se pinta el último presupuesto en la pantalla modificar
			tr=document.createElement("tr");
			tbodyNuevoPresupuesto.appendChild(tr);
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='codigoNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 0)' class='table-body-control' value='"+ producto.codigo +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='text' id='nombreNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 1)' class='table-body-control' value='"+ producto.nombre +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='cantidadNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 2)' class='table-body-control' value='"+ producto.cantidad +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='precioNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 3)' class='table-body-control' value='"+ producto.precio +"'>";
			td=document.createElement("td");
			tr.appendChild(td);
			td.innerHTML="<input type='number' id='importeNuevo"+k+"' onkeyup='onKeyUp(event, "+k+", 4)' class='table-body-control' value='"+ producto.importe +"'>";
		}
	}
}

Usuario.prototype.imprimirPresupuesto = function() {
	var presupuesto = JSON.parse(sessionStorage.presupuesto);
	
	var request=new XMLHttpRequest();
	request.open("post", "imprimirPresupuesto.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha impreso")
			} else {
				alert("Error al imprimir presupuesto: " + resultado.texto);
			}
		} 
	}
	
	var parametros= {
		presupuesto : presupuesto
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

//Se utiliza para tique, factura, presupuesto y albaranes.
Usuario.prototype.buscarProductoTique = function(key) {
	var codigo = document.getElementById("codigoNuevo"+key).value;
	
	var request=new XMLHttpRequest();
	request.open("post", "buscarProductoTique.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				document.getElementById("nombreNuevo"+key).value = resultado.nombre;
				document.getElementById("precioNuevo"+key).value = resultado.precioIVA;
			} else {
				var mensaje = confirm("Este producto no está dado de alta. ¿Desea darlo de alta?");
				if(mensaje){
					$("#alertaAlta").addClass("show");
					$("#alertaAlta").removeClass("hide");
				}else {
					document.getElementById("nombreNuevo"+key).focus();
				}
			}
		} 
	}
	
	var parametros= {
		codigoProductoTique : codigo
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

//Se utiliza en facturas, albaranes y presupuestos para buscar el cliente
Usuario.prototype.buscarClienteTique = function() {
	var dni = document.getElementById("clienteNuevo").value;
	var request=new XMLHttpRequest();
	request.open("post", "buscarClienteTique.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				var cliente = resultado.cliente
				sessionStorage.cliente=JSON.stringify(cliente);
				document.getElementById("clienteNuevo").value = cliente.nombre;
				$("#clienteNuevo").css("background-color", "#19ff00");
			} else {
				alert("Este cliente no está dado de alta: " + resultado.texto);
				$("#clienteNuevo").css("background-color", "red");
			}
		} 
	}
	
	var parametros= {
		dniClienteTique : dni
	};
	var linea="p=" + JSON.stringify(parametros);
	request.send(linea);
}

function Anyo() {
	this.anyo=null;
	this.abierto=null;
}

Anyo.prototype.modificar = function(anyos) {
	var request=new XMLHttpRequest();
	request.open("post", "modificarAnyo.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se han modificado los años abiertos");
			} else if (resultado.tipo=="ERROR"){
				alert("No se pueden modificar los años: " + resultado.texto);
			}
		}
	}
	var p = {
			anyos : anyos
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Anyo.prototype.crear = function(anyoNuevo, abierto) {
	var request=new XMLHttpRequest();
	request.open("post", "crearAnyo.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha añadido un nuevo año");
				//Se cargan los años de nuevo para que aparezca el nuevo año
				document.getElementById("tbodyAnyos").innerHTML = "";
				
				//Ocultar botones
				$("#btnAnyadirAnyo").addClass("show");
				$("#btnAnyadirAnyo").removeClass("hide");
				$("#btnGuardarAnyo").addClass("show");
				$("#btnGuardarAnyo").removeClass("hide");
				//Mostrar botón Guardar y Cancelar
				$("#btnGuardarAnyoNuevo").addClass("hide");
				$("#btnGuardarAnyoNuevo").removeClass("show");
				$("#btnCancelar").addClass("hide");
				$("#btnCancelar").removeClass("show");
				
				usuario.cargarAnyos();
			} else if (resultado.tipo=="ERROR"){
				alert("No es posible añadir un nuevo año: "+ resultado.texto);
			}
		}
	}
	var p = {
			anyoNuevo : anyoNuevo,
			abierto : abierto
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

function Cliente() {
	this.idCliente=-1;
	this.nombre=null;
	this.apellido=null;
	this.NIF=null;
	this.direccion=null;
	this.localidad=null;
	this.CP=null;
	this.provincia=null;
	this.pais=null;
	this.telefono=null;
	this.correo=null;
}

Cliente.prototype.cargar = function(idCliente) {
	var request=new XMLHttpRequest();
	request.open("post", "cargarClientes.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				cliente.idCliente=idCliente;
				sessionStorage.idCliente=this.idCliente;
				for (var i=0; i<resultado.clientes.length; i++) {
					if (resultado.clientes[i].id == idCliente) {
						cliente.nombre=resultado.clientes[i].nombre;
						cliente.apellido=resultado.clientes[i].apellido;
						cliente.NIF=resultado.clientes[i].nif;
						cliente.direccion=resultado.clientes[i].direccion;
						cliente.localidad=resultado.clientes[i].localidad;
						cliente.CP=resultado.clientes[i].cp;
						cliente.provincia=resultado.clientes[i].provincia;
						cliente.pais=resultado.clientes[i].pais;
						cliente.telefono=resultado.clientes[i].telefono;
						cliente.correo=resultado.clientes[i].correo;
						cliente.mostrar();
						cliente.mostrar2();
					}
				}
			} else if (resultado.tipo=="ERROR") {
				alert("Error al cargar los clientes: " + resultado.texto);
			} 
		}
	}
	request.send("idCliente=" + idCliente);
}

Cliente.prototype.mostrar = function() {
	document.getElementById("cajaNombreCliente2").value = this.nombre;
	document.getElementById("cajaApellidoCliente2").value = this.apellido;
	document.getElementById("cajaNIFCliente2").value = this.NIF;
	document.getElementById("cajaDireccionCliente2").value = this.direccion;
	document.getElementById("cajaLocalidadCliente2").value = this.localidad;
	document.getElementById("cajaCPCliente2").value = this.CP;
	document.getElementById("cajaProvinciaCliente2").value = this.provincia;
	document.getElementById("cajaPaisCliente2").value = this.pais;
	document.getElementById("cajaTelefonoCliente2").value = this.telefono;
	document.getElementById("cajaCorreoCliente2").value = this.correo;
	sessionStorage.idCliente=this.idCliente;
}

Cliente.prototype.mostrar2 = function() {
	document.getElementById("cajaNombreCliente").value = this.nombre;
	document.getElementById("cajaApellidoCliente").value = this.apellido;
	document.getElementById("cajaNIFCliente").value = this.NIF;
	document.getElementById("cajaDireccionCliente").value = this.direccion;
	document.getElementById("cajaLocalidadCliente").value = this.localidad;
	document.getElementById("cajaCPCliente").value = this.CP;
	document.getElementById("cajaProvinciaCliente").value = this.provincia;
	document.getElementById("cajaPaisCliente").value = this.pais;
	document.getElementById("cajaTelefonoCliente").value = this.telefono;
	document.getElementById("cajaCorreoCliente").value = this.correo;
}

Cliente.prototype.crear = function(nombre, apellido, NIF, direccion, localidad, CP, provincia, pais, telefono, correo) {
	var request=new XMLHttpRequest();
	request.open("post", "crearCliente.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha creado el cliente");
				sessionStorage.idCliente=resultado.idCliente;
				usuario.cargarClientes();
				document.getElementById("tdClientes").setAttribute("style", "display:visible");
				document.getElementById("tdCrearCliente").setAttribute("style", "display:none");
				$("#botonesSuperiores").addClass("show");
				$("#botonesSuperiores").removeClass("hide");
			} else if (resultado.tipo=="ERROR") {
				alert("Error al crear un cliente: " + resultado.texto);
			} 
		}
	}
	var p = {
			nombreCliente : nombre,
			apellidoCliente : apellido,
			NIFCliente : NIF,
			direccionCliente : direccion,
			localidadCliente : localidad,
			CPCliente : CP,
			provinciaCliente : provincia,
			paisCliente : pais,
			telefonoCliente : telefono,
			correoCliente : correo
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Cliente.prototype.modificar = function(id, nombre, apellido, NIF, direccion, localidad, CP, provincia, pais, telefono, correo) {
	var request=new XMLHttpRequest();
	request.open("post", "modificarCliente.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha modificado el cliente");
				usuario.cargarClientes();
				cliente.cargar(sessionStorage.idCliente);
				document.getElementById("tdClientes").setAttribute("style", "display:visible");
				document.getElementById("tdCrearCliente").setAttribute("style", "display:none");
				clienteBuscar.setAttribute("style", "display:none");
				$("#botonesSuperiores").addClass("show");
				$("#botonesSuperiores").removeClass("hide");
			} else if (resultado.tipo=="ERROR") {
				alert("Error al modificar un cliente: " + resultado.texto);
			} 
		}
	}
	var p = {
			idCliente : id,
			nombreCliente : nombre,
			apellidoCliente : apellido,
			NIFCliente : NIF,
			direccionCliente : direccion,
			localidadCliente : localidad,
			CPCliente : CP,
			provinciaCliente : provincia,
			paisCliente : pais,
			telefonoCliente : telefono,
			correoCliente : correo
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Cliente.prototype.eliminar = function(id) {
	var opcion = confirm("¿Seguro quiere eliminar este cliente?");
	if (opcion == true) {
		var request=new XMLHttpRequest();
		request.open("post", "eliminarCliente.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha eliminado el cliente");
					$("#tableCliente").addClass("hide");
					$("#tableCliente").removeClass("show");
					document.getElementById("cajaNombreCliente").value = '';
					document.getElementById("cajaApellidoCliente").value = '';
					document.getElementById("cajaNIFCliente").value = '';
					document.getElementById("cajaDireccionCliente").value = '';
					document.getElementById("cajaLocalidadCliente").value = '';
					document.getElementById("cajaCPCliente").value = '';
					document.getElementById("cajaProvinciaCliente").value = '';
					document.getElementById("cajaPaisCliente").value = '';
					document.getElementById("cajaTelefonoCliente").value = '';
					document.getElementById("cajaCorreoCliente").value = '';
					document.getElementById("alert").innerHTML = "";
					document.getElementById("btnModificarCliente").disabled = true;
					document.getElementById("btnEliminarCliente").disabled = true;
					sessionStorage.removeItem("idCliente");
					usuario.cargarClientes();
				} else if (resultado.tipo=="ERROR") {
					alert("Error al eliminar un cliente: " + resultado.texto);
				} 
			}
		}
		var p = {
				idCliente : id
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

function Articulo() {
	this.idArticulo=-1;
	this.codigo=null;
	this.nombre=null;
	this.fehcaAlta=null;
	this.fehcaModificacion=null;
	this.cantidad=null;
	this.precio=null;
	this.precioIVA=null;
}

Articulo.prototype.cargar = function(idArticulo) {
	var request=new XMLHttpRequest();
	request.open("post", "cargarArticulos.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				articulo.idArticulo=idArticulo;
				sessionStorage.idArticulo=this.idArticulo;
				for (var i=0; i<resultado.articulos.length; i++) {
					if (resultado.articulos[i].id == idArticulo) {
						articulo.codigo=resultado.articulos[i].codigo;
						articulo.nombre=resultado.articulos[i].nombre;
						articulo.fechaAlta=resultado.articulos[i].fechaAlta;
						articulo.fechaModificacion=resultado.articulos[i].fechaModificacion;
						articulo.cantidad=resultado.articulos[i].cantidad;
						articulo.precio=resultado.articulos[i].precio;
						articulo.precioIVA=resultado.articulos[i].precioIVA;
						articulo.mostrar();
						articulo.mostrar2();
					}
				}
			} else if (resultado.tipo=="ERROR") {
				alert("Error al cargar los articulos: " + resultado.texto);
			} 
		}
	}
	request.send("idArticulo=" + idArticulo);
}

Articulo.prototype.mostrar = function() {
	document.getElementById("cajaCodigoArticulo2").value = this.codigo;
	document.getElementById("cajaNombreArticulo2").value = this.nombre;
	document.getElementById("cajaFechaAltaArticulo2").value = this.fechaAlta;
	document.getElementById("cajaFechaModificacionArticulo2").value = this.fechaModificacion;
	document.getElementById("cajaCantidadArticulo2").value = this.cantidad;
	document.getElementById("cajaPrecioArticulo2").value = this.precio;
	document.getElementById("cajaPrecioIVAArticulo2").value = this.precioIVA;
	sessionStorage.idArticulo=this.idArticulo;
}

Articulo.prototype.mostrar2 = function() {
	document.getElementById("cajaCodigoArticulo").value = this.codigo;
	document.getElementById("cajaNombreArticulo").value = this.nombre;
	document.getElementById("cajaCantidadArticulo").value = this.cantidad;
	document.getElementById("cajaPrecioArticulo").value = this.precio;
	document.getElementById("cajaPrecioIVAArticulo").value = this.precioIVA;
}

Articulo.prototype.crear = function(codigo, nombre, cantidad, precio, precioIVA) {
	var request=new XMLHttpRequest();
	request.open("post", "crearArticulo.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha creado el articulo");
				sessionStorage.idArticulo=resultado.idArticulo;
				usuario.cargarArticulos();
				document.getElementById("tdArticulos").setAttribute("style", "display:visible");
				document.getElementById("tdCrearArticulo").setAttribute("style", "display:none");
				$("#botonesSuperiores").addClass("show");
				$("#botonesSuperiores").removeClass("hide");
			} else if (resultado.tipo=="ERROR") {
				alert("Error al crear un articulo: " + resultado.texto);
			} 
		}
	}
	var p = {
			codigoArticulo : codigo,
			nombreArticulo : nombre,
			cantidadArticulo : cantidad,
			precioArticulo : precio,
			precioIVAArticulo : precioIVA
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Articulo.prototype.modificar = function(id, codigo, nombre, cantidad, precio, precioIVA) {
	var request=new XMLHttpRequest();
	request.open("post", "modificarArticulo.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha modificado el articulo");
				usuario.cargarArticulos();
				articulo.cargar(sessionStorage.idArticulo);
				document.getElementById("tdArticulos").setAttribute("style", "display:visible");
				document.getElementById("tdCrearArticulo").setAttribute("style", "display:none");
				articuloBuscar.setAttribute("style", "display:none");
				$("#botonesSuperiores").addClass("show");
				$("#botonesSuperiores").removeClass("hide");
			} else if (resultado.tipo=="ERROR") {
				alert("Error al modificar un articulo: " + resultado.texto);
			} 
		}
	}
	var p = {
			idArticulo : id,
			codigoArticulo : codigo,
			nombreArticulo : nombre,
			cantidadArticulo : cantidad,
			precioArticulo : precio,
			precioIVAArticulo : precioIVA
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Articulo.prototype.eliminar = function(id) {
	var opcion = confirm("¿Seguro quiere eliminar este artículo?");
	if (opcion == true) {
		var request=new XMLHttpRequest();
		request.open("post", "eliminarArticulo.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha eliminado el articulo");
					$("#tableArticulo").addClass("hide");
					$("#tableArticulo").removeClass("show");
					document.getElementById("cajaCodigoArticulo").value = '';
					document.getElementById("cajaNombreArticulo").value = '';
					document.getElementById("cajaCantidadArticulo").value = '';
					document.getElementById("cajaPrecioArticulo").value = '';
					document.getElementById("cajaPrecioIVAArticulo").value = '';
					document.getElementById("alert").innerHTML = "";
					document.getElementById("btnModificarArticulo").disabled = true;
					document.getElementById("btnEliminarArticulo").disabled = true;
					sessionStorage.removeItem("idArticulo");
					usuario.cargarArticulos();
				} else if (resultado.tipo=="ERROR") {
					alert("Error al eliminar un artículo: " + resultado.texto);
				} 
			}
		}
		var p = {
				idArticulo : id
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

function Libro() {
	this.idLibro=-1;
	this.codigo=null;
	this.titulo=null;
	this.autor=null;
	this.editorial=null;
	this.distribuidor=null;
	this.fehcaAlta=null;
	this.fehcaModificacion=null;
	this.cantidad=null;
	this.precio=null;
	this.precioIVA=null;
}

Libro.prototype.cargar = function(idLibro) {
	var request=new XMLHttpRequest();
	request.open("post", "cargarLibros.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				libro.idLibro=idLibro;
				sessionStorage.idLibro=this.idLibro;
				for (var i=0; i<resultado.libros.length; i++) {
					if (resultado.libros[i].id == idLibro) {
						libro.codigo=resultado.libros[i].codigo;
						libro.titulo=resultado.libros[i].titulo;
						libro.autor=resultado.libros[i].autor;
						libro.editorial=resultado.libros[i].editorial;
						libro.distribuidor=resultado.libros[i].distribuidor;
						libro.fechaAlta=resultado.libros[i].fechaAlta;
						libro.fechaModificacion=resultado.libros[i].fechaModificacion;
						libro.cantidad=resultado.libros[i].cantidad;
						libro.precio=resultado.libros[i].precio;
						libro.precioIVA=resultado.libros[i].precioIVA;
						libro.mostrar();
						libro.mostrar2();
					}
				}
			} else if (resultado.tipo=="ERROR") {
				alert("Error al cargar los libros: " + resultado.texto);
			} 
		}
	}
	request.send("idLibro=" + idLibro);
}

Libro.prototype.mostrar = function() {
	document.getElementById("cajaCodigoLibro2").value = this.codigo;
	document.getElementById("cajaTituloLibro2").value = this.titulo;
	document.getElementById("cajaAutorLibro2").value = this.autor;
	document.getElementById("cajaEditorialLibro2").value = this.editorial;
	document.getElementById("cajaDistribuidorLibro2").value = this.distribuidor;
	document.getElementById("cajaFechaAltaLibro2").value = this.fechaAlta;
	document.getElementById("cajaFechaModificacionLibro2").value = this.fechaModificacion;
	document.getElementById("cajaCantidadLibro2").value = this.cantidad;
	document.getElementById("cajaPrecioLibro2").value = this.precio;
	document.getElementById("cajaPrecioIVALibro2").value = this.precioIVA;
	sessionStorage.idLibro=this.idLibro;
}

Libro.prototype.mostrar2 = function() {
	document.getElementById("cajaCodigoLibro").value = this.codigo;
	document.getElementById("cajaTituloLibro").value = this.titulo;
	document.getElementById("cajaAutorLibro").value = this.autor;
	document.getElementById("cajaEditorialLibro").value = this.editorial;
	document.getElementById("cajaDistribuidorLibro").value = this.distribuidor;
	document.getElementById("cajaCantidadLibro").value = this.cantidad;
	document.getElementById("cajaPrecioLibro").value = this.precio;
	document.getElementById("cajaPrecioIVALibro").value = this.precioIVA;
}

Libro.prototype.crear = function(codigo, titulo, autor, editorial, distribuidor, cantidad, precio, precioIVA) {
	var request=new XMLHttpRequest();
	request.open("post", "crearLibro.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha creado el libro");
				sessionStorage.idLibro=resultado.idLibro;
				usuario.cargarLibros();
				document.getElementById("tdLibros").setAttribute("style", "display:visible");
				document.getElementById("tdCrearLibro").setAttribute("style", "display:none");
				$("#botonesSuperiores").addClass("show");
				$("#botonesSuperiores").removeClass("hide");
			} else if (resultado.tipo=="ERROR") {
				alert("Error al crear un libro: " + resultado.texto);
			} 
		}
	}
	var p = {
			codigoLibro : codigo,
			tituloLibro : titulo,
			autorLibro : autor,
			editorialLibro : editorial,
			distribuidorLibro : distribuidor,
			cantidadLibro : cantidad,
			precioLibro : precio,
			precioIVALibro : precioIVA
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Libro.prototype.modificar = function(id, codigo, titulo, autor, editorial, distribuidor, cantidad, precio, precioIVA) {
	var request=new XMLHttpRequest();
	request.open("post", "modificarLibro.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha modificado el libro");
				usuario.cargarLibros();
				libro.cargar(sessionStorage.idLibro);
				document.getElementById("tdLibros").setAttribute("style", "display:visible");
				document.getElementById("tdCrearLibro").setAttribute("style", "display:none");
				libroBuscar.setAttribute("style", "display:none");
				$("#botonesSuperiores").addClass("show");
				$("#botonesSuperiores").removeClass("hide");
			} else if (resultado.tipo=="ERROR") {
				alert("Error al modificar un libro: " + resultado.texto);
			} 
		}
	}
	var p = {
			idLibro : id,
			codigoLibro : codigo,
			tituloLibro : titulo,
			autorLibro : autor,
			editorialLibro : editorial,
			distribuidorLibro : distribuidor,
			cantidadLibro : cantidad,
			precioLibro : precio,
			precioIVALibro : precioIVA
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Libro.prototype.eliminar = function(id) {
	var opcion = confirm("¿Seguro quiere eliminar este libro?");
	if (opcion == true) {
		var request=new XMLHttpRequest();
		request.open("post", "eliminarLibro.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha eliminado el libro");
					$("#tableLibro").addClass("hide");
					$("#tableLibro").removeClass("show");
					document.getElementById("cajaCodigoLibro").value = '';
					document.getElementById("cajaTituloLibro").value = '';
					document.getElementById("cajaAutorLibro").value = '';
					document.getElementById("cajaEditorialLibro").value = '';
					document.getElementById("cajaDistribuidorLibro").value = '';
					document.getElementById("cajaCantidadLibro").value = '';
					document.getElementById("cajaPrecioLibro").value = '';
					document.getElementById("cajaPrecioIVALibro").value = '';
					document.getElementById("alert").innerHTML = "";
					document.getElementById("btnModificarLibro").disabled = true;
					document.getElementById("btnEliminarLibro").disabled = true;
					sessionStorage.removeItem("idLibro");
					usuario.cargarLibros();
				} else if (resultado.tipo=="ERROR") {
					alert("Error al eliminar un libro: " + resultado.texto);
				} 
			}
		}
		var p = {
				idLibro : id
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

function Tique() {
	this.idTique=-1;
	this.total=null;
	this.productos=null;
}

Tique.prototype.comprobarProductos = function(total, productos, key) {
	var request=new XMLHttpRequest();
	request.open("post", "comprobarProductosTique.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				//Tique nuevo
				if (key==0) {
					tique.crear(total, productos);
				//Tique modificado
				} else if (key==1){
					tique.modificar(total, productos);
				}
			} else if (resultado.tipo == "ERROR") {
				var mensaje = confirm("Hay algún producto sin existencias. ¿Desea continuar?")
				if (mensaje) {
					//Tique nuevo
					if (key==0) {
						tique.crear(total, productos);
					//Tique modificado
					} else if (key==1){
						tique.modificar(total, productos);
					}
				}
			} else if (resultado.tipo == "ERROR2") {
				alert("No se puede guardar un tique sin productos");
			}
		}
	}
	var p = {
			productosTique : productos
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Tique.prototype.crear = function(total, productos) {
	var request=new XMLHttpRequest();
	request.open("post", "crearTique.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha creado el tique");
				//El tique se añade a session al cargar los tiques.
				usuario.cargarTiques();
				var mensaje = confirm("¿Desea imprimir este tique?");
				if(mensaje){
					sessionStorage.tique = JSON.stringify(resultado.tique);
					usuario.imprimirTique();
				}
				$("#tdTique").addClass("show");
				$("#tdTique").removeClass("hide");
				$("#tdNuevoTique").addClass("hide");
				$("#tdNuevoTique").removeClass("show");
				tbodyNuevoTique.innerHTML = "";
				document.getElementById("totalNuevo").innerHTML = "0";
			} else if (resultado.tipo=="ERROR") {
				alert("Error al crear tique: " + resultado.texto);
			} 
		}
	}
	var p = {
			totalTique : total,
			productosTique : productos
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Tique.prototype.modificar = function(total, productos) {
	var tique = sessionStorage.getItem("tique");
	var tiqueJSON = JSON.parse(tique);
	var request=new XMLHttpRequest();
	request.open("post", "modificarTique.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha modificado el tique");
				//El tique se añade a session al cargar los tiques.
				usuario.cargarTiques();
				var mensaje = confirm("¿Desea imprimir este tique?");
				if(mensaje){
					sessionStorage.tique = JSON.stringify(resultado.tique);
					usuario.imprimirTique();
				}
				$("#tdTique").addClass("show");
				$("#tdTique").removeClass("hide");
				$("#tdNuevoTique").addClass("hide");
				$("#tdNuevoTique").removeClass("show");
				tbodyNuevoTique.innerHTML = "";
				document.getElementById("totalNuevo").innerHTML = "0";
			} else if (resultado.tipo=="ERROR") {
				alert("Error al modificar tique: " + resultado.texto);
			} 
		}
	}
	var p = {
			idTiqueSession : tiqueJSON.id,
			productosSession : tiqueJSON.productos,
			totalTique : total,
			productosTique : productos
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Tique.prototype.eliminar = function(tique) {
	var tiqueJSON = JSON.parse(tique);
	var opcion = confirm("¿Seguro quiere eliminar este tique?");
	if (opcion == true) {
		var request=new XMLHttpRequest();
		request.open("post", "eliminarTique.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha eliminado el tique");
					sessionStorage.removeItem("tique");
					usuario.cargarTiques();
				} else if (resultado.tipo=="ERROR") {
					alert("Error al eliminar un tique: " + resultado.texto);
				} 
			}
		}
		var p = {
				idTique : tiqueJSON.id,
				productosTique : tiqueJSON.productos
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

function Factura() {
	this.idFactura=-1;
	this.cliente=null;
	this.total=null;
	this.productos=null;
}

Factura.prototype.comprobarProductos = function(total, productos, key) {
	var request=new XMLHttpRequest();
	request.open("post", "comprobarProductosFactura.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				//Factura nuevo
				if (key==0) {
					factura.crear(total, productos);
				//Factura modificado
				} else if (key==1){
					factura.modificar(total, productos);
				}
			} else if (resultado.tipo=="ERROR"){
				var mensaje = confirm("Hay algún producto sin existencias. ¿Desea continuar?")
				if (mensaje) {
					//Factura nuevo
					if (key==0) {
						factura.crear(total, productos);
					//Factura modificado
					} else if (key==1){
						factura.modificar(total, productos);
					}
				}
			}
		}
	}
	var p = {
			productosFactura : productos
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Factura.prototype.crear = function(total, productos) {
	//Se comprueba que se ha seleccionado el cliente
	if (sessionStorage.cliente == null) {
		alert("No se puede guardar una factura sin cliente");
		document.getElementById("clienteNuevo").focus();
	} else {
		var cliente = JSON.parse(sessionStorage.cliente);
		var request=new XMLHttpRequest();
		request.open("post", "crearFactura.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha creado la factura");
					//La factura se añade a session al cargar las facturas.
					usuario.cargarFacturas();
					var mensaje = confirm("¿Desea imprimir esta factura?");
					if(mensaje){
						sessionStorage.factura = JSON.stringify(resultado.factura);
						usuario.imprimirFactura();
					}
					$("#tdFactura").addClass("show");
					$("#tdFactura").removeClass("hide");
					$("#tdNuevaFactura").addClass("hide");
					$("#tdNuevaFactura").removeClass("show");
					tbodyNuevaFactura.innerHTML = "";
					document.getElementById("totalNuevo").innerHTML = "0";
				} else if (resultado.tipo=="ERROR") {
					alert("Error al crear factura: " + resultado.texto);
				} 
			}
		}
		var p = {
				totalFactura : total,
				productosFactura : productos,
				clienteFactura : cliente
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

Factura.prototype.modificar = function(total, productos) {
	var factura = sessionStorage.getItem("factura");
	var facturaJSON = JSON.parse(factura);
	var clienteFactura = "";
	if (sessionStorage.cliente != null) {
		clienteFactura = JSON.parse(sessionStorage.cliente);
	}
	var request=new XMLHttpRequest();
	request.open("post", "modificarFactura.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha modificado la factura");
				//La factura se añade a session al cargar las facturas.
				usuario.cargarFacturas();
				var mensaje = confirm("¿Desea imprimir esta factura?");
				if(mensaje){
					sessionStorage.factura = JSON.stringify(resultado.factura);
					usuario.imprimirFactura();
				}
				$("#tdFactura").addClass("show");
				$("#tdFactura").removeClass("hide");
				$("#tdNuevaFactura").addClass("hide");
				$("#tdNuevaFactura").removeClass("show");
				tbodyNuevaFactura.innerHTML = "";
				document.getElementById("totalNuevo").innerHTML = "0";
			} else if (resultado.tipo=="ERROR") {
				alert("Error al modificar factura: " + resultado.texto);
			} 
		}
	}
	var p = {
			idFacturaSession : facturaJSON.id,
			productosSession : facturaJSON.productos,
			clienteSession : facturaJSON.cliente,
			totalFactura : total,
			productosFactura : productos,
			clienteFactura : clienteFactura
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Factura.prototype.eliminar = function(factura) {
	var facturaJSON = JSON.parse(factura);
	var opcion = confirm("¿Seguro quiere eliminar esta factura?");
	if (opcion == true) {
		var request=new XMLHttpRequest();
		request.open("post", "eliminarFactura.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha eliminado la factura");
					sessionStorage.removeItem("factura");
					usuario.cargarFacturas();
				} else if (resultado.tipo=="ERROR") {
					alert("Error al eliminar una factura: " + resultado.texto);
				} 
			}
		}
		var p = {
				idFactura : facturaJSON.id,
				productosFactura : facturaJSON.productos
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

function Albaran() {
	this.idAlbaran=-1;
	this.cliente=null;
	this.total=null;
	this.productos=null;
}

Albaran.prototype.crear = function(total, productos) {
	//Se comprueba que se ha seleccionado el cliente
	if (sessionStorage.cliente == null) {
		alert("No se puede guardar un albarán sin cliente");
		document.getElementById("clienteNuevo").focus();
	} else {
		var cliente = JSON.parse(sessionStorage.cliente);
		var request=new XMLHttpRequest();
		request.open("post", "crearAlbaran.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha creado el albarán");
					//El albarán se añade a session al cargar los albaranes.
					usuario.cargarAlbaranes();
					var mensaje = confirm("¿Desea imprimir este albarán?");
					if(mensaje){
						sessionStorage.albaran = JSON.stringify(resultado.albaran);
						usuario.imprimirAlbaran();
					}
					$("#tdAlbaran").addClass("show");
					$("#tdAlbaran").removeClass("hide");
					$("#tdNuevoAlbaran").addClass("hide");
					$("#tdNuevoAlbaran").removeClass("show");
					tbodyNuevoAlbaran.innerHTML = "";
					document.getElementById("totalNuevo").innerHTML = "0";
				} else if (resultado.tipo=="ERROR") {
					alert("Error al crear albarán: " + resultado.texto);
				} 
			}
		}
		var p = {
				totalAlbaran : total,
				productosAlbaran : productos,
				clienteAlbaran : cliente
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

Albaran.prototype.modificar = function(total, productos) {
	var albaran = sessionStorage.getItem("albaran");
	var albaranJSON = JSON.parse(albaran);
	var clienteAlbaran = "";
	if (sessionStorage.cliente != null) {
		clienteAlbaran = JSON.parse(sessionStorage.cliente);
	}
	var request=new XMLHttpRequest();
	request.open("post", "modificarAlbaran.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha modificado el albarán");
				//El albarán se añade a session al cargar los albaranes.
				usuario.cargarAlbaranes();
				var mensaje = confirm("¿Desea imprimir este albarán?");
				if(mensaje){
					sessionStorage.albaran = JSON.stringify(resultado.albaran);
					usuario.imprimirAlbaran();
				}
				$("#tdAlbaran").addClass("show");
				$("#tdAlbaran").removeClass("hide");
				$("#tdNuevoAlbaran").addClass("hide");
				$("#tdNuevoAlbaran").removeClass("show");
				tbodyNuevoAlbaran.innerHTML = "";
				document.getElementById("totalNuevo").innerHTML = "0";
			} else if (resultado.tipo=="ERROR") {
				alert("Error al modificar albarán: " + resultado.texto);
			} 
		}
	}
	var p = {
			idAlbaranSession : albaranJSON.id,
			productosSession : albaranJSON.productos,
			clienteSession : albaranJSON.cliente,
			totalAlbaran : total,
			productosAlbaran : productos,
			clienteAlbaran : clienteAlbaran
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Albaran.prototype.eliminar = function(albaran) {
	var albaranJSON = JSON.parse(albaran);
	var opcion = confirm("¿Seguro quiere eliminar este albarán?");
	if (opcion == true) {
		var request=new XMLHttpRequest();
		request.open("post", "eliminarAlbaran.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha eliminado el albarán");
					sessionStorage.removeItem("albaran");
					usuario.cargarAlbaranes();
				} else if (resultado.tipo=="ERROR") {
					alert("Error al eliminar albarán: " + resultado.texto);
				} 
			}
		}
		var p = {
				idAlbaran : albaranJSON.id
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

function Presupuesto() {
	this.idPresupuesto=-1;
	this.cliente=null;
	this.total=null;
	this.productos=null;
}

Presupuesto.prototype.crear = function(total, productos) {
	//Se comprueba que se ha seleccionado el cliente
	if (sessionStorage.cliente == null) {
		alert("No se puede guardar un presupuesto sin cliente");
		document.getElementById("clienteNuevo").focus();
	} else {
		var cliente = JSON.parse(sessionStorage.cliente);
		var request=new XMLHttpRequest();
		request.open("post", "crearPresupuesto.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha creado el presupuesto");
					//El presupuesto se añade a session al cargar los presupuestos.
					usuario.cargarPresupuestos();
					var mensaje = confirm("¿Desea imprimir este presupuesto?");
					if(mensaje){
						//Se añade el presupuesto en session para poder imprimirlo
						sessionStorage.presupuesto = JSON.stringify(resultado.presupuesto);
						usuario.imprimirPresupuesto();
					}
					$("#tdPresupuesto").addClass("show");
					$("#tdPresupuesto").removeClass("hide");
					$("#tdNuevoPresupuesto").addClass("hide");
					$("#tdNuevoPresupuesto").removeClass("show");
					tbodyNuevoPresupuesto.innerHTML = "";
					document.getElementById("totalNuevo").innerHTML = "0";
				} else if (resultado.tipo=="ERROR") {
					alert("Error al crear presupuesto: " + resultado.texto);
				} 
			}
		}
		var p = {
				totalPresupuesto : total,
				productosPresupuesto : productos,
				clientePresupuesto : cliente
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}

Presupuesto.prototype.modificar = function(total, productos) {
	var presupuesto = sessionStorage.getItem("presupuesto");
	var presupuestoJSON = JSON.parse(presupuesto);
	var clientePresupuesto = "";
	if (sessionStorage.cliente != null) {
		clientePresupuesto = JSON.parse(sessionStorage.cliente);
	}
	var request=new XMLHttpRequest();
	request.open("post", "modificarPresupuesto.jsp");
	request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	request.onreadystatechange = function() {
		if (request.readyState==4) {
			var resultado=JSON.parse(request.responseText);
			if (resultado.tipo=="OK") {
				alert("Se ha modificado el presupuesto");
				//El presupuesto se añade a session al cargar los presupuestos.
				usuario.cargarPresupuestos();
				var mensaje = confirm("¿Desea imprimir este presupuesto?");
				if(mensaje){
					sessionStorage.presupuesto = JSON.stringify(resultado.presupuesto);
					usuario.imprimirPresupuesto();
				}
				$("#tdPresupuesto").addClass("show");
				$("#tdPresupuesto").removeClass("hide");
				$("#tdNuevoPresupuesto").addClass("hide");
				$("#tdNuevoPresupuesto").removeClass("show");
				tbodyNuevoPresupuesto.innerHTML = "";
				document.getElementById("totalNuevo").innerHTML = "0";
			} else if (resultado.tipo=="ERROR") {
				alert("Error al modificar presupuesto: " + resultado.texto);
			} 
		}
	}
	var p = {
			idPresupuestoSession : presupuestoJSON.id,
			productosSession : presupuestoJSON.productos,
			clienteSession : presupuestoJSON.cliente,
			totalPresupuesto : total,
			productosPresupuesto : productos,
			clientePresupuesto : clientePresupuesto
	};
	var linea="p=" + JSON.stringify(p);
	request.send(linea);
}

Presupuesto.prototype.eliminar = function(presupuesto) {
	var presupuestoJSON = JSON.parse(presupuesto);
	var opcion = confirm("¿Seguro quiere eliminar este presupuesto?");
	if (opcion == true) {
		var request=new XMLHttpRequest();
		request.open("post", "eliminarPresupuesto.jsp");
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		request.onreadystatechange = function() {
			if (request.readyState==4) {
				var resultado=JSON.parse(request.responseText);
				if (resultado.tipo=="OK") {
					alert("Se ha eliminado el presupuesto");
					sessionStorage.removeItem("presupuesto");
					usuario.cargarPresupuestos();
				} else if (resultado.tipo=="ERROR") {
					alert("Error al eliminar presupuesto: " + resultado.texto);
				} 
			}
		}
		var p = {
				idPresupuesto : presupuestoJSON.id
		};
		var linea="p=" + JSON.stringify(p);
		request.send(linea);
	}
}