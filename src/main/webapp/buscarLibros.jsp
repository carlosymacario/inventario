<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Articulo"%>
<%@page import="gestionK.dominio.Usuario"%>
<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	Usuario usuario=(Usuario) session.getAttribute("usuario");

	String p=request.getParameter("p");
	JSONObject objeto=new JSONObject(p);

	String codigoLibro=objeto.get("codigoLibro").toString();
	String tituloLibro=objeto.get("tituloLibro").toString();
	String autorLibro=objeto.get("autorLibro").toString();
	String editorialLibro=objeto.get("editorialLibro").toString();
	String distribuidorLibro=objeto.get("distribuidorLibro").toString();
	int cantidadLibro = -1;
	double precioLibro = -1;
	double precioIVALibro = -1;
	if (!objeto.get("cantidadLibro").toString().equals("")) {
		cantidadLibro = Integer.parseInt(objeto.get("cantidadLibro").toString());
	}
	if (!objeto.get("precioLibro").toString().equals("")) {
		precioLibro=Double.parseDouble(objeto.get("precioLibro").toString());
	}
	if (!objeto.get("precioIVALibro").toString().equals("")) {
		precioIVALibro=Double.parseDouble(objeto.get("precioIVALibro").toString());
	}
	
	JSONObject resultado=new JSONObject();
	if (usuario!=null) {
		try {
			JSONArray libros=Manager.get().getBusquedaLibrosAsJSONArray(usuario, codigoLibro, tituloLibro, autorLibro, editorialLibro, distribuidorLibro, cantidadLibro, precioLibro, precioIVALibro);
			resultado.put("tipo", "OK");
			resultado.put("libros", libros);
		}
		catch (Exception e) {
			resultado.put("tipo", "ERROR");
			resultado.put("texto", e.getMessage());
		}
	} else {
		resultado.put("tipo", "NO_EXISTE");
	}
	out.println(resultado);
%>