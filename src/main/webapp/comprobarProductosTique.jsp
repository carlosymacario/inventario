<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	JSONArray productosTique=objeto.getJSONArray("productosTique");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Manager.get().comprobarProductosTique(productosTique);
		resultado.put("tipo", "OK");
	}
	catch (Exception e) {
		if(e.getMessage().equals("NO")) {
			resultado.put("tipo", "ERROR2");
			resultado.put("texto", "No hay productos");
		} else {
			resultado.put("tipo", "ERROR");
			resultado.put("texto", e.getMessage());
		}
	}
	out.println(resultado);
%>