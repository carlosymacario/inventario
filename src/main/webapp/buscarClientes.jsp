<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Cliente"%>
<%@page import="gestionK.dominio.Usuario"%>
<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	Usuario usuario=(Usuario) session.getAttribute("usuario");

	String p=request.getParameter("p");
	JSONObject objeto=new JSONObject(p);

	String nombreCliente=objeto.getString("nombreCliente");
	String apellidoCliente=objeto.getString("apellidoCliente");
	String NIFCliente=objeto.getString("NIFCliente");
	String direccionCliente=objeto.getString("direccionCliente");
	String localidadCliente=objeto.getString("localidadCliente");
	String CPCliente=objeto.getString("CPCliente");
	String provinciaCliente=objeto.getString("provinciaCliente");
	String paisCliente=objeto.getString("paisCliente");
	String telefonoCliente=objeto.getString("telefonoCliente");
	String correoCliente=objeto.getString("correoCliente");
	
	JSONObject resultado=new JSONObject();
	if (usuario!=null) {
		try {
			JSONArray clientes=Manager.get().getBusquedaClientesAsJSONArray(usuario, nombreCliente, apellidoCliente, NIFCliente, direccionCliente, localidadCliente, CPCliente, provinciaCliente, paisCliente, telefonoCliente, correoCliente);
			resultado.put("tipo", "OK");
			resultado.put("clientes", clientes);
		}
		catch (Exception e) {
			resultado.put("tipo", "ERROR");
			resultado.put("texto", e.getMessage());
		}
	} else {
		resultado.put("tipo", "NO_EXISTE");
	}
	out.println(resultado);
%>