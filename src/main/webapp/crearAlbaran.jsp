<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Albaran"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalAlbaran=-1;
	if (!objeto.getString("totalAlbaran").equals("")) {
		totalAlbaran = Double.parseDouble(objeto.getString("totalAlbaran"));
	}
	JSONArray productosAlbaran=objeto.getJSONArray("productosAlbaran");
	JSONObject cliente = objeto.getJSONObject("clienteAlbaran");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Albaran albaran = Manager.get().crearAlbaran(totalAlbaran, productosAlbaran, cliente);
		resultado.put("tipo", "OK");
		resultado.put("albaran", albaran);
		resultado.put("totalAlbaran", totalAlbaran);
		resultado.put("productos", productosAlbaran);
		resultado.put("cliente", cliente);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>