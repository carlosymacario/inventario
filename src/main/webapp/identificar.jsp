<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Usuario"%>
<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONObject"%>

<%
	String p=request.getParameter("p");
	JSONObject objeto=new JSONObject(p);
	
	String email=objeto.get("email").toString();
	String pwd=objeto.get("pwd").toString();

	JSONObject resultado=new JSONObject();		
	try {
		Usuario usuario=Manager.get().identificar(email, pwd);
		resultado.put("tipo", "OK");
		resultado.put("email", email);
		String id = usuario.getId();
		resultado.put("id", id);
		session.setAttribute("usuario", usuario);
		
		Cookie cookie=new Cookie("idUsuario", ""+usuario.getId());
		cookie.setMaxAge(365*24*3600);
		response.addCookie(cookie);	
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", "Ese usuario no se encuentra en Base de datos");
	}
	out.print(resultado);
%>