<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Factura"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalFacturaNuevo=-1;
	if (!objeto.getString("totalFactura").equals("")) {
		totalFacturaNuevo = Double.parseDouble(objeto.getString("totalFactura"));
	}
	JSONArray productosFacturaNuevo=objeto.getJSONArray("productosFactura");
	JSONObject clienteFacturaNuevo = null;
	if (!objeto.get("clienteFactura").equals("")) {
		clienteFacturaNuevo=objeto.getJSONObject("clienteFactura");
	}
	
	//Factura de session (antes de modificarla)
	String idFacturaSession = objeto.getString("idFacturaSession");
	JSONArray productosFacturaSession=objeto.getJSONArray("productosSession");
	JSONObject clienteFacturaSession=objeto.getJSONObject("clienteSession");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Factura factura = Manager.get().modificarFactura(totalFacturaNuevo, productosFacturaNuevo, clienteFacturaNuevo, productosFacturaSession, idFacturaSession, clienteFacturaSession);
		resultado.put("tipo", "OK");
		resultado.put("factura", factura);
		resultado.put("totalFactura", totalFacturaNuevo);
		resultado.put("productos", productosFacturaNuevo);
		resultado.put("cliente", clienteFacturaNuevo);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>