<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto=new JSONObject(p);

	String nombreCliente=objeto.get("nombreCliente").toString();
	String apellidoCliente=objeto.get("apellidoCliente").toString();
	String NIFCliente=objeto.get("NIFCliente").toString();
	String direccionCliente=objeto.get("direccionCliente").toString();
	String localidadCliente=objeto.get("localidadCliente").toString();
	String CPCliente=objeto.get("CPCliente").toString();
	String provinciaCliente=objeto.get("provinciaCliente").toString();
	String paisCliente=objeto.get("paisCliente").toString();
	String telefonoCliente=objeto.get("telefonoCliente").toString();
	String correoCliente=objeto.get("correoCliente").toString();
	
	JSONObject resultado=new JSONObject();		
	try {
		String idCliente = Manager.get().crearCliente(nombreCliente, apellidoCliente, NIFCliente, direccionCliente, localidadCliente, CPCliente, provinciaCliente, paisCliente, telefonoCliente, correoCliente);
		resultado.put("tipo", "OK");
		resultado.put("idCliente", idCliente);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>