<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Tique"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalTique=-1;
	if (!objeto.getString("totalTique").equals("")) {
		totalTique = Double.parseDouble(objeto.getString("totalTique"));
	}
	JSONArray productosTique=objeto.getJSONArray("productosTique");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Tique tique = Manager.get().crearTique(totalTique, productosTique);
		resultado.put("tipo", "OK");
		resultado.put("tique", tique);
		resultado.put("totalTique", totalTique);
		resultado.put("productos", productosTique);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>