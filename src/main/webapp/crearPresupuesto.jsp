<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Presupuesto"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalPresupuesto=-1;
	if (!objeto.getString("totalPresupuesto").equals("")) {
		totalPresupuesto = Double.parseDouble(objeto.getString("totalPresupuesto"));
	}
	JSONArray productosPresupuesto=objeto.getJSONArray("productosPresupuesto");
	JSONObject cliente = objeto.getJSONObject("clientePresupuesto");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Presupuesto presupuesto = Manager.get().crearPresupuesto(totalPresupuesto, productosPresupuesto, cliente);
		resultado.put("tipo", "OK");
		resultado.put("presupuesto", presupuesto.toJSONObject());
		resultado.put("totalPresupuesto", totalPresupuesto);
		resultado.put("productos", productosPresupuesto);
		resultado.put("cliente", cliente);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>