<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Usuario"%>
<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONObject"%>

<%
	Cookie[] cookies=request.getCookies();
	Cookie cookieIdUsuario=null;
	if (cookies!=null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("idUsuario")) {
				cookieIdUsuario=cookie;
				break;
			}
		}
	}

	JSONObject resultado=new JSONObject();		
	try {
		Usuario usuario=Manager.get().identificar(cookieIdUsuario.getValue());
		resultado.put("tipo", "OK");
		resultado.put("email", usuario.getEmail());
		resultado.put("id", usuario.getId());
		session.putValue("usuario", usuario);
		
		Cookie cookie=new Cookie("idUsuario", ""+usuario.getId());
		cookie.setMaxAge(365*24*3600);
		response.addCookie(cookie);		
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.print(resultado);
%>