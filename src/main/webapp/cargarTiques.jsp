<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Producto"%>
<%@page import="gestionK.dominio.Usuario"%>
<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	Usuario usuario=(Usuario) session.getAttribute("usuario");
	
	JSONObject resultado=new JSONObject();
	if (usuario!=null) {
		try {
			JSONArray tiques=Manager.get().getTiquesAsJSONArray(usuario);
			resultado.put("tipo", "OK");
			resultado.put("tiques", tiques);
		}
		catch (Exception e) {
			resultado.put("tipo", "ERROR");
			resultado.put("texto", e.getMessage());
		}
	} else {
		resultado.put("tipo", "NO_EXISTE");
	}
	out.println(resultado);
%>