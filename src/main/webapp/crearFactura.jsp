<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Factura"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalFactura=-1;
	if (!objeto.getString("totalFactura").equals("")) {
		totalFactura = Double.parseDouble(objeto.getString("totalFactura"));
	}
	JSONArray productosFactura=objeto.getJSONArray("productosFactura");
	JSONObject cliente = objeto.getJSONObject("clienteFactura");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Factura factura = Manager.get().crearFactura(totalFactura, productosFactura, cliente);
		resultado.put("tipo", "OK");
		resultado.put("factura", factura);
		resultado.put("totalFactura", totalFactura);
		resultado.put("productos", productosFactura);
		resultado.put("cliente", cliente);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>