<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Tique"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalTiqueNuevo=-1;
	if (!objeto.getString("totalTique").equals("")) {
		totalTiqueNuevo = Double.parseDouble(objeto.getString("totalTique"));
	}
	JSONArray productosTiqueNuevo=objeto.getJSONArray("productosTique");
	
	//Tique de session (antes de modificarlo)
	String idTiqueSession = objeto.getString("idTiqueSession");
	JSONArray productosTiqueSession=objeto.getJSONArray("productosSession");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Tique tique = Manager.get().modificarTique(totalTiqueNuevo, productosTiqueNuevo, productosTiqueSession, idTiqueSession);
		resultado.put("tipo", "OK");
		resultado.put("tique", tique);
		resultado.put("totalTique", totalTiqueNuevo);
		resultado.put("productos", productosTiqueNuevo);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>