<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Presupuesto"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalPresupuestoNuevo=-1;
	if (!objeto.getString("totalPresupuesto").equals("")) {
		totalPresupuestoNuevo = Double.parseDouble(objeto.getString("totalPresupuesto"));
	}
	JSONArray productosPresupuestoNuevo=objeto.getJSONArray("productosPresupuesto");
	JSONObject clientePresupuestoNuevo = null;
	if (!objeto.get("clientePresupuesto").equals("")) {
		clientePresupuestoNuevo=objeto.getJSONObject("clientePresupuesto");
	}
	
	//Presupuesto de session (antes de modificarlo)
	String idPresupuestoSession = objeto.getString("idPresupuestoSession");
	JSONArray productosPresupuestoSession=objeto.getJSONArray("productosSession");
	JSONObject clientePresupuestoSession=objeto.getJSONObject("clienteSession");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Presupuesto presupuesto = Manager.get().modificarPresupuesto(totalPresupuestoNuevo, productosPresupuestoNuevo, clientePresupuestoNuevo, productosPresupuestoSession, idPresupuestoSession, clientePresupuestoSession);
		resultado.put("tipo", "OK");
		resultado.put("presupuesto", presupuesto);
		resultado.put("totalPresupuesto", totalPresupuestoNuevo);
		resultado.put("productos", productosPresupuestoNuevo);
		resultado.put("cliente", clientePresupuestoNuevo);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>