<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Articulo"%>
<%@page import="gestionK.dominio.Usuario"%>
<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}
	
	Usuario usuario=(Usuario) session.getAttribute("usuario");

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	String codigoArticulo=objeto.get("codigoArticulo").toString();
	String nombreArticulo=objeto.get("nombreArticulo").toString();
	int cantidadArticulo = -1;
	double precioArticulo = -1;
	double precioIVAArticulo = -1;
	if (!objeto.get("cantidadArticulo").toString().equals("")) {
		cantidadArticulo = Integer.parseInt(objeto.get("cantidadArticulo").toString());
	}
	if (!objeto.get("precioArticulo").toString().equals("")) {
		precioArticulo=Double.parseDouble(objeto.get("precioArticulo").toString());
	}
	if (!objeto.get("precioIVAArticulo").toString().equals("")) {
		precioIVAArticulo=Double.parseDouble(objeto.get("precioIVAArticulo").toString());
	}
	
	JSONObject resultado=new JSONObject();
	if (usuario!=null) {
		try {
			JSONArray articulos=Manager.get().getBusquedaArticulosAsJSONArray(usuario, codigoArticulo, nombreArticulo, cantidadArticulo, precioArticulo, precioIVAArticulo);
			resultado.put("tipo", "OK");
			resultado.put("articulos", articulos);
		}
		catch (Exception e) {
			resultado.put("tipo", "ERROR");
			resultado.put("texto", e.getMessage());
		}
	} else {
		resultado.put("tipo", "NO_EXISTE");
	}
	out.println(resultado);
%>