<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto=new JSONObject(p);
	
	String idArticulo=objeto.get("idArticulo").toString();
	String codigoArticulo=objeto.get("codigoArticulo").toString();
	String nombreArticulo=objeto.get("nombreArticulo").toString();
	int cantidadArticulo = -1;
	double precioArticulo = -1;
	double precioIVAArticulo = -1;
	if (!objeto.get("cantidadArticulo").toString().equals("")) {
		cantidadArticulo = Integer.parseInt(objeto.get("cantidadArticulo").toString());
	}
	if (!objeto.get("precioArticulo").toString().equals("")) {
		precioArticulo=Double.parseDouble(objeto.get("precioArticulo").toString());
	}
	if (!objeto.get("precioIVAArticulo").toString().equals("")) {
		precioIVAArticulo=Double.parseDouble(objeto.get("precioIVAArticulo").toString());
	}
	
	JSONObject resultado=new JSONObject();		
	try {
		Manager.get().modificarArticulo(idArticulo, codigoArticulo, nombreArticulo, cantidadArticulo, precioArticulo, precioIVAArticulo);
		resultado.put("tipo", "OK");
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>