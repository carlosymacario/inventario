<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto=new JSONObject(p);

	String idTique=objeto.get("idTique").toString();
	JSONArray productosTique=objeto.getJSONArray("productosTique");
	
	JSONObject resultado=new JSONObject();		
	try {
		Manager.get().eliminarTique(idTique, productosTique);
		resultado.put("tipo", "OK");
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>