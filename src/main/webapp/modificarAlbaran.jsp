<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="gestionK.dominio.Albaran"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto= new JSONObject(p);

	double totalAlbaranNuevo=-1;
	if (!objeto.getString("totalAlbaran").equals("")) {
		totalAlbaranNuevo = Double.parseDouble(objeto.getString("totalAlbaran"));
	}
	JSONArray productosAlbaranNuevo=objeto.getJSONArray("productosAlbaran");
	JSONObject clienteAlbaranNuevo = null;
	if (!objeto.get("clienteAlbaran").equals("")) {
		clienteAlbaranNuevo=objeto.getJSONObject("clienteAlbaran");
	}
	
	//Albarán de session (antes de modificarlo)
	String idAlbaranSession = objeto.getString("idAlbaranSession");
	JSONArray productosAlbaranSession=objeto.getJSONArray("productosSession");
	JSONObject clienteAlbaranSession=objeto.getJSONObject("clienteSession");
	
	JSONObject resultado=new JSONObject();		
	
	try {
		Albaran albaran = Manager.get().modificarAlbaran(totalAlbaranNuevo, productosAlbaranNuevo, clienteAlbaranNuevo, productosAlbaranSession, idAlbaranSession, clienteAlbaranSession);
		resultado.put("tipo", "OK");
		resultado.put("albaran", albaran);
		resultado.put("totalAlbaran", totalAlbaranNuevo);
		resultado.put("productos", productosAlbaranNuevo);
		resultado.put("cliente", clienteAlbaranNuevo);
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>