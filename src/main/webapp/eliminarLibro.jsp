<%@ page language="java" contentType="application/json" pageEncoding="UTF-8"%>

<%@page import="gestionK.dominio.Manager"%>
<%@page import="org.json.JSONObject"%>

<%
	JSONObject existe = gestionK.web.CheckSession.check(session);

	if (existe != null) {
		out.print(existe);
		return;
	}

	String p=request.getParameter("p");
	JSONObject objeto=new JSONObject(p);

	String idLibro=objeto.get("idLibro").toString();
	
	JSONObject resultado=new JSONObject();		
	try {
		Manager.get().eliminarLibro(idLibro);
		resultado.put("tipo", "OK");
	}
	catch (Exception e) {
		resultado.put("tipo", "ERROR");
		resultado.put("texto", e.getMessage());
	}
	out.println(resultado);
%>